/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Surname;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author sergio
 */
public class ESSurname {

    

    private Long id;
    private String surname;
    private Date updated;

    public ESSurname() {
    }

    public ESSurname(long id, String surname) {
        this.id = id;
        this.surname = surname;
    }

    public ESSurname(String surname) {
        this.surname = surname;
    }

    private ESSurname(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    
    public static List<ESSurname> fromArray(String[] surnames) {
        if (surnames != null) {
            List<ESSurname> sm = new ArrayList();
            for(String name: surnames) {
                sm.add(new ESSurname(name));
            }
            return sm;
        }
        return null;
    }
    
    public static List<ESSurname> fromArray(Long[] surname_ids) {
        if (surname_ids != null) {
            List<ESSurname> sm = new ArrayList();
            for(Long id: surname_ids) {
                sm.add(new ESSurname(id));
            }
            return sm;
        }
        return null;
    }
    
    public static List<ESSurname> fromList(List<Surname> surnames) {
        if (surnames != null) {
            List<ESSurname> esurname = new ArrayList();
            surnames.forEach((s) -> {
                if (s != null) {
                    esurname.add(ESSurname.fromSurname(s));
                }
            });
            return esurname;
        }
        return null;
    }

    public static ESSurname fromSurname(Surname surname) {
        if (surname != null) {
            ESSurname sur = new ESSurname();
            
            sur.setId(surname.getId());
            sur.setSurname(surname.getSurname());
            sur.setUpdated(surname.getUpdated());
            
            return sur;
        }
        return null;
    }
    
}
