/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

/**
 *
 * @author sergio
 */
public interface SpeedEntity {
    public String getIndexName();
    public String getTypeName();
    public String getId();
    public SpeedEntity fromBatchEntity(BatchEntity bentity);
    public Boolean isSameAs(BatchEntity bentity);
}
