/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Job;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author fnunez
 */
@Document(indexName = "genpro", type = "jobs", createIndex = false)
public class ESJob implements SpeedEntity {

    @Id
    private String id;

    private String name;

    private String description;

    private String status;

    private double amount;

    private Date due_date;
    
    private Date posted;

    private int post_days;

    private long user_id;
    
    private String user_name;

    private long expert_id;

    private String expert_name;

    @Field(type = FieldType.Object)
    private List<ESJobSkill> skills;
    
    @Field(type = FieldType.Object)
    private List<ESPlace> places;

    @Field(type = FieldType.Object)
    private List<ESAttachment> attachments;

    @Field(type = FieldType.Object)
    private ESJobRating rating;

    private Boolean deleted;
    private Date created;
    private Date updated;

    @Override
    public String getIndexName() {
        return "genpro";
    }

    @Override
    public String getTypeName() {
        return "jobs";
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        return ESJob.fromJob((Job) bentity);
    }

    public static ESJob fromJob(Job job) {
        if (job != null) {
            ESJob esJob = new ESJob();
            esJob.setId(String.valueOf(job.getId()));
            esJob.setAmount(job.getAmount());
            esJob.setAttachments(ESAttachment.fromJobDocuments(job.getDocuments()));
            esJob.setCreated(job.getCreated());
            esJob.setUpdated(job.getUpdated());
            esJob.setDeleted(job.getDeleted() == 1);
            esJob.setDescription(job.getDescription());
            esJob.setDue_date(job.getDue_date());
            if (job.getExpert() != null) {
                esJob.setExpert_id(job.getExpert().getId());
                esJob.setExpert_name(job.getExpert().getUser().getPerson().getFirst_name() + " " + job.getExpert().getUser().getPerson().getLast_name());
            }
            esJob.setName(job.getName());
            esJob.setPosted(job.getPosted());
            esJob.setPost_days(job.getDays_post());
            esJob.setRating(ESJobRating.fromFeedback(job.getFeedback()));
            esJob.setSkills(ESJobSkill.fromSkillsList(job.getSkills()));
            esJob.setPlaces(ESPlace.fromList(job.getPlaces()));
            esJob.setStatus(job.getStatus().getName());
            esJob.setUser_id(job.getUser().getId());
            esJob.setUser_name(job.getUser().getPerson().getFirst_name() + " " + job.getUser().getPerson().getLast_name());
            return esJob;
        }
        return null;
    }

    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        if (bentity == null) {
            return false;
        }
        if (bentity instanceof Job) {
            Job job = (Job) bentity;
            if (this.id != null && Long.valueOf(this.id) != job.getId()) {
                return false;
            }
            if (job.getUpdated() == null) {
                return false;
            }
            return (this.updated != null && this.updated.getTime() == job.getUpdated().getTime());
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPosted() {
        return posted;
    }

    public void setPosted(Date posted) {
        this.posted = posted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public int getPost_days() {
        return post_days;
    }

    public void setPost_days(int post_days) {
        this.post_days = post_days;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getExpert_id() {
        return expert_id;
    }

    public void setExpert_id(long expert_id) {
        this.expert_id = expert_id;
    }

    public String getExpert_name() {
        return expert_name;
    }

    public void setExpert_name(String expert_name) {
        this.expert_name = expert_name;
    }

    public List<ESJobSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<ESJobSkill> skills) {
        this.skills = skills;
    }

    public List<ESAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<ESAttachment> attachments) {
        this.attachments = attachments;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public ESJobRating getRating() {
        return rating;
    }

    public void setRating(ESJobRating rating) {
        this.rating = rating;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

	public List<ESPlace> getPlaces() 
	{
		return places;
	}

	public void setPlaces(List<ESPlace> places) 
	{
		this.places = places;
	}
}
