/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Feedback;
import java.util.Date;

/**
 *
 * @author fnunez
 */
public class ESJobRating {

    private double quality;
    private double expertise;
    private double responsiveness;
    private double average;
    private String comments;
    private Date created;

    public static ESJobRating fromFeedback(Feedback feedback) {
        if (feedback != null) {
            ESJobRating rating = new ESJobRating();
            rating.setQuality(feedback.getQuality());
            rating.setExpertise(feedback.getExpertise());
            rating.setResponsiveness(feedback.getResponsiveness());
            rating.setAverage((rating.getExpertise() + rating.getQuality() + rating.getResponsiveness()) / 3);
            rating.setComments(feedback.getComments());
            rating.setCreated(feedback.getCreated());
            return rating;
        }
        return null;
    }

    public double getQuality() {
        return quality;
    }

    public void setQuality(double quality) {
        this.quality = quality;
    }

    public double getExpertise() {
        return expertise;
    }

    public void setExpertise(double expertise) {
        this.expertise = expertise;
    }

    public double getResponsiveness() {
        return responsiveness;
    }

    public void setResponsiveness(double responsiveness) {
        this.responsiveness = responsiveness;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
