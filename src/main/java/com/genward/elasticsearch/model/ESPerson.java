/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.genward.persistence.model.People;
import com.genward.persistence.model.PeoplePlaces;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author sergio
 */
@Document(indexName = "genealogy_catalog", type = "people", createIndex = false)
public class ESPerson implements SpeedEntity {

    @Id
    private String id;

    private String name;

    private String prefix;

    private String gender;

    @Field(type = FieldType.Object)
    private ESSurname surname;

    @Field(type = FieldType.Object)
    private List<ESPersonAltName> altnames;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deathdate;

    @Field(type = FieldType.Object)
    private ESPlace birthplace;

    @Field(type = FieldType.Object)
    private ESPlace deathplace;

    @Field(type = FieldType.Object)
    private ESFamily parents;

    @Field(type = FieldType.Object)
    private List<ESFamily> marriages;

    @Field(type = FieldType.Object)
    private List<ESCitation> citations;

    @Field(type = FieldType.Object)
    private List<ESFactPlace> factPlaces;

    private Boolean deleted;
    private Date created;
    private Date updated;

    public ESPerson() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public ESSurname getSurname() {
        return surname;
    }

    public void setSurname(ESSurname surname) {
        this.surname = surname;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<ESPersonAltName> getAltnames() {
        return altnames;
    }

    public void setAltnames(List<ESPersonAltName> altnames) {
        this.altnames = altnames;
    }

    public Date getDeathdate() {
        return deathdate;
    }

    public void setDeathdate(Date deathdate) {
        this.deathdate = deathdate;
    }

    public ESPlace getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(ESPlace birthplace) {
        this.birthplace = birthplace;
    }

    public ESPlace getDeathplace() {
        return deathplace;
    }

    public void setDeathplace(ESPlace deathplace) {
        this.deathplace = deathplace;
    }

    public ESFamily getParents() {
        return parents;
    }

    public void setParents(ESFamily parents) {
        this.parents = parents;
    }

    public List<ESFamily> getMarriages() {
        return marriages;
    }

    public void setMarriages(List<ESFamily> marriages) {
        this.marriages = marriages;
    }

    public List<ESCitation> getCitations() {
        return citations;
    }

    public void setCitations(List<ESCitation> citations) {
        this.citations = citations;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<ESFactPlace> getFactPlaces() {
        return factPlaces;
    }

    public void setFactPlaces(List<ESFactPlace> factPlaces) {
        this.factPlaces = factPlaces;
    }

    public static ESPerson fromPeople(People people) {
        if (people != null) {
            ESPerson person = new ESPerson();
            person.setId(String.valueOf(people.getId()));
            person.setName(people.getName());
            person.setPrefix(people.getPrefix());
            person.setGender(people.getGender());
            person.setSurname(ESSurname.fromSurname(people.getSurname()));
            person.setAltnames(ESPersonAltName.fromList(people.getAltNames()));
            person.setBirthdate(people.getBirth());
            person.setDeathdate(people.getDeath());
            if (people.getFactPlaces() != null) {
                for (PeoplePlaces pp : people.getFactPlaces()) {
                    if (pp.getType().getName() != null && pp.getType().getName().equals("Birth")) {
                        person.setBirthplace(ESPlace.fromPlace(pp.getPlace()));
                    }
                    if (pp.getType().getName() != null && pp.getType().getName().equals("Death")) {
                        person.setDeathplace(ESPlace.fromPlace(pp.getPlace()));
                    }
                }
            }
            person.setParents(ESFamily.fromFamilies(people.getParents()));
            person.setMarriages(ESFamily.fromList(people.getMarriages()));
            person.setCitations(ESCitation.fromList(people.getCitations()));
            person.setFactPlaces(ESFactPlace.fromList(people.getFactPlaces()));
            person.setCreated(people.getCreated());
            person.setUpdated(people.getUpdated());
            person.setDeleted(people.getDeleted() == 1);
            return person;
        }
        return null;
    }

    public static List<ESPerson> fromList(List<People> people) {
        if (people != null) {
            List<ESPerson> esperson = new ArrayList();
            for (People p : people) {
                if (p != null) {
                    esperson.add(ESPerson.fromPeople(p));
                }

            }
            return esperson;
        }
        return null;
    }

//    public static List<ESPerson> listFromCommaSeparated(String author) {
//        if (author != null) {
//
//            List<String> anames = Arrays.asList(author);
//            List<ESPerson> authors = new ArrayList();
//            List<String> pair;
//            ESPerson p = new ESPerson();
//            p.setSurname(new ESSurname(author));
//
//            if (author.contains(";")) {
//                anames = Arrays.asList(author.trim().split(";"));
//            }
//
//            for (String fullname : anames) {
//                if (fullname.contains(",")) {
//                    pair = Arrays.asList(fullname.trim().split(","));
//                    p.setName(pair.get(1));
//                    p.getSurname().setSurname(pair.get(0));
//                }
//                authors.add(p);
//            }
//            return authors;
//        }
//        return null;
//    }

    @Override
    public String getIndexName() {
        return "genealogy_catalog";
    }

    @Override
    public String getTypeName() {
        return "people";
    }

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        return ESPerson.fromPeople((People) bentity);
    }

    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        if (bentity == null) {
            return false;
        }
        if (bentity instanceof People) {
            People people = (People) bentity;
            if (this.id != null && Long.valueOf(this.id) != people.getId()) {
                return false;
            }
            if (people.getUpdated() == null) {
                return false;
            }
            return (this.updated != null && this.updated.getTime() == people.getUpdated().getTime());
        }
        return false;
    }
}
