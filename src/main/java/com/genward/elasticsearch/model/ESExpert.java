/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Expert;
import com.genward.persistence.model.Job;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author fnunez
 */
@Document(indexName = "genpro", type = "experts")
public class ESExpert implements SpeedEntity {

    @Id
    private String id;

    private String name;

    private String description;

    private String country;

    private double hourlyRate;

    private int jobs;

    private double jobSuccess;

    private long points;

    private double lastYearEarnings;

    private double lifetimeEarnings;

    private double availableHoursPerWeek;

    private int availabilityStart;
    private int availabilityEnd;

    @Field(type = FieldType.Object)
    private ESPlace place;

    @Field(type = FieldType.Object)
    private List<ESSkill> skills;

    @Field(type = FieldType.Object)
    private List<ESLanguage> spokenLanguages;

    @Field(type = FieldType.Object)
    private List<ESBillingMethod> billingMethods;
    
    private Date created;
    private Date updated;
    private Date lastActivity;

    @Override
    public String getIndexName() {
        return "genpro";
    }

    @Override
    public String getTypeName() {
        return "experts";
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        return ESExpert.fromExpert((Expert) bentity);
    }

    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        if (bentity == null) {
            return false;
        }
        if (bentity instanceof Expert) {
            Expert expert = (Expert) bentity;
            if (this.id != null && Long.valueOf(this.id) != expert.getId()) {
                return false;
            }
            if (expert.getUpdated() == null) {
                return false;
            }
            return (this.updated != null && this.updated.getTime() == expert.getUpdated().getTime());
        }
        return false;
    }

    public static ESExpert fromExpert(Expert expert) {
        ESExpert e = new ESExpert();
        e.setId(String.valueOf(expert.getId()));
        e.setDescription(expert.getDescription());
        e.setName(expert.getUser().getPerson().getFirst_name() + " " + expert.getUser().getPerson().getLast_name());
        e.setPlace(ESPlace.fromPlace(expert.getUser().getPerson().getPlace()));
        e.setCountry(expert.getCountry());
        e.setHourlyRate(expert.getAcceptable_rate());
        e.setPoints(expert.getUser().getPoints());
        e.setAvailableHoursPerWeek(expert.getAvailable_hours());
        e.setCreated(expert.getCreated());
        e.setUpdated(expert.getUpdated());
        e.setSkills(ESSkill.fromUsrSkillList(expert.getSkills()));
        e.setSpokenLanguages(ESLanguage.fromList(expert.getSpokenLanguages()));
        e.setLastActivity(expert.getUser().getLast_activity());
        e = addJobsDetail(e, expert.getJobs());
        return e;
    }

    private static ESExpert addJobsDetail(ESExpert e, List<Job> jobs) {
        if (jobs != null && !jobs.isEmpty()) {
            double feedbackSum = 0;
            int completedJobs = 0;
            for (Job job : jobs) {
                if (job.getStatus().getId() == Job.STATUS_COMPLETED || job.getStatus().getId() == Job.STATUS_CLOSED || job.getStatus().getId() == Job.STATUS_FINISHED) {
                    if (job.getFeedback() != null) {
                        feedbackSum += job.getFeedback().getTotal();
                    }
                    completedJobs++;
                }
            }
            if (completedJobs > 0) {
                e.setJobSuccess(feedbackSum / completedJobs);
            }
            e.setJobs(jobs.size());
        }
        return e;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getJobs() {
        return jobs;
    }

    public void setJobs(int jobs) {
        this.jobs = jobs;
    }

    public double getJobSuccess() {
        return jobSuccess;
    }

    public void setJobSuccess(double jobSuccess) {
        this.jobSuccess = jobSuccess;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public double getLastYearEarnings() {
        return lastYearEarnings;
    }

    public void setLastYearEarnings(double lastYearEarnings) {
        this.lastYearEarnings = lastYearEarnings;
    }

    public double getLifetimeEarnings() {
        return lifetimeEarnings;
    }

    public void setLifetimeEarnings(double lifetimeEarnings) {
        this.lifetimeEarnings = lifetimeEarnings;
    }
    
    public void setLifetimeEarnings(BigDecimal lifetimeEarnings) {
        this.lifetimeEarnings = lifetimeEarnings != null ? lifetimeEarnings.doubleValue() : 0;
    }

    public double getAvailableHoursPerWeek() {
        return availableHoursPerWeek;
    }

    public void setAvailableHoursPerWeek(double availableHoursPerWeek) {
        this.availableHoursPerWeek = availableHoursPerWeek;
    }

    public int getAvailabilityStart() {
        return availabilityStart;
    }

    public void setAvailabilityStart(int availabilityStart) {
        this.availabilityStart = availabilityStart;
    }

    public int getAvailabilityEnd() {
        return availabilityEnd;
    }

    public void setAvailabilityEnd(int availabilityEnd) {
        this.availabilityEnd = availabilityEnd;
    }

    public ESPlace getPlace() {
        return place;
    }

    public void setPlace(ESPlace place) {
        this.place = place;
    }

    public List<ESSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<ESSkill> skills) {
        this.skills = skills;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ESLanguage> getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(List<ESLanguage> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public List<ESBillingMethod> getBillingMethods() {
        return billingMethods;
    }

    public void setBillingMethods(List<ESBillingMethod> billingMethods) {
        this.billingMethods = billingMethods;
    }

}
