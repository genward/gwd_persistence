/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

/**
 *
 * @author fnunez
 */
public class ESBillingMethod {

    private Long id;
    private String method;

    public ESBillingMethod() {
    }

    private ESBillingMethod(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public ESBillingMethod(Long id, String method) {
        this.id = id;
        this.method = method;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

}
