/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Publisher;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sergio
 */
public class ESPublisher {

    
    private Long id;
    private String name;
    private Date updated;

    public ESPublisher(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ESPublisher() {
    }

    public ESPublisher(Long id) {
        this.id = id;
    }

    public ESPublisher(String publisher) {
        this.name = publisher;
    }

    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static ESPublisher fromPublisher(Publisher publisher) {
        if (publisher != null) {
            ESPublisher esp = new ESPublisher();
            
            esp.setId(publisher.getId());
            esp.setName(publisher.getName());
            esp.setUpdated(publisher.getUpdated());
            
            return esp;
        }
        return null;
    }
    
    public static List<ESPublisher> fromList(List<Publisher> publishers) {
        if (publishers != null) {
            List<ESPublisher> esp = new ArrayList();
            publishers.forEach((p) -> {
                if (p != null) {
                    esp.add(ESPublisher.fromPublisher(p));
                }
            });
            return esp;
        }
        return null;
    }
}
