/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.catalog.LanguagesCatalog;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fnunez
 */
public class ESLanguage {

    private Long id;
    private String language;

    public ESLanguage() {
    }

    public ESLanguage(long id, String language) {
        this.id = id;
        this.language = language;
    }

    public ESLanguage(String language) {
        this.language = language;
    }

    private ESLanguage(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public static List<ESLanguage> fromArray(String[] languages) {
        if (languages != null) {
            List<ESLanguage> sm = new ArrayList();
            for (String name : languages) {
                sm.add(new ESLanguage(name));
            }
            return sm;
        }
        return null;
    }

    public static List<ESLanguage> fromArray(Long[] lang_ids) {
        if (lang_ids != null) {
            List<ESLanguage> sm = new ArrayList();
            for (Long id : lang_ids) {
                sm.add(new ESLanguage(id));
            }
            return sm;
        }
        return null;
    }

    public static List<ESLanguage> fromList(Set<LanguagesCatalog> languages) {
        if (languages != null) {
            List<ESLanguage> langs = new ArrayList();
            languages.forEach((s) -> {
                if (s != null) {
                    langs.add(ESLanguage.fromLanguage(s));
                }
            });
            return langs;
        }
        return null;
    }

    public static ESLanguage fromLanguage(LanguagesCatalog language) {
        if (language != null) {
            ESLanguage lan = new ESLanguage();
            lan.setId(language.getId());
            lan.setLanguage(language.getName());
            return lan;
        }
        return null;
    }

}
