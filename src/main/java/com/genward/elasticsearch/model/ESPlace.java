/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Place;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.geo.Point;

/**
 *
 * @author sergio
 */
public class ESPlace {

    private Long id;
    private String name;
    private String type;
    private Date updated;

    @GeoPointField
    private GeoPoint location;

    @Field(type = FieldType.Object)
    private List<ESAltname> altnames;

    public ESPlace(long id, String name, String type, GeoPoint location, List<ESAltname> altnames) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.location = location;
        this.altnames = altnames;
    }

    public ESPlace() {
    }

    public ESPlace(String name) {
        this.name = name;
    }

    public ESPlace(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public void setLocation(Float latitude, Float longitude) {
        this.location = GeoPoint.fromPoint(new Point(latitude, longitude));
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ESAltname> getAltnames() {
        return altnames;
    }

    public void setAltnames(List<ESAltname> altnames) {
        this.altnames = altnames;
    }

    public void addAltname(ESAltname altname) {
        if (this.altnames == null) {
            this.altnames = new ArrayList();
        }
        this.altnames.add(altname);
    }

    public static List<ESPlace> fromArray(String[] places) {
        if (places != null) {
            List<ESPlace> ps = new ArrayList();
            for (String name : places) {
                ps.add(new ESPlace(name));
            }
            return ps;
        }
        return null;
    }

    public static List<ESPlace> fromArray(Long[] place_ids) {
        if (place_ids != null) {
            List<ESPlace> ps = new ArrayList();
            for (Long id : place_ids) {
                ps.add(new ESPlace(id));
            }
            return ps;
        }
        return null;
    }

    public static ESPlace fromPlace(Place place) {
        if (place != null) {
            ESPlace pc = new ESPlace();

            pc.setId(place.getId());
            pc.setName(place.getName());
            if (place.getLatitude() != null && place.getLongitude() != null) {
                pc.setLocation(new GeoPoint(place.getLatitude(), place.getLongitude()));
            }

            if (place.getType() != null) {
                pc.setType(place.getType().getName());
            }
            pc.setUpdated(place.getUpdated());
            pc.setAltnames(ESAltname.fromList(place.getAlt_names()));

            return pc;
        }
        return null;
    }

    public static List<ESPlace> fromList(List<Place> places) {
        if (places != null) {
            List<ESPlace> esplaces = new ArrayList();
            places.forEach((pc) -> {
                if (pc != null) {
                    esplaces.add(ESPlace.fromPlace(pc));
                }

            });
            return esplaces;
        }
        return null;
    }

    public static List<ESPlace> fromList(Set<Place> places) 
    {
        if (places != null) 
        {
            List<ESPlace> esplaces = new ArrayList<ESPlace>();
            places.forEach((pc) -> {
                if (pc != null) 
                    esplaces.add(ESPlace.fromPlace(pc));
            });
            
            return esplaces;
        }
        
        return null;
    }
}
