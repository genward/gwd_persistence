/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.genward.persistence.model.Job;
import java.util.Date;
import org.springframework.data.annotation.Id;

/**
 *
 * @author fnunez
 */
public class ESJobMessageOffer {

    @Id
    private long jobid;

    private String name;

    private String description;
    
    private long status_id;

    private double amount;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date due_date;

    public long getJobid() {
        return jobid;
    }

    public void setJobid(long jobid) {
        this.jobid = jobid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public long getStatus_id() {
        return status_id;
    }

    public void setStatus_id(long status_id) {
        this.status_id = status_id;
    }
    
    public static ESJobMessageOffer fromJob(Job job) {
        if (job != null) {
            ESJobMessageOffer esJobOffer = new ESJobMessageOffer();
            esJobOffer.setJobid(job.getId());
            esJobOffer.setAmount(job.getAmount());
            esJobOffer.setDescription(job.getDescription());
            esJobOffer.setDue_date(job.getDue_date());
            esJobOffer.setName(job.getName());
            esJobOffer.setStatus_id(job.getStatus().getId());
            return esJobOffer;
        }
        return null;
    }

}
