/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.Skill;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fnunez
 */
public class ESJobSkill {

    private String name;

    private long id;

    private String type;

    private long type_id;

    public ESJobSkill() {
    }

    public ESJobSkill(String name, String type, long skill_id, long type_id) {
        this.name = name;
        this.type = type;

        this.id = skill_id;
        this.type_id = type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getType_id() {
        return type_id;
    }

    public void setType_id(long type_id) {
        this.type_id = type_id;
    }

    static List<ESJobSkill> fromSkillsList(Set<Skill> skills) {
        if (skills != null && !skills.isEmpty()) {
            List<ESJobSkill> esSkills = new ArrayList<>();
            for (Skill skill : skills) {
                esSkills.add(ESJobSkill.fromSkill(skill));
            }
            return esSkills;
        }
        return null;
    }

    private static ESJobSkill fromSkill(Skill skill) {
        if (skill != null) {
            return new ESJobSkill(skill.getName(), skill.getType().getName(), skill.getId(), skill.getType().getId());
        }
        return null;
    }

}
