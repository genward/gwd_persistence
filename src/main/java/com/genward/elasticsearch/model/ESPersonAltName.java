/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.PeopleAltNames;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fnunez
 */
public class ESPersonAltName {

    private String name;
    private String prefix;
    private String surname;
    private String type;

    public ESPersonAltName() {
    }

    public ESPersonAltName(String name, String prefix, String surname, String type) {
        this.name = name;
        this.prefix = prefix;
        this.surname = surname;
        this.type = type;
    }

    public static ESPersonAltName fromPeopleAltName(PeopleAltNames altName) {
        if (altName != null) {
            return new ESPersonAltName(altName.getName(), altName.getPrefix(), altName.getSurname(), altName.getType());
        }
        return null;
    }

    public static List<ESPersonAltName> fromList(List<PeopleAltNames> altnames) {
        if (altnames != null) {
            List<ESPersonAltName> list = new ArrayList<>();
            for (PeopleAltNames altname : altnames) {
                list.add(ESPersonAltName.fromPeopleAltName(altname));
            }
            return list;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
