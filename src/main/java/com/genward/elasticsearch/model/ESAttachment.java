/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.JobDocument;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fnunez
 */
public class ESAttachment {

    private long id;
    private String hashed_name;
    private String original_name;
    private String content_type;
    private long size;

    public String getHashed_name() {
        return hashed_name;
    }

    public void setHashed_name(String hashed_name) {
        this.hashed_name = hashed_name;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public static List<ESAttachment> fromJobDocuments(Set<JobDocument> documents) {
        if (documents != null && !documents.isEmpty()) {
            List<ESAttachment> attachments = new ArrayList<>();
            for (JobDocument document : documents) {
                attachments.add(ESAttachment.fromDocument(document));
            }
            return attachments;
        }
        return null;
    }

    private static ESAttachment fromDocument(JobDocument document) {
        if (document != null) {
            ESAttachment attachment = new ESAttachment();
            attachment.setId(document.getId());
            attachment.setHashed_name(document.getHash_name());
            attachment.setContent_type(document.getContent_type());
            attachment.setOriginal_name(document.getOriginal_name());
            attachment.setSize(document.getSize());
            return attachment;
        }
        return null;
    }
}
