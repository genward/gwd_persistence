/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

/**
 *
 * @author sergio
 */
public interface BatchEntity {
    public long getId();
    public int getDeleted();
}
