/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.ItemFamilies;
import com.genward.persistence.model.ItemPeople;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fnunez
 */
public class ESCitation {

    private long itemid;
    private String title;
    private String pages;

    public ESCitation(){}
    
    public ESCitation(String title) {
        this.title = title;
    }

    public ESCitation(long itemid) {
        this.itemid = itemid;
    }

    public ESCitation(long itemid, String title, String pages) {
        this.itemid = itemid;
        this.title = title;
        this.pages = pages;
    }

    public static ESCitation fromItemPeople(ItemPeople itemPeople) {
        if (itemPeople != null) {
            return new ESCitation(itemPeople.getItem().getId(), itemPeople.getItem().getTitle(), itemPeople.getPages());
        }
        return null;
    }

    public static List<ESCitation> fromList(List<ItemPeople> itemPeopleList) {
        if (itemPeopleList != null) {
            List<ESCitation> citations = new ArrayList<>();
            for (ItemPeople ip : itemPeopleList) {
                citations.add(ESCitation.fromItemPeople(ip));
            }
            return citations;
        }
        return null;
    }

    private static ESCitation fromItemFamilies(ItemFamilies itFam) {
        if (itFam != null) {
            return new ESCitation(itFam.getItem().getId(), itFam.getItem().getTitle(), itFam.getPages());
        }
        return null;
    }

    public static List<ESCitation> fromItemFamiliesList(List<ItemFamilies> itemFamiliesList) {
        if (itemFamiliesList != null) {
            List<ESCitation> citations = new ArrayList<>();
            for (ItemFamilies itFam : itemFamiliesList) {
                citations.add(ESCitation.fromItemFamilies(itFam));
            }
            return citations;
        }
        return null;
    }

    public long getItemid() {
        return itemid;
    }

    public void setItemid(long itemid) {
        this.itemid = itemid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

}
