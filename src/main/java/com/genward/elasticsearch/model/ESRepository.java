/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.genward.persistence.model.Repositories;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.geo.Point;

/**
 *
 * @author sergio
 */
@Document(indexName = "genealogy_catalog", type = "repositories", createIndex = false)
public class ESRepository implements SpeedEntity {

    @Id
    private String id;
    
    private String name;
    private String address;
    private String zip;
    private String website;
    private String availability;
    private String phone;
    private Date updated;
    private Date created;
    private Long user_id;
    
    @Field( type = FieldType.Object)
    private ESPlace place;
    @GeoPointField
    private GeoPoint location;


    public ESRepository() {
    }
    
    public static ESRepository fromRepository (Repositories repository) {
        ESRepository esr = new ESRepository();
        
        esr.setId(repository.getId());
        esr.setAddress(repository.getAddress());
        esr.setName(repository.getName());
        if (repository.getLatitude() != null && repository.getLongitude() != null) {
            esr.setLocation(new GeoPoint(repository.getLatitude(), repository.getLongitude()));
        }
        esr.setWebsite(repository.getWebsite());
        esr.setZip(repository.getZip());
        esr.setPhone(repository.getPhone());
        esr.setPlace(ESPlace.fromPlace(repository.getPlace()));
        esr.setAvailability(repository.getAvailability().getName());
        esr.setUpdated(repository.getUpdated());
        esr.setCreated(repository.getCreated());
        if (repository.getUser()!=null){
            esr.setUser_id(repository.getUser().getId());
        }
        return esr;
    }

    public ESRepository(Long id) {
        this.id = String.valueOf(id);
    }

    public ESRepository(String repository) {
        this.name =  repository;
    }


    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void setId(Long id) {
        this.id = String.valueOf(id);
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }


    public ESPlace getPlace() {
        return place;
    }

    public void setPlace(ESPlace place) {
        this.place = place;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }
    
    
    
    @JsonIgnore
    public void setLocation(Float latitude, Float longitude) {
        this.location = GeoPoint.fromPoint(new Point(latitude, longitude));
    }
    
    
    public static List<ESRepository> fromList(List<Repositories> repositories) {
        if (repositories != null) {
            List<ESRepository> repo = new ArrayList();
            repositories.forEach((rp) -> {
                if (rp != null) {
                    repo.add(ESRepository.fromRepository(rp));
                }
                
            });
            return repo;
        }
        return null;
    }
    
    @JsonIgnore
    @Override
    @Deprecated
    public boolean equals(Object comparer) {
        
        if (comparer == null) return false;
        if (comparer instanceof Repositories) {
            Repositories repo = (Repositories)comparer;
            if (this.id != null && Long.valueOf(this.id) != repo.getId()) return false;
            if (repo.getUpdated() == null) return false;
            return (
                this.updated != null && this.updated.getTime() == repo.getUpdated().getTime()
            );
        }
        return super.equals(comparer);
    }
    
    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        if (bentity == null) return false;
        if (bentity instanceof Repositories) {
            Repositories repo = (Repositories)bentity;
            if (this.id != null && Long.valueOf(this.id) != repo.getId()) return false;
            if (repo.getUpdated() == null) return false;
            return (
                this.updated != null && this.updated.getTime() == repo.getUpdated().getTime()
            );
        }
        return false;
    }

    @Override
    public String getIndexName() {
        return "genealogy_catalog";
    }

    @Override
    public String getTypeName() {
        return "repositories";
    }

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        return ESRepository.fromRepository((Repositories) bentity);
    }
    
    @Override
    @JsonIgnore
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ESItem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    
}
