/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.genward.persistence.model.Families;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author fnunez
 */
@Document(indexName = "genealogy_catalog", type = "families", createIndex = false)
public class ESFamily implements SpeedEntity {

    @Id
    private String id;

    @Field(type = FieldType.Object)
    private ESRelative wife;

    @Field(type = FieldType.Object)
    private ESRelative husband;

    @Field(type = FieldType.Object)
    private List<ESRelative> children;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date marriagedate;

    @Field(type = FieldType.Object)
    private ESPlace marriageplace;

    @Field(type = FieldType.Object)
    private List<ESCitation> citations;

    private Boolean deleted;
    private Date created;
    private Date updated;

    public ESFamily() {
    }

    public ESFamily(long id, ESRelative wife, ESRelative husband, List<ESRelative> children, Date marriagedate, ESPlace marriageplace, List<ESCitation> citations, Boolean deleted, Date created, Date updated) {
        this.id = String.valueOf(id);
        this.wife = wife;
        this.husband = husband;
        this.children = children;
        this.marriagedate = marriagedate;
        this.marriageplace = marriageplace;
        this.citations = citations;
        this.deleted = deleted;
        this.created = created;
        this.updated = updated;
    }

    public static ESFamily fromFamilies(Families families) {
        if (families != null) {
            return new ESFamily(families.getId(),
                    ESRelative.fromPeople(families.getWife()),
                    ESRelative.fromPeople(families.getHusband()),
                    ESRelative.fromList(families.getChildren()),
                    families.getMarriageDate(),
                    ESPlace.fromPlace(families.getMarriagePlace()),
                    ESCitation.fromItemFamiliesList(families.getCitations()),
                    families.getDeleted() == 1,
                    families.getCreated(),
                    families.getUpdated());
        }
        return null;
    }

    public static List<ESFamily> fromList(List<Families> famList) {
        if (famList != null) {
            List<ESFamily> fams = new ArrayList<>();
            for (Families f : famList) {
                fams.add(ESFamily.fromFamilies(f));
            }
            return fams;
        }
        return null;
    }

    public ESRelative getWife() {
        return wife;
    }

    public void setWife(ESRelative wife) {
        this.wife = wife;
    }

    public ESRelative getHusband() {
        return husband;
    }

    public void setHusband(ESRelative husband) {
        this.husband = husband;
    }

    public List<ESRelative> getChildren() {
        return children;
    }

    public void setChildren(List<ESRelative> children) {
        this.children = children;
    }

    public Date getMarriagedate() {
        return marriagedate;
    }

    public void setMarriagedate(Date marriagedate) {
        this.marriagedate = marriagedate;
    }

    public ESPlace getMarriageplace() {
        return marriageplace;
    }

    public void setMarriageplace(ESPlace marriageplace) {
        this.marriageplace = marriageplace;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ESCitation> getCitations() {
        return citations;
    }

    public void setCitations(List<ESCitation> citations) {
        this.citations = citations;
    }

    @Override
    public String getIndexName() {
        return "genealogy_catalog";
    }

    @Override
    public String getTypeName() {
        return "families";
    }

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        return ESFamily.fromFamilies((Families) bentity);
    }

    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        if (bentity == null) {
            return false;
        }
        if (bentity instanceof Families) {
            Families family = (Families) bentity;
            if (this.id != null && Long.valueOf(this.id) != family.getId()) {
                return false;
            }
            if (family.getUpdated() == null) {
                return false;
            }
            return (this.updated != null && this.updated.getTime() == family.getUpdated().getTime());
        }
        return false;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
