/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.genward.persistence.model.People;
import com.genward.persistence.model.PeoplePlaces;
import com.genward.persistence.model.Surname;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author fnunez
 */
public class ESRelative {

    private long id;
    private String name;
    private String prefix;
    @Field(type = FieldType.Object)
    private ESSurname surname;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;
    @Field(type = FieldType.Object)
    private ESPlace birthplace;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deathdate;
    @Field(type = FieldType.Object)
    private ESPlace deathplace;
    private Long yearsLived;

    public ESRelative() {
    }

    public ESRelative(String name) {
        this.name = name;
    }

    public ESRelative(String name, ESSurname surname) {
        this.name = name;
        this.surname = surname;
    }

    public ESRelative(long id, String name, Surname surname, String prefix, Date birthdate, Date deathdate) {
        this.id = id;
        this.surname = ESSurname.fromSurname(surname);
        this.name = name;
        this.prefix = prefix;
        this.birthdate = birthdate;
        this.deathdate = deathdate;
    }

    public static ESRelative fromPeople(People people) {
        if (people != null) {
            ESRelative relative = new ESRelative(people.getId(), people.getName(), people.getSurname(), people.getPrefix(), people.getBirth(), people.getDeath());
            // SET yearsLived
            if (people.getBirth() != null && people.getDeath() != null) {
                long reachedAge = Period.between(people.getBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), people.getDeath().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).getYears();
                relative.setYearsLived(reachedAge);
            }
            // SET birth and death places when given
            if (people.getFactPlaces() != null) {
                for (PeoplePlaces pp : people.getFactPlaces()) {
                    if (pp.getType().getName() != null && pp.getType().getName().equals("Birth")) {
                        relative.setBirthplace(ESPlace.fromPlace(pp.getPlace()));
                    }
                    if (pp.getType().getName() != null && pp.getType().getName().equals("Death")) {
                        relative.setDeathplace(ESPlace.fromPlace(pp.getPlace()));
                    }
                }
            }
            return relative;
        }
        return null;
    }

    public static List<ESRelative> fromList(List<People> peopleList) {
        if (peopleList != null) {
            List<ESRelative> relatives = new ArrayList<>();
            for (People p : peopleList) {
                relatives.add(ESRelative.fromPeople(p));
            }
            return relatives;
        }
        return null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public ESSurname getSurname() {
        return surname;
    }

    public void setSurname(ESSurname surname) {
        this.surname = surname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public ESPlace getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(ESPlace birthplace) {
        this.birthplace = birthplace;
    }

    public Date getDeathdate() {
        return deathdate;
    }

    public void setDeathdate(Date deathdate) {
        this.deathdate = deathdate;
    }

    public ESPlace getDeathplace() {
        return deathplace;
    }

    public void setDeathplace(ESPlace deathplace) {
        this.deathplace = deathplace;
    }

    public Long getYearsLived() {
        return yearsLived;
    }

    public void setYearsLived(Long yearsLived) {
        this.yearsLived = yearsLived;
    }

}
