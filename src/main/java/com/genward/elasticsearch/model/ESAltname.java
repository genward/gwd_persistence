/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.AltNames;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sergio
 */
public class ESAltname {

    

    
    private long id;
    private String name;
    private String source;
    private Date start_date;
    private Date end_date;
    private Date updated;

    public ESAltname() {
    }
    
    

    public ESAltname(long id, String name, String source, Date start_date, Date end_date) {
        this.id = id;
        this.name = name;
        this.source = source;
        this.start_date = start_date;
        this.end_date = end_date;
    }
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
    
    public static ESAltname fromAltname(AltNames altname) {
        if (altname != null) {
            ESAltname esp = new ESAltname();
            
            esp.setId(altname.getId());
            esp.setName(altname.getName());
            esp.setUpdated(altname.getUpdated());
            
            return esp;
        }
        return null;
    }
    
    public static List<ESAltname> fromList(List<AltNames> alt_names) {
        if (alt_names != null) {
            List<ESAltname> esp = new ArrayList();
            alt_names.forEach((p) -> {
                esp.add(ESAltname.fromAltname(p));
            });
            return esp;
        }
        return null;
    }
}
