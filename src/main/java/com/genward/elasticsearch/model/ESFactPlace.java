/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.genward.persistence.model.PeoplePlaces;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author fnunez
 */
public class ESFactPlace {

    @Field(type = FieldType.Object)
    private ESPlace place;

    private String factType;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    public ESFactPlace() {
    }

    public ESFactPlace(ESPlace place, String factType, Date date) {
        this.place = place;
        this.factType = factType;
        this.date = date;
    }

    public ESPlace getPlace() {
        return place;
    }

    public void setPlace(ESPlace place) {
        this.place = place;
    }

    public String getFactType() {
        return factType;
    }

    public void setFactType(String factType) {
        this.factType = factType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public static ESFactPlace fromPeoplePlaces(PeoplePlaces pp) {
        if (pp != null) {
            return new ESFactPlace(ESPlace.fromPlace(pp.getPlace()), pp.getType().getName(), pp.getDate());
        }
        return null;
    }

    public static List<ESFactPlace> fromList(List<PeoplePlaces> pp) {
        if (pp != null) {
            List<ESFactPlace> places = new ArrayList<>();
            for (PeoplePlaces p : pp) {
                places.add(ESFactPlace.fromPeoplePlaces(p));
            }
            return places;
        }
        return null;
    }

}
