/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.request.ChatRequest;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author fnunez
 */
@Document(indexName = "gpchat", type = "messages")
public class ESChatMessage implements SpeedEntity {
    
    // CONSTANTS
    public static final int TYPE_TEXT_ONLY = 1;
    public static final int TYPE_WITH_ATTACHMENTS = 2;
    public static final int TYPE_JOB_OFFER = 3;
    public static final int STATUS_PENDING_UPLOAD = 0;
    public static final int STATUS_OK_UPLOADED = 1;
    public static final int STATUS_ERROR = 2;

    @Id
    private String id;
    private long conversation_id;
    private long consecutive;
    private long sender_id;
    private String sender_name;
    private Date sent;
    private Date read;
    private int type;
    private String message;
    private String attachment_hashedName;
    private String attachment_originalName;
    private String attachment_contentType;    
    private long attachment_size;
    private int attachment_status;
    @Field(type = FieldType.Object)
    private ESJobMessageOffer job;

    @Override
    public String getIndexName() {
        return "gpchat";
    }

    @Override
    public String getTypeName() {
        return "messages";
    }

    @Override
    public String getId() {
        return id;
    }

    public ESChatMessage() {
    }

    public ESChatMessage(String id, long conversation_id, long sender_id, String sender_name, Date sent, String message, String attachment_hashedName, String attachment_originalName, String attachment_contentType, long attachment_size) {
        this.id = id;
        this.conversation_id = conversation_id;
        this.sender_id = sender_id;
        this.sender_name = sender_name;
        this.sent = sent;
        this.message = message;
        this.attachment_hashedName = attachment_hashedName;
        this.attachment_originalName = attachment_originalName;
        this.attachment_contentType = attachment_contentType;
        this.attachment_size = attachment_size;
    }
    
    public static ESChatMessage fromChatRequest(ChatRequest chatRequest){
        if (chatRequest!=null){
            ESChatMessage chatMessage = new ESChatMessage();
            chatMessage.setConversation_id(chatRequest.getConversation_id());
            chatMessage.setMessage(chatRequest.getMessage());
            chatMessage.setSent(chatRequest.getPersistedDate());
            chatMessage.setSender_id(chatRequest.getFrom_id());           
            chatMessage.setAttachment_status(STATUS_PENDING_UPLOAD);
            chatMessage.setConsecutive(chatRequest.getConsecutive());
            chatMessage.setType(chatRequest.getType());
            return chatMessage;
        }
        return null;
    } 

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public long getConversation_id() {
        return conversation_id;
    }

    public void setConversation_id(long conversation_id) {
        this.conversation_id = conversation_id;
    }

    public long getSender_id() {
        return sender_id;
    }

    public void setSender_id(long sender_id) {
        this.sender_id = sender_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment_hashedName() {
        return attachment_hashedName;
    }

    public void setAttachment_hashedName(String attachment_hashedName) {
        this.attachment_hashedName = attachment_hashedName;
    }

    public String getAttachment_originalName() {
        return attachment_originalName;
    }

    public void setAttachment_originalName(String attachment_originalName) {
        this.attachment_originalName = attachment_originalName;
    }

    public String getAttachment_contentType() {
        return attachment_contentType;
    }

    public void setAttachment_contentType(String attachment_contentType) {
        this.attachment_contentType = attachment_contentType;
    }

    public long getAttachment_size() {
        return attachment_size;
    }

    public void setAttachment_size(long attachment_size) {
        this.attachment_size = attachment_size;
    }
    
     public long getConsecutive() {
        return consecutive;
    }

    public void setConsecutive(long consecutive) {
        this.consecutive = consecutive;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getRead() {
        return read;
    }

    public void setRead(Date read) {
        this.read = read;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAttachment_status() {
        return attachment_status;
    }

    public void setAttachment_status(int attachment_status) {
        this.attachment_status = attachment_status;
    }

    public ESJobMessageOffer getJob() {
        return job;
    }

    public void setJob(ESJobMessageOffer job) {
        this.job = job;
    }
    
}
