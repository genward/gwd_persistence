/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.genward.persistence.model.UserSkill;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fnunez
 */
public class ESSkill {

    private String name;
    
    private long id;

    private String type;
    
    private long type_id;

    private int jobs;

    private double rating;

    public ESSkill() {
    }

    public ESSkill(String name, String type, int jobs, double rating, long skill_id, long type_id) {
        this.name = name;
        this.type = type;
        this.jobs = jobs;
        this.rating = rating;
        this.id = skill_id;
        this.type_id = type_id;
    }
 
    private static ESSkill fromUserSkill(UserSkill s) {
        if (s != null) {
            return new ESSkill(s.getSkill().getName(), s.getSkill().getType().getName(), s.getJobs(), s.getRate_avg(), s.getSkill().getId(), s.getSkill().getType().getId());
        }
        return null;
    }

    public static List<ESSkill> fromUsrSkillList(List<UserSkill> userskills) {
        if (userskills != null) {
            List<ESSkill> esskills = new ArrayList<>();
            for (UserSkill s : userskills) {
                esskills.add(ESSkill.fromUserSkill(s));
            }
            return esskills;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getJobs() {
        return jobs;
    }

    public void setJobs(int jobs) {
        this.jobs = jobs;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getType_id() {
        return type_id;
    }

    public void setType_id(long type_id) {
        this.type_id = type_id;
    }

}
