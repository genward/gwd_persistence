/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.elasticsearch.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.genward.persistence.model.Items;
import com.genward.persistence.model.People;
import com.genward.persistence.model.Place;
import com.genward.persistence.model.Publisher;
import com.genward.persistence.model.Repositories;
import com.genward.persistence.model.Surname;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;



/**
 *
 * @author sergio
 */
@Document(indexName = "genealogy_catalog", type = "items", createIndex = false)
public class ESItem implements SpeedEntity{
    
    private static final double GEO_TOLERANCE = 0.00001;
    
    @Id
    private String id;
    private String title;
    private String subtitle;
    private String language;
    private String citation;
    private String type;
    private String subject;
    private String series_name;
    
    private long user_id;
    
    private Boolean deleted;
    
    private Date created;
    private Date updated;
    
    
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date issued;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date start_date;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date end_date;
    
    @Field( type = FieldType.Object)
    private List<ESRepository> repositories;
    @Field( type = FieldType.Object)
    private List<ESPlace> places;
    //@Field( type = FieldType.Object)
    //private List<ESPerson> authors;
    private String authors;
    @Field( type = FieldType.Object)
    private List<ESSurname> surnames;
    @Field( type = FieldType.Object)
    private List<ESPublisher> publishers;
    
    
    public ESItem() {
    }
    
    public static ESItem fromItem(Items item, Repositories repository) {
        ESItem esitem = fromItem(item);
        esitem.addRepository(ESRepository.fromRepository(repository));
        
        return esitem;
    }
    
    public static ESItem fromItem (Items item) {
        ESItem esitem = new ESItem();
        
        esitem.setId(String.valueOf(item.getId()));
        esitem.setTitle(item.getTitle());
        esitem.setSubtitle(item.getSubtitle());
        esitem.setLanguage(item.getLanguage());
        esitem.setCitation(item.getCitation());
        esitem.setCreated(item.getCreated());
        esitem.setIssued(item.getIssued());
        esitem.setStart_date(item.getStart_date());
        esitem.setEnd_date(item.getEnd_date());
        esitem.setType(item.getType().getName());
        esitem.setSubject(item.getSubject().getName());
        esitem.setUpdated(item.getUpdated());
        esitem.setAuthors(item.getAuthor());
        esitem.setSeries_name(item.getSeries_name());
        
        if (item.getUser() != null) {
            esitem.setUser_id(item.getUser().getId());
        } 
        
        esitem.setPlaces(ESPlace.fromList(item.getPlacesAsList()));
        //esitem.setAuthors(ESPerson.fromList(item.getPeopleAsList()));
        esitem.setPublishers(ESPublisher.fromList(item.getPublishersAsList()));
        esitem.setSurnames(ESSurname.fromList(item.getSurnamesAsList()));
        esitem.setRepositories(ESRepository.fromList(item.getRepositoriesAsList()));
        
        return esitem;
    }

    public ESItem(long id) {
        this.id = String.valueOf(id);
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getSeries_name() {
        return series_name;
    }

    public void setSeries_name(String series_name) {
        this.series_name = series_name;
    }
    
    
    
    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getIssued() {
        return issued;
    }

    public void setIssued(Date issued) {
        this.issued = issued;
    }

    public String getCitation() {
        return citation;
    }

    public void setCitation(String citation) {
        this.citation = citation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<ESRepository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<ESRepository> repositories) {
        this.repositories = repositories;
    }

    public List<ESPlace> getPlaces() {
        return places;
    }

    public void setPlaces(List<ESPlace> places) {
        this.places = places;
    }

    /*public List<ESPerson> getAuthors() {
        return authors;
    }

    public void setAuthors(List<ESPerson> authors) {
        this.authors = authors;
    }*/
    
    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public List<ESSurname> getSurnames() {
        return surnames;
    }

    public void setSurnames(List<ESSurname> surname) {
        this.surnames = surname;
    }

    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<ESPublisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<ESPublisher> publishers) {
        this.publishers = publishers;
    }

    
    
    
    
    
    
    @JsonIgnore
    public void addRepository(ESRepository repository) {
        if (this.repositories == null) {
            this.repositories = new ArrayList();
        }
        this.repositories.add(repository);
    }  
    
    /*@JsonIgnore
    public void addAuthor(ESPerson author) {
        if (this.authors == null) {
            this.authors = author.getName() + " " + author.getSurname();//new ArrayList();
        }
        else
        {
        	this.authors += ", " + author.getName() + " " + author.getSurname();//.add(author);
        }
    }*/
    
    @JsonIgnore
    public void addPlace(ESPlace coveredPlace) {
        if (this.places == null) {
            this.places = new ArrayList();
        }
        this.places.add(coveredPlace);
    }
    
    @JsonIgnore
    public void addSurname(ESSurname coveredSurname) {
        if (this.surnames == null) {
            this.surnames = new ArrayList();
        }
        this.surnames.add(coveredSurname);
    }
    
    @JsonIgnore
    public void addPublisher(ESPublisher esPublisher) {
        if (this.publishers == null) {
            this.publishers = new ArrayList();
        }
        this.publishers.add(esPublisher);
    }
    
    @JsonIgnore
    @Override
    @Deprecated
    public boolean equals(Object comparer) {
        
        if (comparer == null) return false;
        if (comparer instanceof Items) {
            Items item = (Items)comparer;
            if (this.id != null && Long.valueOf(this.id) != item.getId()) return false;
            if ((this.getDeleted() == null || !this.getDeleted()) && item.getDeleted() > 0) return false;
            if (item.getUpdated() == null) return false;
            return (
                this.updated != null && this.updated.getTime() == item.getUpdated().getTime() &&
                this.equalRepositories(item.getRepositoriesAsList()) &&
                this.equalPlaces(item.getPlacesAsList()) &&
                this.equalPublishers(item.getPublishersAsList()) &&
                this.equalSurnames(item.getSurnamesAsList()) //&&
                //this.equalAuthors(item.getPeopleAsList())
            );
        }
        return super.equals(comparer);
    }
    
    @Override
    public Boolean isSameAs(BatchEntity bentity) {
        if (bentity == null) return false;
        if (bentity instanceof Items) {
            Items item = (Items)bentity;
            if (this.id != null && Long.valueOf(this.id) != item.getId()) return false;
            if (item.getUpdated() == null) return false;
            return (
                this.updated != null && this.updated.getTime() == item.getUpdated().getTime()
            );
        }
        return false;
    }
    
    @JsonIgnore
    public boolean equalRepositories(List<Repositories> repositories) {
        if (this.repositories == null || repositories == null) return false;
        if (this.repositories.size() != repositories.size()) return false;
        
        for(int i = 0; i < this.repositories.size(); i++) {
            if (this.repositories.get(i) == null || repositories.get(i) == null) return false;
            if (this.repositories.get(i).getUpdated() == null || repositories.get(i) == null) return false;
            if (this.repositories.get(i).getUpdated().getTime() != repositories.get(i).getUpdated().getTime()) return false;
        }
        return true;
    }
    
    /*@JsonIgnore
    public boolean equalAuthors(List<People> authors) {
        if (this.authors == null || authors == null) return false;
        if (this.authors.size() != authors.size()) return false;
        
        for(int i = 0; i < this.authors.size(); i++) {
            if (this.authors.get(i) == null || authors.get(i) == null) return false;
            if (this.authors.get(i).getUpdated() == null || authors.get(i).getUpdated() == null) return false;
            if (this.authors.get(i).getUpdated().getTime() != authors.get(i).getUpdated().getTime()) return false;
        }
        return true;
    }*/
    
    @JsonIgnore
    public boolean equalPlaces(List<Place> places) {
        if (this.places == null || places == null) return false;
        if (this.places.size() != places.size()) return false;
        
        for(int i = 0; i < this.places.size(); i++) {
            if (this.places.get(i) == null || places.get(i) == null) return false;
            if (this.places.get(i).getUpdated() == null || places.get(i) == null) return false;
            if (this.places.get(i).getUpdated().getTime() != places.get(i).getUpdated().getTime()) return false;
        }
        return true;
    }
    
    @JsonIgnore
    public boolean equalSurnames(List<Surname> surnames) {
        if (this.surnames == null || surnames == null) return false;
        if (this.surnames.size() != surnames.size()) return false;
        
        for(int i = 0; i < this.surnames.size(); i++) {
            if (this.surnames.get(i) == null || surnames.get(i) == null) return false;
            if (this.surnames.get(i).getUpdated() == null || surnames.get(i) == null) return false;
            if (this.surnames.get(i).getUpdated().getTime() != surnames.get(i).getUpdated().getTime()) return false;
        }
        return true;
    }
    
    @JsonIgnore
    public boolean equalPublishers(List<Publisher> publisher) {
        if (this.publishers == null || publisher == null) return false;
        if (this.publishers.size() != publisher.size()) return false;
        
        for(int i = 0; i < this.publishers.size(); i++) {
            if (this.publishers.get(i) == null || publishers.get(i) == null) return false;
            if (this.publishers.get(i).getUpdated() == null || publishers.get(i) == null) return false;
            if (this.publishers.get(i).getUpdated().getTime() != publishers.get(i).getUpdated().getTime()) return false;
        }
        return true;
    }
    
    
    
    
    @Override
    @JsonIgnore
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ESItem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    @JsonIgnore
    public String getIndexName() {
        return "genealogy_catalog";
    }

    @Override
    @JsonIgnore
    public String getTypeName() {
        return "items";
    }

    @Override
    public SpeedEntity fromBatchEntity(BatchEntity bentity) {
        return ESItem.fromItem((Items)bentity);
    }
    
}
