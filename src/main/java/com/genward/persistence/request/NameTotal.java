/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.io.Serializable;

/**
 *
 * @author fnunez
 */
public class NameTotal implements Serializable{
    
    private String name;
    private Object total;

    public NameTotal() {
    }

    public NameTotal(String name, Object total) {
        this.name = name;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getTotal() {
        return total;
    }

    public void setTotal(Object total) {
        this.total = total;
    }
    
}
