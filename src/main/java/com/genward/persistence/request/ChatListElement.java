/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import com.genward.persistence.model.Conversation;
import java.util.Date;

/**
 *
 * @author fnunez
 */
public class ChatListElement {
    
    private Long conversation_id;
    private Long to_user_id;
    private String to_user_name;
    private String lastMessagePreview;
    private Date lastMessageDate;
    private String lastMessageDay;
    private Long unread;
    private Long totalMessages;

    public ChatListElement() {
    }

    public ChatListElement(Long conversation_id, Long to_user_id, String to_user_name, String lastMessagePreview, Date lastMessageDate, String lastMessageDay) {
        this.conversation_id = conversation_id;
        this.to_user_id = to_user_id;
        this.to_user_name = to_user_name;
        this.lastMessagePreview = lastMessagePreview;
        this.lastMessageDate = lastMessageDate;
        this.lastMessageDay = lastMessageDay;
    }

    public ChatListElement(Long conversation_id, Long to_user_id, String to_user_name, String lastMessagePreview, Date lastMessageDate, String lastMessageDay, Long unread, Long totalMessages) {
        this.conversation_id = conversation_id;
        this.to_user_id = to_user_id;
        this.to_user_name = to_user_name;
        this.lastMessagePreview = lastMessagePreview;
        this.lastMessageDate = lastMessageDate;
        this.lastMessageDay = lastMessageDay;
        this.unread = unread;
        this.totalMessages = totalMessages;
    }

    public ChatListElement(Conversation c) {
        this.conversation_id = c.getId();
        this.lastMessageDate = c.getUpdated();
        this.lastMessagePreview = c.getLast_message();
        this.totalMessages = c.getMessage_count();
        this.to_user_id = c.getSender().getId();
        this.to_user_name = c.getSender().getPerson().getFirst_name()+" "+c.getSender().getPerson().getLast_name();
        this.unread = new Long(c.getUnread());
    }

    public Long getConversation_id() {
        return conversation_id;
    }

    public void setConversation_id(Long conversation_id) {
        this.conversation_id = conversation_id;
    }

    public Long getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(Long to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name(String to_user_name) {
        this.to_user_name = to_user_name;
    }

    public String getLastMessagePreview() {
        return lastMessagePreview;
    }

    public void setLastMessagePreview(String lastMessagePreview) {
        this.lastMessagePreview = lastMessagePreview;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public String getLastMessageDay() {
        return lastMessageDay;
    }

    public void setLastMessageDay(String lastMessageDay) {
        this.lastMessageDay = lastMessageDay;
    }
    
    
    public Long getUnread() {
        return unread;
    }

    public void setUnread(Long unread) {
        this.unread = unread;
    }

    public Long getTotalMessages() {
        return totalMessages;
    }

    public void setTotalMessages(Long totalMessages) {
        this.totalMessages = totalMessages;
    }

    @Override
    public String toString() {
        return "ChatListElement{" + "conversation_id=" + conversation_id + ", to_user_id=" + to_user_id + ", to_user_name=" + to_user_name + ", lastMessagePreview=" + lastMessagePreview + ", lastMessageDate=" + lastMessageDate + ", lastMessageDay=" + lastMessageDay + ", unread=" + unread + ", totalMessages=" + totalMessages + '}';
    }
    
}
