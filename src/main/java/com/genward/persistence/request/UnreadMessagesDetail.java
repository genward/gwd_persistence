/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.util.List;

/**
 *
 * @author fnunez
 */
public class UnreadMessagesDetail {
    
    private int unread_total;
    private List<ChatListElement> chats;

    public int getUnread_total() {
        return unread_total;
    }

    public void setUnread_total(int unread_total) {
        this.unread_total = unread_total;
    }

    public List<ChatListElement> getChats() {
        return chats;
    }

    public void setChats(List<ChatListElement> chats) {
        this.chats = chats;
    }

    @Override
    public String toString() {
        return "UnreadMessagesDetail{" + "unread_total=" + unread_total + ", chats=" + chats + '}';
    }
    
}
