/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.io.Serializable;

/**
 *
 * @author fnunez
 */
public class ChartPoint implements Serializable {

    private String section;
    private Object value;

    public ChartPoint() {
    }

    public ChartPoint(String section, Object value) {
        this.section = section;
        this.value = value;
    }

    public ChartPoint(Integer yearMonthSectionAsInteger, Object value) {
        if (yearMonthSectionAsInteger != null && yearMonthSectionAsInteger > 0) {
            String year = yearMonthSectionAsInteger.toString().substring(0, 4);
            this.section = year + "-" + Integer.parseInt(yearMonthSectionAsInteger.toString().substring(4));
        }
        this.value = value;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
