package com.genward.persistence.request;

import java.util.Date;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.genward.persistence.catalog.RepositoryAvailabilityCatalog;
import com.genward.persistence.model.Repositories;

public class RepositoriesMapping 
{
	private long id;
	private String name;
	private String address;
	private String phone;
	private String zip;
	private String website;
	private RepositoryAvailabilityCatalog availability;
	private PlaceMapping place;
	private Date created;
	private Date updated;
	private int deleted;
	private Double latitude;
	private Double longitude;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public RepositoryAvailabilityCatalog getAvailability() {
		return availability;
	}
	public void setAvailability(RepositoryAvailabilityCatalog availability) {
		this.availability = availability;
	}
	public PlaceMapping getPlace() {
		return place;
	}
	public void setPlace(PlaceMapping place) {
		this.place = place;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public int getDeleted() {
		return deleted;
	}
	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public void map(Repositories repositories)
	{
		this.id = repositories.getId();
		this.name = repositories.getName();
		this.address = repositories.getAddress();
		this.phone = repositories.getPhone();
		this.zip = repositories.getZip();
		this.website = repositories.getWebsite();
		this.availability = repositories.getAvailability();
		PlaceMapping p = new PlaceMapping();
		if(repositories.getPlace() != null)
			p.map(repositories.getPlace(), repositories.getPlace().getAlt_names());
		this.place = p;
		this.created = repositories.getCreated();
		this.updated = repositories.getUpdated();
		this.deleted = repositories.getDeleted();
		this.latitude = repositories.getLatitude();
		this.longitude = repositories.getLongitude();
	}
	
	public HashMap<String, Object> toHashMap()
	{
		HashMap<String,Object> hash = new HashMap<String,Object>();
		hash.put("id", id);
		hash.put("name", name);
		hash.put("address", address);
		hash.put("phone", phone);
		hash.put("zip", zip);
		hash.put("website", website);
		hash.put("availability", availability);
		hash.put("place", place.toHashMap());
		hash.put("latitude", latitude);
		hash.put("longitude", longitude);
		
		return hash;
	}
}
