package com.genward.persistence.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

public class Externalrequest {
        @ApiModelProperty(notes = "This is the email used in the request")
	private String username;
	private String error;
	private String success;
	private String redirect;
        @ApiModelProperty(notes = "This is the token to validate the request")
	private String token;
        @ApiModelProperty(notes = "This is the password used for the password recovery process")
	private String password;
        @ApiModelProperty(notes = "This is the confirmation of the password for the password recovery process")
	private String pass;
    private String actual_pass;
        private int[] repositories;
        /*Repository*/
        private String name;
        private String address;
        private String zip;
        private String website;
        private long avaliability_id;
        private String phone;
        /*Repository*/
        /*Place*/
        private String country;
        private String state;
        private String county;
        private String city;
        private Double latitude;
        private Double longitude;
        private int type_id;
        private String type_name;
        private String country_altname;
        private String state_altname;
        private String county_altname;
        private String city_altname;
        /*Place*/
	public Externalrequest () {
        
    }
    
    public Externalrequest(String username, String urlerror, String urlsuccess, String urlredirect) {
        this.username = username;
        this.error = urlerror;
        this.success = urlsuccess;
        this.redirect = urlredirect;
    }
    
    public Externalrequest(String token, String password, String pass) {
        this.token = token;
        this.password = password;
        this.pass = pass;
    }
	
    
    public Externalrequest(String username) {
        this.username = username;
    }
    
    public Externalrequest(String username, String urlerror, String urlsuccess, String token, String password, String verifyPassword) 
    {
        this.username = username;
        this.error = urlerror;
        this.success = urlsuccess;
        this.token = token;
        this.password = password;
        this.pass = verifyPassword;
    }
    /*Repository*/
    public String getName(){
        return this.name;
    }
    public String getAddress(){
        return this.address;
    }
    public String getZip(){
        return this.zip;
    }
    public String getWebsite(){
        return this.website;
    }
    public long getAvaliability_id(){
        return this.avaliability_id;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public void setZip(String zip){
        this.zip = zip;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public void setWebsite(String website){
        this.website = website;
    }
    public void setAvaliability_id(long avaliability_id){
        this.avaliability_id=avaliability_id;
    }
    /*Repository*/
    /*Place*/
    public String getCountry(){
        return this.country;
    }
    public String getCity(){
        return this.city;
    }
    public String getPhone(){
        return this.phone;
    }
    public String getState(){
        return this.state;
    }
    public String getCounty(){
        return this.county;
    }
    public Double getLatitude(){
        return this.latitude;
    }
    public Double getLongitude(){
        return this.longitude;
    }
    public int getType_id(){
        return this.type_id;
    }
    public void setCountry(String country){
        this.country=country;
    }
    public void setCity(String city){
        this.city=city;
    }
    public void setCounty(String county){
        this.county=county;
    }
    public void setState(String state){
        this.state = state;
    }
    public void setLatitude(Double latitude){
        this.latitude=latitude;
    }
    public void setLongitude(Double longitude){
        this.longitude = longitude;
    }
    public void setType_id(int type_id){
        this.type_id= type_id;
    }
    /*Place*/
    
    public String getPass() 
    {
		return pass;
	}
    public int[] getRepositories(){
        return repositories;
    }
	public String getToken() 
	{
		return token;
	}

	public String getUsername() 
	{
		return username;
	}
	
	public String getError()
	{
		return error;
	}
	
	public String getSuccess()
	{
		return success;
	}
	
	public String getRedirect()
	{
		return redirect;
	}
	
	public String getPassword()
	{
		return password;
	}
	@JsonIgnore
	public boolean isValidPassword()
	{
		return password.equals(pass);
	}

	public String getCountry_altname() {
		return country_altname;
	}

	public void setCountry_altname(String country_altname) {
		this.country_altname = country_altname;
	}

	public String getCounty_altname() {
		return county_altname;
	}

	public void setCounty_altname(String county_altname) {
		this.county_altname = county_altname;
	}

	public String getCity_altname() {
		return city_altname;
	}

	public void setCity_altname(String city_altname) {
		this.city_altname = city_altname;
	}

	public String getState_altname() {
		return state_altname;
	}

	public void setState_altname(String state_altname) {
		this.state_altname = state_altname;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public String getActual_pass() {
		return actual_pass;
	}

	public void setActual_pass(String actual_pass) {
		this.actual_pass = actual_pass;
	}
}
