package com.genward.persistence.request;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.genward.persistence.model.Profile;

public class ProfileMapping 
{
	private long id;
	private String s_id;
	private String first_name;
	private String last_name;
	private String email;
	private String phone1;
	private String phone_type1;
	private String phone2;
	private String phone_type2;
	private String street;
	private String zip_code;
	//private ArrayList<HashMap<String, Object>> place;
	
	public long getId() 
	{
		return id;
	}
	
	public void setId(long id) 
	{
		this.id = id;
	}
	
	public String getS_id() 
	{
		return s_id;
	}
	
	public void setS_id(String s_id)
	{
		this.s_id = s_id;
	}
	
	public String getFirst_name() 
	{
		return first_name;
	}
	
	public void setFirst_name(String first_name) 
	{
		this.first_name = first_name;
	}
	
	public String getLast_name() 
	{
		return last_name;
	}
	
	public void setLast_name(String last_name) 
	{
		this.last_name = last_name;
	}
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public String getPhone1() 
	{
		return phone1;
	}
	
	public void setPhone1(String phone1) 
	{
		this.phone1 = phone1;
	}
	
	public String getPhone_type1() 
	{
		return phone_type1;
	}
	
	public void setPhone_type1(String phone_type1)
	{
		this.phone_type1 = phone_type1;
	}
	
	public String getPhone2() 
	{
		return phone2;
	}
	
	public void setPhone2(String phone2) 
	{
		this.phone2 = phone2;
	}
	
	public String getPhone_type2() 
	{
		return phone_type2;
	}
	
	public void setPhone_type2(String phone_type2) 
	{
		this.phone_type2 = phone_type2;
	}
	
	public String getStreet() 
	{
		return street;
	}
	
	public void setStreet(String street) 
	{
		this.street = street;
	}
	
	public String getZip_code() 
	{
		return zip_code;
	}
	
	public void setZip_code(String zip_code) 
	{
		this.zip_code = zip_code;
	}
	
	/*public ArrayList<HashMap<String, Object>> getPlace() 
	{
		return place;
	}
	
	public void setPlace(ArrayList<HashMap<String, Object>> place) 
	{
		this.place = place;
	}
	
	public void addPlace(HashMap<String, Object> place) 
	{
		this.place.add(place);
	}*/
	
	public void map(Profile profile)
	{
		this.id = profile.getId();
		this.s_id = this.id + "";
		this.first_name = profile.getFirst_name();
		this.last_name = profile.getLast_name();
		this.email = profile.getEmail();
		this.phone1 = profile.getPhone1();
		this.phone_type1 = profile.getPhone_type1();
		this.phone2 = profile.getPhone2();
		this.phone_type2 = profile.getPhone_type2();
		this.street = profile.getStreet();
		this.zip_code = profile.getZip_code();
		//PlaceMapping p = new PlaceMapping();
		//if(profile.getPlace() != null)
			//p.map(profile.getPlace(), profile.getPlace().getAlt_names());
		//this.place = new ArrayList();
	}
	
	public HashMap<String, Object> toHashMap()
	{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("s_id", s_id);
		map.put("first_name", first_name);
		map.put("last_name", last_name);
		map.put("email", email);
		map.put("phone1", phone1);
		map.put("phone_type1", phone_type1);
		map.put("phone2", phone2);
		map.put("phone_type2", phone_type2);
		map.put("street", street);
		map.put("zip_code", zip_code);
		
		return map;
	}
}
