/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.io.Serializable;

/**
 *
 * @author fnunez
 */
public class PresenceStatus implements Serializable {

    public final static String STATUS_ONLINE = "online";
    public final static String STATUS_OFFLINE = "offline";

    private Long user_id;
    private String status;

    public PresenceStatus() {
    }

    public PresenceStatus(Long user_id, String status) {
        this.user_id = user_id;
        this.status = status;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
