package com.genward.persistence.request;

import com.genward.persistence.model.Skill;
import com.genward.persistence.model.UserSkill;

public class UserSkillMapping 
{
	private String skill;
	private String type;
	private String experience;
	private String rating;
	private String points;
	
	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public void map(UserSkill userSkill, Skill skill)
	{
		this.skill = skill.getName();
		this.type = skill.getType().getName();
		this.experience = userSkill.getExperience();
		this.rating = userSkill.getRating();
		this.points = userSkill.getPoints() + "";
	}
}
