package com.genward.persistence.request;

import java.util.HashMap;
import java.util.List;

public class PeopleRequest 
{
	private String name;
	private String prefix;
	private String code;
	private String gender;
	private String birth;
	private String death;
	private String surname;
	private FamilyRequest parents; // falta agregar las citas de los items
	private List<HashMap<String, String>> factPlaces;
	private List<PeopleAltMapping> altNames;
	private List<HashMap<String, String>> items;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getDeath() {
		return death;
	}
	public void setDeath(String death) {
		this.death = death;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public FamilyRequest getParents() {
		return parents;
	}
	public void setParents(FamilyRequest parents) {
		this.parents = parents;
	}
	public List<HashMap<String, String>> getFactPlaces() {
		return factPlaces;
	}
	public void setFactPlaces(List<HashMap<String, String>> factPlaces) {
		this.factPlaces = factPlaces;
	}
	public List<PeopleAltMapping> getAltNames() {
		return altNames;
	}
	public void setAltNames(List<PeopleAltMapping> altNames) {
		this.altNames = altNames;
	}
	public List<HashMap<String, String>> getItems() {
		return items;
	}
	public void setItems(List<HashMap<String, String>> items) {
		this.items = items;
	}
}
