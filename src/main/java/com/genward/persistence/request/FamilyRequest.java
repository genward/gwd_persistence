package com.genward.persistence.request;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class FamilyRequest 
{
	private long id;
	private String code;
	private PeopleRequest wife;
	private PeopleRequest husband;
	private Date marriageDate;
	private String city;
	private String county;
	private String state;
	private String country;
	private double latitude;
	private double longitude;
	private List<PeopleRequest> children;
	private List<HashMap<String, String>> items;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public PeopleRequest getWife() {
		return wife;
	}
	public void setWife(PeopleRequest wife) {
		this.wife = wife;
	}
	public PeopleRequest getHusband() {
		return husband;
	}
	public void setHusband(PeopleRequest husband) {
		this.husband = husband;
	}
	public Date getMarriageDate() {
		return marriageDate;
	}
	public void setMarriageDate(Date marriageDate) {
		this.marriageDate = marriageDate;
	}
	public List<PeopleRequest> getChildren() {
		return children;
	}
	public void setChildren(List<PeopleRequest> children) {
		this.children = children;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public List<HashMap<String, String>> getItems() {
		return items;
	}
	public void setItems(List<HashMap<String, String>> items) {
		this.items = items;
	}
}
