/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import org.elasticsearch.common.geo.GeoPoint;

/**
 *
 * @author fnunez
 */
public class ExpertSearchFilters {

    private Long[] ids;
    private String querystring;
    private String[] country;
    private Long[] skill_id;
    private GeoPoint[] coords;
    private Integer jobsFrom;
    private Integer jobsTo;
    private Double jobRating;
    private Double hourlyRateFrom;
    private Double hourlyRateTo;
    private Long pointsFrom;
    private Long pointsTo;
    private Long lastLogin;
    private Long[] language;
    private GeoPoint location;
    private Integer radius;
    private Long user_id;
    private Boolean myjobs;
    private String status;

    public ExpertSearchFilters() {
    }

    public ExpertSearchFilters(Long[] ids, String[] country, Long[] skill_id, GeoPoint[] coords, Integer jobsFrom, Integer jobsTo, Double jobRating, Double hourlyRateFrom, Double hourlyRateTo, Long pointsFrom, Long pointsTo, Long lastLogin, Long[] language) {
        this.ids = ids;
        this.country = country;
        this.coords = coords;
        this.jobsFrom = jobsFrom;
        this.jobsTo = jobsTo;
        this.jobRating = jobRating;
        this.hourlyRateFrom = hourlyRateFrom;
        this.hourlyRateTo = hourlyRateTo;
        this.pointsFrom = pointsFrom;
        this.pointsTo = pointsTo;
        this.lastLogin = lastLogin;
        this.language = language;
        this.skill_id = skill_id;
    }

    public ExpertSearchFilters(String querystring, Long[] ids, String[] country, Long[] skill_id, GeoPoint[] coords, Integer jobsFrom, Integer jobsTo, Double jobRating, Double hourlyRateFrom, Double hourlyRateTo, Long pointsFrom, Long pointsTo, Long lastLogin, Long[] language, GeoPoint location, Integer radius) {
        this.ids = ids;
        this.country = country;
        this.coords = coords;
        this.jobsFrom = jobsFrom;
        this.jobsTo = jobsTo;
        this.jobRating = jobRating;
        this.hourlyRateFrom = hourlyRateFrom;
        this.hourlyRateTo = hourlyRateTo;
        this.pointsFrom = pointsFrom;
        this.pointsTo = pointsTo;
        this.lastLogin = lastLogin;
        this.language = language;
        this.skill_id = skill_id;
        this.location = location;
        this.radius = radius;
        this.querystring = querystring;
    }
    
    public ExpertSearchFilters(String querystring, Long user_id, Boolean myjobs, GeoPoint[] coords, GeoPoint location, Integer radius, String status)
    {
    	this.querystring = querystring;
    	this.user_id = user_id;
    	this.myjobs = myjobs;
    	this.coords = coords;
    	this.location = location;
    	this.radius = radius;
    	this.status = status;
    }

    public Long[] getIds() {
        return ids;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }

    public String[] getCountry() {
        return country;
    }

    public void setCountry(String[] country) {
        this.country = country;
    }

    public GeoPoint[] getCoords() {
        return coords;
    }

    public void setCoords(GeoPoint[] coords) {
        this.coords = coords;
    }

    public Integer getJobsFrom() {
        return jobsFrom;
    }

    public void setJobsFrom(Integer jobsFrom) {
        this.jobsFrom = jobsFrom;
    }

    public Integer getJobsTo() {
        return jobsTo;
    }

    public void setJobsTo(Integer jobsTo) {
        this.jobsTo = jobsTo;
    }

    public Double getJobRating() {
        return jobRating;
    }

    public void setJobRating(Double jobRating) {
        this.jobRating = jobRating;
    }

    public Double getHourlyRateFrom() {
        return hourlyRateFrom;
    }

    public void setHourlyRateFrom(Double hourlyRateFrom) {
        this.hourlyRateFrom = hourlyRateFrom;
    }

    public Double getHourlyRateTo() {
        return hourlyRateTo;
    }

    public void setHourlyRateTo(Double hourlyRateTo) {
        this.hourlyRateTo = hourlyRateTo;
    }

    public Long getPointsFrom() {
        return pointsFrom;
    }

    public void setPointsFrom(Long pointsFrom) {
        this.pointsFrom = pointsFrom;
    }

    public Long getPointsTo() {
        return pointsTo;
    }

    public void setPointsTo(Long pointsTo) {
        this.pointsTo = pointsTo;
    }

    public Long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Long[] getLanguage() {
        return language;
    }

    public void setLanguage(Long[] language) {
        this.language = language;
    }

    public Long[] getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(Long[] skill_id) {
        this.skill_id = skill_id;
    }

    public Boolean anyNotNull() {
        return country != null || skill_id != null || coords != null || jobsFrom != null
                || jobsTo != null || jobRating != null || hourlyRateFrom != null
                || hourlyRateTo != null || pointsFrom != null || pointsTo != null
                || lastLogin != null || language != null || (location != null && radius != null) || querystring != null
                || user_id != null || myjobs != null || status != null;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getQuerystring() {
        return querystring;
    }

    public void setQuerystring(String querystring) {
        this.querystring = querystring;
    }

	public Long getUser_id() 
	{
		return user_id;
	}

	public void setUser_id(Long user_id) 
	{
		this.user_id = user_id;
	}

	public Boolean getMyjobs() 
	{
		return myjobs;
	}

	public void setMyjobs(Boolean myjobs) 
	{
		this.myjobs = myjobs;
	}

	public String getStatus() 
	{
		return status;
	}

	public void setStatus(String status) 
	{
		this.status = status;
	}
}
