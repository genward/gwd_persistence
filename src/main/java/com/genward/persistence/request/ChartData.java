/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fnunez
 */
public class ChartData implements Serializable {

    private List<String> sections;
    private List<PeriodicalDataset> dataset;

    public ChartData() {
    }

    public ChartData(List<String> sections, List<PeriodicalDataset> dataset) {
        this.sections = sections;
        this.dataset = dataset;
    }

    public ChartData(String datasetName, List<Object[]> sectionsAndDataset, Class<?> dataType) {
        this.sections = new ArrayList<>();
        this.dataset = new ArrayList<>();
        PeriodicalDataset singleDataset = new PeriodicalDataset(datasetName, new ArrayList<>());
        for (Object[] sectionAndData : sectionsAndDataset) {
            Integer yearMonthPeriodAsInteger = (Integer) sectionAndData[0];
            if (yearMonthPeriodAsInteger != null && yearMonthPeriodAsInteger > 0) {
                String year = yearMonthPeriodAsInteger.toString().substring(0, 4);
                sections.add(year + "-" + Integer.parseInt(yearMonthPeriodAsInteger.toString().substring(4)));
            }
            singleDataset.getData().add(dataType.cast(sectionAndData[1]));
        }
        this.dataset.add(singleDataset);
    }

    public List<String> getSections() {
        return sections;
    }

    public void setSections(List<String> sections) {
        this.sections = sections;
    }

    public List<PeriodicalDataset> getDataset() {
        return dataset;
    }

    public void setDataset(List<PeriodicalDataset> dataset) {
        this.dataset = dataset;
    }

}
