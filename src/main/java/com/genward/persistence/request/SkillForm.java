package com.genward.persistence.request;

public class SkillForm {

    private Long user_id;
    private Long skill_id;
    private String category;
    private String skill;
    private String rating;
    private String experience;

    public SkillForm(String category, String skill, String rating, String experience) {
        this.category = category;
        this.skill = skill;
        this.rating = rating;
        this.experience = experience;
    }

    public SkillForm() {

    }

    public SkillForm(Long user_id, Long skill_id, String category, String skill, String rating, String experience) {
        this.user_id = user_id;
        this.skill_id = skill_id;
        this.category = category;
        this.skill = skill;
        this.rating = rating;
        this.experience = experience;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(Long skill_id) {
        this.skill_id = skill_id;
    }
    
    
}
