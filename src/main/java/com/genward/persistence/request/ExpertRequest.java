package com.genward.persistence.request;

public class ExpertRequest {

    private SkillForm[] addSkills;
    private ContactInformation contactInformation;
    private Yourself yourself;

    public SkillForm[] getAddSkills() {
        return addSkills;
    }

    public void setAddSkills(SkillForm[] addSkills) {
        this.addSkills = addSkills;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public Yourself getYourself() {
        return yourself;
    }

    public void setYourself(Yourself yourself) {
        this.yourself = yourself;
    }

}
