package com.genward.persistence.request;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.genward.persistence.model.Role;
import com.genward.persistence.model.User;

public class UserMapping 
{
	private long id;
	private String s_id;
	private String username;
	private ProfileMapping profile;
	private Set<Role> roles = new HashSet<>();
	private int points;
	
	public long getId() 
	{
		return id;
	}
	
	public void setId(long id) 
	{
		this.id = id;
	}
	
	public String getS_id() 
	{
		return s_id;
	}
	
	public void setS_id(String s_id) 
	{
		this.s_id = s_id;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username) 
	{
		this.username = username;
	}
	
	public ProfileMapping getProfile() 
	{
		return profile;
	}
	
	public void setProfile(ProfileMapping profile) 
	{
		this.profile = profile;
	}
	
	public int getPoints()
	{
		return points;
	}
	
	public void setPoints(int points) 
	{
		this.points = points;
	}
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void map(User user)
	{
		this.id = user.getId();
		this.s_id = this.id + "";
		this.username = user.getUsername();
		ProfileMapping p = new ProfileMapping();
		if(user.getPerson() != null)
			p.map(user.getPerson());
		this.profile = p;
		this.points = user.getPoints();
		if(user.getRoles() != null)
			this.roles = user.getRoles();
	}
	
	public HashMap<String, Object> toHashMap()
	{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("s_id", s_id);
		map.put("username", username);
		map.put("profile", profile.toHashMap());
		map.put("points", points);
		map.put("roles", roles);
		
		return map;
	}
}
