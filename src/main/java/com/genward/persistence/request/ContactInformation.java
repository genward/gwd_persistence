package com.genward.persistence.request;

public class ContactInformation 
{
	private String first_name;
	private String last_name;
	private String country;
	private String address;
	private String state;
	private String zipCode;
	private String address2;
	private String phoneNumber;
	private String phoneNumber2;
	private String city;
	private String employeeOf;
	private Double latitude;
	private Double longitude;
        private String photo;
	
        public ContactInformation(String first_name, String last_name, String country, String address, String state, String zipCode, String address2, String phoneNumber, String phoneNumber2, String city, String employeeOf)
	{
		this.first_name = first_name;
		this.last_name = last_name;
		this.country = country;
		this.address = address;
		this.state = state;
		this.zipCode = zipCode;
		this.address2 = address2;
		this.phoneNumber = phoneNumber;
		this.phoneNumber2 = phoneNumber2;
		this.city = city;
		this.employeeOf = employeeOf;
	}
        
	public ContactInformation(String first_name, String last_name, String country, String address, String state, String zipCode, String address2, String phoneNumber, String phoneNumber2, String city, String employeeOf, String photo)
	{
		this.first_name = first_name;
		this.last_name = last_name;
		this.country = country;
		this.address = address;
		this.state = state;
		this.zipCode = zipCode;
		this.address2 = address2;
		this.phoneNumber = phoneNumber;
		this.phoneNumber2 = phoneNumber2;
		this.city = city;
		this.employeeOf = employeeOf;
                this.photo = photo;
	}
	
	public ContactInformation()
	{
		
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEmployeeOf() {
		return employeeOf;
	}
	public void setEmployeeOf(String employeeOf) {
		this.employeeOf = employeeOf;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

	public Double getLatitude() 
	{
		return latitude;
	}

	public void setLatitude(Double latitude) 
	{
		this.latitude = latitude;
	}

	public Double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Double longitude) 
	{
		this.longitude = longitude;
	}         
}
