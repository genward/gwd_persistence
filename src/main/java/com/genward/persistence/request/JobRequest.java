/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import com.genward.persistence.model.Expert;
import com.genward.persistence.model.Job;
import com.genward.persistence.model.Skill;
import com.genward.persistence.model.User;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fnunez
 */
public class JobRequest {

    private String name;
    private String description;
    private Double amount;
    private Date due;
    private Long status;
    private Long expert_id;
    private long user_id;
    private Integer days_post;
    private List<Long> skill_ids;
    private Long id;
    private Double max_amount;
    private Double min_amount;
    private Integer type;
    private Boolean secret;
    private Boolean restrictedRange;
    private List<Externalrequest> places;
    
    public static final Integer TYPE_FIXED = 1;
    public static final Integer TYPE_BID = 2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDue() {
        return due;
    }

    public void setDue(Date due) {
        this.due = due;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getExpert_id() {
        return expert_id;
    }

    public void setExpert_id(Long expert_id) {
        this.expert_id = expert_id;
    }

    public Integer getDays_post() {
        return days_post;
    }

    public void setDays_post(Integer days_post) {
        this.days_post = days_post;
    }

    public List<Long> getSkill_ids() {
        return skill_ids;
    }

    public void setSkill_ids(List<Long> skill_ids) {
        this.skill_ids = skill_ids;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Job newJob() {
        Job job = new Job();

        if (id != null && id != 0) {
            job.setId(id);
        }

        if (this.amount != null) {
            job.setAmount(this.amount);
        }
        job.setCreated(new Date());
        job.setDeleted(0);
        if (this.days_post != null) {
            job.setDays_post(this.days_post);
        }
        job.setDescription(this.description);
        job.setDue_date(this.due);
        if (this.expert_id != null) {
            job.setExpert(new Expert(this.expert_id));
        }
        job.setName(this.name);
        job.setUser(new User(this.user_id));
        if (this.skill_ids != null && !this.skill_ids.isEmpty()) {
            Set<Skill> jobskills = new HashSet<>();
            for (Long id : skill_ids) {
                Skill sk = new Skill();
                sk.setId(id);
                jobskills.add(sk);
            }
            job.setSkills(jobskills);
        }
        return job;
    }

    public Double getMax_amount() {
        return max_amount;
    }

    public void setMax_amount(Double max_amount) {
        this.max_amount = max_amount;
    }

    public Double getMin_amount() {
        return min_amount;
    }

    public void setMin_amount(Double min_amount) {
        this.min_amount = min_amount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean isSecret() {
        return secret;
    }

    public void setSecret(Boolean secret) {
        this.secret = secret;
    }

    public Boolean isRestrictedRange() {
        return restrictedRange;
    }

    public void setRestrictedRange(Boolean restrictedRange) {
        this.restrictedRange = restrictedRange;
    }

    public List<Externalrequest> getPlaces() {
        return places;
    }

    public void setPlaces(List<Externalrequest> places) {
        this.places = places;
    }
}
