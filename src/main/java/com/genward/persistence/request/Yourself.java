package com.genward.persistence.request;

public class Yourself {

    private String country;
    private Integer workHoursPerWeek;
    private String dateBirth;
    private Long genealogyCredentials;
    private Integer familyHistoryDegree;
    private Double hourlyRate;
    private Double minimunRate;
    private Integer availability_start;
    private Integer availability_end;
    private String description;
    private Long[] spokenLanguages;

    public Yourself(String country, Integer workHoursPerWeek, String dateBirth, Long genealogyCredentials, Integer familyHistoryDegree, Double hourlyRate, Double minimunRate, Integer availability_start, Integer availability_end, String description, Long[] spokenLanguages) {
        this.country = country;
        this.workHoursPerWeek = workHoursPerWeek;
        this.dateBirth = dateBirth;
        this.genealogyCredentials = genealogyCredentials;
        this.familyHistoryDegree = familyHistoryDegree;
        this.hourlyRate = hourlyRate;
        this.minimunRate = minimunRate;
        this.availability_start = availability_start;
        this.availability_end = availability_end;
        this.description = description;
        this.spokenLanguages = spokenLanguages;
    }

    public Yourself() {

    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getWorkHoursPerWeek() {
        return workHoursPerWeek;
    }

    public void setWorkHoursPerWeek(Integer workHoursPerWeek) {
        this.workHoursPerWeek = workHoursPerWeek;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getGenealogyCredentials() {
        return genealogyCredentials;
    }

    public void setGenealogyCredentials(Long genealogyCredentials) {
        this.genealogyCredentials = genealogyCredentials;
    }

    public Integer getFamilyHistoryDegree() {
        return familyHistoryDegree;
    }

    public void setFamilyHistoryDegree(Integer familyHistoryDegree) {
        this.familyHistoryDegree = familyHistoryDegree;
    }

    public Double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public Double getMinimunRate() {
        return minimunRate;
    }

    public void setMinimunRate(Double minimunRate) {
        this.minimunRate = minimunRate;
    }

    public Integer getAvailability_start() {
        return availability_start;
    }

    public void setAvailability_start(Integer availability_start) {
        this.availability_start = availability_start;
    }

    public Integer getAvailability_end() {
        return availability_end;
    }

    public void setAvailability_end(Integer availability_end) {
        this.availability_end = availability_end;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long[] getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(Long[] spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

}
