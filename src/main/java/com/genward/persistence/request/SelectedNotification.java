/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import com.genward.persistence.catalog.ActivityTypeCatalog;
import com.genward.persistence.catalog.NotificationChannelCatalog;
import com.genward.persistence.model.Notification;
import com.genward.persistence.model.NotificationId;
import com.genward.persistence.model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author fnunez
 */
public class SelectedNotification implements Serializable {

    private Long activity;
    private Long channel;
    private Boolean counter;
    private Boolean sound;

    public SelectedNotification() {
    }

    public SelectedNotification(Long activity, Long channel, Boolean counter, Boolean sound) {
        this.activity = activity;
        this.channel = channel;
        this.counter = counter;
        this.sound = sound;
    }

    public Long getActivity() {
        return activity;
    }

    public void setActivity(Long activity) {
        this.activity = activity;
    }

    public Boolean getCounter() {
        return counter;
    }

    public void setCounter(Boolean counter) {
        this.counter = counter;
    }

    public Boolean getSound() {
        return sound;
    }

    public void setSound(Boolean sound) {
        this.sound = sound;
    }

    public Long getChannel() {
        return channel;
    }

    public void setChannel(Long channel) {
        this.channel = channel;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.activity);
        hash = 43 * hash + Objects.hashCode(this.channel);
        hash = 43 * hash + Objects.hashCode(this.counter);
        hash = 43 * hash + Objects.hashCode(this.sound);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SelectedNotification other = (SelectedNotification) obj;
        if (!Objects.equals(this.activity, other.activity)) {
            return false;
        }
        if (!Objects.equals(this.channel, other.channel)) {
            return false;
        }
        if (!Objects.equals(this.counter, other.counter)) {
            return false;
        }
        if (!Objects.equals(this.sound, other.sound)) {
            return false;
        }
        return true;
    }

    public static Set<Notification> ToPersistenceEntities(List<SelectedNotification> selectedNotifications, User user) {
        if (selectedNotifications != null) {
            Set<Notification> notifications = new HashSet<>();
            for (SelectedNotification selected : selectedNotifications) {
                NotificationChannelCatalog channelCat = new NotificationChannelCatalog();
                channelCat.setId(selected.getChannel());
                ActivityTypeCatalog activityCat = new ActivityTypeCatalog();
                activityCat.setId(selected.getActivity());
                Notification notification = new Notification(new NotificationId(user, channelCat, activityCat), selected.getCounter(), selected.getSound(), new Date());
                notifications.add(notification);
            }
            return notifications;
        }
        return null;
    }

    public static List<SelectedNotification> fromPersistenceEntities(List<Notification> notifications) {
        if (notifications != null) {
            List<SelectedNotification> selected = new ArrayList<>();
            for (Notification notification : notifications) {
                selected.add(new SelectedNotification(notification.getId().getActivity().getId(), notification.getId().getChannel().getId(), notification.getCounter(), notification.getSound()));
            }
            return selected;
        }
        return new ArrayList<>();
    }

}
