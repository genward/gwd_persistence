/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

/**
 *
 * @author fnunez
 */
public class SkillCount {

    private String skill;
    private String category;
    private Long count;

    public SkillCount() {
    }

    public SkillCount(Long count, String skill, String category) {
        this.skill = skill;
        this.category = category;
        this.count = count;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
    
    

}
