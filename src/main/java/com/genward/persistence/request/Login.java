package com.genward.persistence.request;

import io.swagger.annotations.ApiModelProperty;

public class Login {
    
    @ApiModelProperty(notes = "This is the email to start session")
    private String username;
    
    @ApiModelProperty(notes = "This is the password to login")
    private String password;
    
    @ApiModelProperty(notes = "This is the first name to register user")
    private String first_name;
    
    @ApiModelProperty(notes = "This is the last name to register user")
    private String last_name;
    
    @ApiModelProperty(notes = "String with captcha response")
    private String grecaptcharesponse;

    public Login() {

    }

    public Login(String username, String password, String first_name, String last_name, String grecaptcharesponse) {
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.grecaptcharesponse = grecaptcharesponse;
    }

    public Login(String username, String password, String first_name, String last_name) {
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getGrecaptcharesponse() {
        return grecaptcharesponse;
    }
}
