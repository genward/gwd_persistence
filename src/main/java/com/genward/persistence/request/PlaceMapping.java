package com.genward.persistence.request;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.genward.persistence.catalog.PlaceTypeCatalog;
import com.genward.persistence.model.AltNames;
import com.genward.persistence.model.Place;

public class PlaceMapping 
{
	private long id;
	private String name;
	private Double latitude;
	private Double longitude;
	private PlaceTypeCatalog type;
	private Date created;
	private Date updated;
	private int deleted;
	private List<AltNameMapping> alt_names;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public PlaceTypeCatalog getType() {
		return type;
	}
	public void setType(PlaceTypeCatalog type) {
		this.type = type;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public int getDeleted() {
		return deleted;
	}
	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
	public List<AltNameMapping> getAlt_names() {
		return alt_names;
	}
	public void setAlt_names(List<AltNameMapping> alt_names) {
		this.alt_names = alt_names;
	}
	
	public void map(Place place, List<AltNames> alt)
	{
		this.id = place.getId();
		this.name = place.getName();
		this.latitude = place.getLatitude();
		this.longitude = place.getLongitude();
		this.type = place.getType();
		this.created = place.getCreated();
		this.updated = place.getUpdated();
		this.deleted = place.getDeleted();
		
		if(alt != null && !alt.isEmpty())
		{
			List<AltNameMapping> altName = new ArrayList<>();
			for(AltNames a : alt)
			{
				AltNameMapping m = new AltNameMapping();
				m.map(a);
				altName.add(m);
			}
		
			this.alt_names = altName;
		}
		else
		{
			this.alt_names = null;
		}
	}
	
	public String toString()
	{
		return "{\"id\":" + id + ",name:" + ((name == null) ? null : name) + ",latitude:" + latitude + ",longitude:" + longitude + ",type:" + ((type == null) ? null : type) + ",altnames:" + alt_names + "}";
	}
	
	public HashMap<String, Object> toHashMap()
	{
		HashMap<String,Object> hash = new HashMap<String,Object>();
		hash.put("id", id);
		hash.put("name", name);
		hash.put("latitude", latitude);
		hash.put("longitude", longitude);
		hash.put("type", type);
		hash.put("altnames", alt_names);
		
		return hash;
	}
}
