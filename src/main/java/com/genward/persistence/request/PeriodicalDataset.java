/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fnunez
 */
public class PeriodicalDataset implements Serializable {

    private String name;
    private List<Object> data;

    public PeriodicalDataset() {
    }

    public PeriodicalDataset(String name, List<Object> data) {
        this.name = name;
        this.data = data;
    }

    public PeriodicalDataset(Integer yearMonthPeriodAsInteger, Object singleSectionData) {
        if (yearMonthPeriodAsInteger != null && yearMonthPeriodAsInteger > 0) {
            String year = yearMonthPeriodAsInteger.toString().substring(0, 4);
            this.name = year + "-" + Integer.parseInt(yearMonthPeriodAsInteger.toString().substring(4));
        }
        this.data = new ArrayList<>();
        this.data.add(singleSectionData);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

}
