package com.genward.persistence.request;

public class ModelAnswerStatistics 
{
	private long count;
	private int data;
	private int aux1;
	private int aux2;
	private String insertion_date;
        private long id;
        private String name;
        private String surname;
        private String username;
        private int totalItems;
        private int totalRepositories;
        private int totalPoints;
    private long jobs;
    private long points;
    private double avg;

	public ModelAnswerStatistics(long count, int data)
	{
		this.count = count;
		this.data = data;
	}
	
	public ModelAnswerStatistics(long count, long id)
	{
		this.count = count;
		this.id = id;
	}
	
	public ModelAnswerStatistics(long count, int data, int aux1, int aux2)
	{
		this.count = count;
		this.data = data;
		this.aux1 = aux1;
		this.aux2 = aux2;
	}
	
	public ModelAnswerStatistics(String insertion_date, long count)
	{
		this.insertion_date = insertion_date;
		this.count = count;
	}
        //Special Constructor
	public ModelAnswerStatistics(long id, String name, String surname, String username, int totalItems, int totalRepositories,int totalPoints)
	{
		this.id = id;
                this.name = name;
                this.surname = surname;
                this.username = username;
                this.totalItems = totalItems;
                this.totalRepositories = totalRepositories;
                this.totalPoints = totalPoints;
                
	}
	
        public ModelAnswerStatistics(long id, String name, String surname, String username)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.username = username;
        }
        
    public ModelAnswerStatistics(long jobs, long points, double avg)
    {
        this.jobs = jobs;
        this.points = points;
        this.avg = avg;
    }
        
        public long getId (){
            return this.id;
        }
        
        public String getName(){
            return this.name;
        }
        
        public String getSurname(){
            return this.surname;
        }
        
        public String getUsername(){
            return this.username;
        }
        
        public int getTotalItems(){
            return this.totalItems;
        }
        
        public int getTotalRepositories(){
            return this.totalRepositories;
        }
        
        public int getTotalPoints(){
            return this.totalPoints;
        }
        //
        
        public void setId (long id){
            this.id = id;
        }
        
        public void setName( String name ){
            this.name = name;
        }
        
        public void setSurname(String surname){
            this.surname = surname;
        }
        
        public void setUsername(String username){
            this.username = username;
        }
        
        public void setTotalItems(int totalItems){
            this.totalItems = totalItems;
        }
        
        public void setTotalRepositories(int totalRepositories){
            this.totalRepositories=totalRepositories;
        }
        
        public void getTotalPoints(int totalPoints){
            this.totalPoints=totalPoints;
        }
        
        //
	public long getCount() {
		return count;
	}

	public long getData() {
		return data;
	}
	
	public long getAux1() {
		return aux1;
	}

	public int getAux2() {
		return aux2;
	}
	
	public String getInsertion_date() {
		return insertion_date;
	}

	public long getJobs() {
		return jobs;
	}

	public void setJobs(long jobs) {
		this.jobs = jobs;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}
}
