package com.genward.persistence.request;

import java.util.Date;
import java.util.HashMap;

import com.genward.persistence.model.AltNames;

public class AltNameMapping
{
	private long id;
	private String name;
	private String source;
	private Date start_date;
	private Date end_date;
	private Date created;
	private Date updated;
	private int deleted;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public int getDeleted() {
		return deleted;
	}
	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
	
	public void map(AltNames altName)
	{
		this.id = altName.getId();
		this.name = altName.getName();
		this.source = altName.getSource();
		this.start_date = altName.getStart_date();
		this.end_date = altName.getEnd_date();
		this.created = altName.getCreated();
		this.updated = altName.getUpdated();
		this.deleted = altName.getDeleted();
	}
	
	public String toString()
	{
		return "{id:" + id + ",name:" + ((name == null) ? null : name) + ",source:" + ((source == null) ? null : source) + ",start_date:" + ((start_date == null) ? null : start_date) + ",end_date:" + ((end_date == null) ? null : end_date) + "}";
	}
	
	public HashMap<String, Object> toHashMap()
	{
		HashMap<String,Object> hash = new HashMap<String,Object>();
		hash.put("id", id);
		hash.put("name", name);
		hash.put("source", source);
		hash.put("start_date", start_date);
		hash.put("end_date", end_date);
		
		return hash;
	}
}
