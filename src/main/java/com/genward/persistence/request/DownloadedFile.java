/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import java.io.InputStream;

/**
 *
 * @author fnunez
 */
public class DownloadedFile {

    private String contentType;
    private long size;
    private InputStream content;

    public DownloadedFile(InputStream content, String contentType, long size) {
        this.content = content;
        this.contentType = contentType;
        this.size = size;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public InputStream getContent() {
        return content;
    }

    public void setContent(InputStream content) {
        this.content = content;
    }

}
