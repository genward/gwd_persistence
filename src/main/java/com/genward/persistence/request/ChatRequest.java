/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.request;

import com.genward.elasticsearch.model.ESChatMessage;
import com.genward.persistence.model.Expert;
import com.genward.persistence.model.Job;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author fnunez
 */
public class ChatRequest {

    private Long conversation_id;
    private int type;
    private String message;
    private String message_id;
    private Long consecutive;
    private Long from_id;
    private Long to_id;
    private String base64File;
    private String attachment_hashedName;
    private String attachment_originalName;
    private String attachment_contentType;
    private long attachment_size;
    private Date persistedDate;

    public ChatRequest() {
    }

    public ChatRequest(Long conversation_id, String message, Long to_id) {
        this.conversation_id = conversation_id;
        this.message = message;
        this.to_id = to_id;
    }

    public ChatRequest(String message, Long to_id) {
        this.message = message;
        this.to_id = to_id;
    }

    public ChatRequest(String message, Long to_id, String base64File) {
        this.message = message;
        this.to_id = to_id;
        this.base64File = base64File;
    }

    public ChatRequest(Long conversation_id, String message, Long to_id, String base64File) {
        this.conversation_id = conversation_id;
        this.message = message;
        this.to_id = to_id;
        this.base64File = base64File;
    }

    public ChatRequest(Job job, String customMessage, Expert invitee) {
        if (job != null) {
            this.type = ESChatMessage.TYPE_JOB_OFFER;
            this.from_id = job.getUser().getId();
            this.to_id = invitee != null ? invitee.getId() : job.getExpert().getId();
            this.message = customMessage != null ? customMessage : "Job Offer";
        }
    }

    public Long getConversation_id() {
        return conversation_id;
    }

    public void setConversation_id(Long conversation_id) {
        this.conversation_id = conversation_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(Long from_id) {
        this.from_id = from_id;
    }

    public Long getTo_id() {
        return to_id;
    }

    public void setTo_id(Long to_id) {
        this.to_id = to_id;
    }

    public String getBase64File() {
        return base64File;
    }

    public void setBase64File(String base64File) {
        this.base64File = base64File;
    }

    public String getAttachment_hashedName() {
        return attachment_hashedName;
    }

    public void setAttachment_hashedName(String attachment_hashedName) {
        this.attachment_hashedName = attachment_hashedName;
    }

    public String getAttachment_originalName() {
        return attachment_originalName;
    }

    public void setAttachment_originalName(String attachment_originalName) {
        this.attachment_originalName = attachment_originalName;
    }

    public String getAttachment_contentType() {
        return attachment_contentType;
    }

    public void setAttachment_contentType(String attachment_contentType) {
        this.attachment_contentType = attachment_contentType;
    }

    public long getAttachment_size() {
        return attachment_size;
    }

    public void setAttachment_size(long attachment_size) {
        this.attachment_size = attachment_size;
    }

    public Date getPersistedDate() {
        return persistedDate;
    }

    public void setPersistedDate(Date persistedDate) {
        this.persistedDate = persistedDate;
    }

    public Long getConsecutive() {
        return consecutive;
    }

    public void setConsecutive(Long consecutive) {
        this.consecutive = consecutive;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id", conversation_id);
        map.put("type", type);
        map.put("message", message);
        map.put("date", persistedDate);
        map.put("to_id", to_id);
        map.put("consecutive", consecutive);
        map.put("message_id", message_id);
        return map;
    }

}
