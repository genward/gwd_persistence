package com.genward.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.genward.persistence.catalog.BillingAccountTypeCatalog;
import com.genward.persistence.request.BillingAccountRequest;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "GWD_BILLING_ACCOUNT")
public class BillingAccount {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the account that the user is using")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_type_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the type of this account")
    private BillingAccountTypeCatalog type;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the id of the user that owns this payment account")
    private User user;

    @Column
    @ApiModelProperty(notes = "This is the external id that the user has depending the payment method")
    private String user_external_id;

    @Column
    @ApiModelProperty(notes = "This is the status to detect if this account is going to be used by default")
    private boolean default_account;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the account was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the last date when the account was updated")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This is the status to detect if this account is active or inactive")
    private boolean deleted;

    public BillingAccount() {

    }

    public BillingAccount(long id) {
        this.id = id;
    }

    public BillingAccount(BillingAccountTypeCatalog type, User user, String user_external_id, boolean default_account) {
        this.type = type;
        this.user = user;
        this.user_external_id = user_external_id;
        this.default_account = default_account;
        created = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BillingAccountTypeCatalog getType() {
        return type;
    }

    public void setType(BillingAccountTypeCatalog type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUser_external_id() {
        return user_external_id;
    }

    public void setUser_external_id(String user_external_id) {
        this.user_external_id = user_external_id;
    }

    public boolean getDefault_account() {
        return default_account;
    }

    public void setDefault_account(boolean default_account) {
        this.default_account = default_account;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public BillingAccountRequest toBillingAccountRequest() {
        BillingAccountRequest req = new BillingAccountRequest();
        req.setAccountId(this.id);
        req.setAccountTypeId(this.getType().getId());
        req.setExternalUserId(this.user_external_id);
        return req;
    }

}
