package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.genward.persistence.request.ModelAnswerStatistics;

@SqlResultSetMapping(name = "findAllUserResult", classes
        = {
            @ConstructorResult(targetClass = ModelAnswerStatistics.class, columns
                    = {
                @ColumnResult(name = "id", type = long.class),
                @ColumnResult(name = "name", type = String.class),
                @ColumnResult(name = "surname", type = String.class),
                @ColumnResult(name = "username", type = String.class)
            })
        })
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "findAllUserData",
                    query = "SELECT USERN.id,PROFILEN.first_name as name, PROFILEN.last_name as surname,USERN.username FROM GWD_USER as USERN INNER JOIN GWD_PROFILE as PROFILEN ON USERN.person_id = PROFILEN.id",
                    resultSetMapping = "findAllUserResult"),
            @NamedNativeQuery(name = "findAllUserDataById",
                    query = "SELECT USERN.id,PROFILEN.first_name as name, PROFILEN.last_name as surname,USERN.username FROM GWD_USER as USERN INNER JOIN GWD_PROFILE as PROFILEN ON USERN.person_id = PROFILEN.id WHERE USERN.id = ?1",
                    resultSetMapping = "findAllUserResult")
        })

@Entity
@Table(name = "GWD_USER")
public class User implements UserDetails//, Serializable
{

    @Id
    @Column(columnDefinition = "bigint(20)")
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    @ApiModelProperty(notes = "This attribute identify the user")
    private long id;

    @Column(nullable = false)
    @ApiModelProperty(notes = "Email that identify the user")
    private String username;

    @Column(nullable = false)
    @ApiModelProperty(notes = "Password that use to login the user")
    private String password;

    @OneToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "The profile that has relation with user")
    private Profile profile;

    @Column(nullable = false)
    @ApiModelProperty(notes = "This are the current points that the user has")
    private int points;

    @Column(nullable = false, columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "The date that create the user")
    private Date creation_date;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "The date that delete the user")
    private Date deletion_date;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Repositories.class)
    @JoinColumn(name = "user_id")
    @ApiModelProperty(notes = "The repositories that has the user")
    private List<Repositories> repositories;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Items.class)
    @JoinColumn(name = "user_id")
    @ApiModelProperty(notes = "The items that has the user")
    private List<Items> items;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GWD_USER_ROLES",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GWD_R_USER_EXPERT",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "expert_id"))
    private Set<Expert> experts;
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private Set<Notification> notifications;
    
    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "The last activity date registered by this user")
    private Date last_activity;

    @Transient
    private Collection<? extends GrantedAuthority> authorities;
    
    @Transient
    private String s_id;

    public User() {

    }
    
    public User(long id){
        this.id = id;
    }

    public User(String username, String password, Date creation_date, Date deletion_date, Profile profile, int points, boolean enabled) {
        this.username = username;
        this.password = password;
        this.creation_date = creation_date;
        this.deletion_date = deletion_date;
        this.profile = profile;
        this.points = points;
        this.enabled = enabled;
    }

    public User(long id, String username, String password, Profile profile, int points, Date creation_date, Date deletion_date, List<Repositories> repositories, List<Items> items, Set<Notification> notifications, boolean enabled) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.profile = profile;
        this.points = points;
        this.creation_date = creation_date;
        this.deletion_date = deletion_date;
        this.repositories = repositories;
        this.items = items;
        this.notifications = notifications;
        this.enabled = enabled;
    }
   

    public void enable(boolean state) {
        this.enabled = state;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    //@JsonBackReference
    public Profile getPerson() {
        return profile;
    }

    public void setPerson(Profile profile) {
        this.profile = profile;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*public int getPerson_id() 
     {
     return person_id;
     }*/
    public int getPoints() {
        return points;
    }
    
    public void setPoints(int points)
    {
    	this.points = points;
    }
    
    public Date getCreation_date() {
        return creation_date;
    }

    public Date getDeletion_date() {
        return deletion_date;
    }

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        authorities = roles.stream().map(role
                -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());

        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return enabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    
    public Set<Expert> getExperts() {
        return experts;
    }

    public void setExperts(Set<Expert> experts) {
        this.experts = experts;
    }
    
    /*public String getS_id() {
		return s_id;
	}

	public void setS_id() {
		this.s_id = id + "";
	}*/
    
    //@Override
	public void setS_id()
	{
		s_id = id + "";
	}

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    public Date getLast_activity() {
        return last_activity;
    }

    public void setLast_activity(Date last_activity) {
        this.last_activity = last_activity;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	
	public HashMap<String, Object> toHashMap()
	{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("username", username);
		map.put("created", creation_date);
		if(deletion_date != null)
			map.put("deleted", deletion_date);
		map.put("enabled", enabled);
		map.put("points", points);
		map.put("last_activity", last_activity);
		map.put("roles", roles);
		if(profile != null)
			map.put("profile", profile.toHashMap());
		
		return map;
	}
          
}
