package com.genward.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_ALT_NAMES")
public class AltNames
{
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the alt name")
	private long id;
	
	@ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "place_id")
    @ApiModelProperty(notes = "This is the place that this alt name refer")
	private Place place;
	
	@Column
    @ApiModelProperty(notes = "This is the name of the alt name")
	private String name;
	
	@Column
    @ApiModelProperty(notes = "This is the source of the alt name")
	private String source;
	
	@Column(columnDefinition ="DATE")
    @ApiModelProperty(notes = "This is the date when this place receive this alt name")
	private Date start_date;
	
	@Column(columnDefinition ="DATE")
    @ApiModelProperty(notes = "This is the date when this place stop receiving this alt name")
	private Date end_date;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the alt name was created")
	private Date created;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the alt name was updated")
	private Date updated;
	
	@Column(name = "deleted",columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a alt name is active or inactive")
	private int deleted;
	
	public AltNames()
	{
		
	}
	
	public AltNames(Place place, String name, String source, Date start_date, Date end_date)
	{
		this.place = place;
		this.name = name;
		this.source = source;
		this.start_date = start_date;
		this.end_date = end_date;
		created = new Date();
	}

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public Place getPlace() 
	{
		return place;
	}

	public void setPlace(Place place) 
	{
		this.place = place;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getSource() 
	{
		return source;
	}

	public void setSource(String source) 
	{
		this.source = source;
	}

	public Date getStart_date() 
	{
		return start_date;
	}

	public void setStart_date(Date start_date) 
	{
		this.start_date = start_date;
	}

	public Date getEnd_date() 
	{
		return end_date;
	}

	public void setEnd_date(Date end_date) 
	{
		this.end_date = end_date;
	}

	public Date getCreated() 
	{
		return created;
	}

	public void setCreated(Date created) 
	{
		this.created = created;
	}

	public Date getUpdated() 
	{
		return updated;
	}

	public void setUpdated(Date updated) 
	{
		this.updated = updated;
	}

	public int getDeleted() 
	{
		return deleted;
	}

	public void setDeleted(int deleted) 
	{
		this.deleted = deleted;
	}
}
