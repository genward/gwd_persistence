package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table (name = "GWD_PROFILE")
public class Profile
{
	@Id
	@Column(columnDefinition = "bigint(20)")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
        @ApiModelProperty(notes = "This is the object identifier")
	private long id;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "This is the first name of the person")
	private String first_name;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "This is the last name of the person")
	private String last_name;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "The date when the person registered")
	private Date creation_date;
	
	@Column 
        @ApiModelProperty(notes = "The date when the person was removed")
	private Date deletion_date;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "The mail of the person who registered")
	private String email;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "This is the person's phone")
	private String phone1;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "This is the type of phone registered, for example, mobile or home")
	private String phone_type1;
	
	@Column 
        @ApiModelProperty(notes = "This is the second person's phone")
	private String phone2;
	
	@Column 
        @ApiModelProperty(notes = "This is the type of phone registered, for example, mobile or home")
	private String phone_type2;
	
	@Column 
        @ApiModelProperty(notes = "This is the street that the person registered")
	private String street;
	
	@Column 
        @ApiModelProperty(notes = "This is the postal code that the person registered")
	private String zip_code;
	
        @Column
        @ApiModelProperty(notes = "This is the filename of last uploaded profile picture")
        private String photo;
        
	/*@Column 
        @ApiModelProperty(notes = "This is the city that registered the person")
	private String city;
	
	@Column
        @ApiModelProperty(notes = "This is the state where the person lives")
	private String state;
	
	@Column 
        @ApiModelProperty(notes = "This is the country where the user lives")
	private String country;*/
	
	@OneToOne
    @JoinColumn(name = "place_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "The place where this user lives")
    private Place place;
	
	@Transient
	private String s_id;
	
	public Profile()
	{
		
	}
	
	public Profile(String first_name, String last_name, Date creation_date, Date deletion_date, String email, String phone1, String phone_type1, String phone2, String phone_type2, String street, String zip_code, Place place)
	{
		this.first_name = first_name;
		this.last_name = last_name;
		this.creation_date = creation_date;
		this.email = email;
		this.phone1 = phone1;
		this.phone_type1 = phone_type1;
		this.phone2 = phone2;
		this.phone_type2 = phone_type2;
		this.street = street;
		//this.city = city;
		this.zip_code = zip_code;
		//this.state = state;
		//this.country = country;
		this.place = place;
	}

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public String getFirst_name() 
	{
		return first_name;
	}

	public void setFirst_name(String first_name) 
	{
		this.first_name = first_name;
	}

	public String getLast_name() 
	{
		return last_name;
	}

	public void setLast_name(String last_name) 
	{
		this.last_name = last_name;
	}

	public Date getCreation_date() 
	{
		return creation_date;
	}

	public void setCreation_date(Date creation_date) 
	{
		this.creation_date = creation_date;
	}

	public Date getDeletion_date() 
	{
		return deletion_date;
	}

	public void setDeletion_date(Date deletion_date) 
	{
		this.deletion_date = deletion_date;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email) 
	{
		this.email = email;
	}

	public String getPhone1()
	{
		return phone1;
	}

	public void setPhone1(String phone1)
	{
		this.phone1 = phone1;
	}

	public String getPhone_type1() 
	{
		return phone_type1;
	}

	public void setPhone_type1(String phone_type1)
	{
		this.phone_type1 = phone_type1;
	}

	public String getPhone2() 
	{
		return phone2;
	}

	public void setPhone2(String phone2) 
	{
		this.phone2 = phone2;
	}

	public String getPhone_type2() 
	{
		return phone_type2;
	}

	public void setPhone_type2(String phone_type2)
	{
		this.phone_type2 = phone_type2;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street) 
	{
		this.street = street;
	}

	public String getZip_code() 
	{
		return zip_code;
	}

	public void setZip_code(String zip_code) 
	{
		this.zip_code = zip_code;
	}

	/*public String getCity() 
	{
		return city;
	}

	public void setCity(String city) 
	{
		this.city = city;
	}

	public String getState() 
	{
		return state;
	}

	public void setState(String state) 
	{
		this.state = state;
	}

	public String getCountry() 
	{
		return country;
	}

	public void setCountry(String country) 
	{
		this.country = country;
	}*/

	/*public String getS_id() {
		return s_id;
	}

	public void setS_id() {
		this.s_id = id + "";
	}*/
	
	//@Override
	public void setS_id()
	{
		s_id = id + "";
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

    public String getPhoto() {
    	return photo;
    }

    public void setPhoto(String photo) {
    	this.photo = photo;
    }
        
    public HashMap<String, Object> toHashMap()
    {
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("first_name", first_name);
    	map.put("last_name", last_name);
    	map.put("phone", phone1);
    	map.put("street", street);
    	map.put("zip_code", zip_code);
    	
    	return map;
    }
}
