/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author fnunez
 */
@Embeddable
public class PeoplePlacesPK implements Serializable {

    @Column(name = "place_id")
    private Long placeId;

    @Column(name = "person_id")
    private Long personId;

    @Column(name = "type_id")
    private Long typeId;

    private PeoplePlacesPK() {
    }

    public PeoplePlacesPK(Long placeId, Long personId, Long typeId) {
        this.placeId = placeId;
        this.personId = personId;
        this.typeId = typeId;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.placeId);
        hash = 47 * hash + Objects.hashCode(this.personId);
        hash = 47 * hash + Objects.hashCode(this.typeId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PeoplePlacesPK other = (PeoplePlacesPK) obj;
        if (!Objects.equals(this.placeId, other.placeId)) {
            return false;
        }
        if (!Objects.equals(this.personId, other.personId)) {
            return false;
        }
        if (!Objects.equals(this.typeId, other.typeId)) {
            return false;
        }
        return true;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

}
