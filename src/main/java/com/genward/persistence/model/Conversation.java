/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import com.genward.persistence.request.ChatRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_CONVERSATION")
public class Conversation {

    /**
     * STATUS CONSTANTS
     */
    public static final Integer STATUS_SENT = 1;
    public static final Integer STATUS_READ = 2;

    @Id
    @Column(columnDefinition = "bigint(20)")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the conversation")
    private long id;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The text content of the last message")
    private String last_message;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when conversation was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when conversation was updated")
    private Date updated;

    @Column(nullable = false)
    @ApiModelProperty(notes = "This is the status of the last message of this conversation")
    private int status;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    @ApiModelProperty(notes = "This is the user that send the last message")
    private User sender;

    @Column
    @ApiModelProperty(notes = "This is the name of the attached file in the last message")
    private String document;

    @Column(nullable = false)
    @ApiModelProperty(notes = "This is the number of messages posted to this conversation")
    private long message_count;

    @Column(nullable = false)
    @ApiModelProperty(notes = "This is the number of unread messages of this conversation")
    private int unread;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GWD_R_USER_CONVERSATIONS", joinColumns = {
        @JoinColumn(name = "conversation_id")}, inverseJoinColumns = {
        @JoinColumn(name = "user_id")})
    // @OrderColumn(name = "id")
    @OrderBy(value = "id")
    private Set<User> users = new HashSet<>();

    public Conversation() {
    }

    public Conversation(long id, String last_message, Date updated, int status, User sender, String document) {
        this.id = id;
        this.last_message = last_message;
        this.updated = updated;
        this.status = status;
        this.sender = sender;
        this.document = document;
    }

    public Conversation(ChatRequest chatRequest) {
        if (chatRequest != null) {
            this.last_message = chatRequest.getMessage() != null ? chatRequest.getMessage() : "";
            this.updated = new Date();
            this.created = new Date();
            this.status = STATUS_SENT;
            this.document = chatRequest.getAttachment_hashedName();
            this.sender = new User(chatRequest.getFrom_id());
            this.users = new HashSet<>();
            users.add(sender);
            users.add(new User(chatRequest.getTo_id()));
            this.message_count = chatRequest.getConsecutive();
            this.unread = (int) this.message_count;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getMessage_count() {
        return message_count;
    }

    public void setMessage_count(long message_count) {
        this.message_count = message_count;
    }

    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 31 * hash + Objects.hashCode(this.last_message);
        hash = 31 * hash + Objects.hashCode(this.created);
        hash = 31 * hash + Objects.hashCode(this.updated);
        hash = 31 * hash + this.status;
        hash = 31 * hash + Objects.hashCode(this.sender);
        hash = 31 * hash + Objects.hashCode(this.document);
        hash = 31 * hash + (int) (this.message_count ^ (this.message_count >>> 32));
        hash = 31 * hash + this.unread;
        hash = 31 * hash + Objects.hashCode(this.users);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conversation other = (Conversation) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (this.message_count != other.message_count) {
            return false;
        }
        if (this.unread != other.unread) {
            return false;
        }
        if (!Objects.equals(this.last_message, other.last_message)) {
            return false;
        }
        if (!Objects.equals(this.document, other.document)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.sender, other.sender)) {
            return false;
        }
        if (!Objects.equals(this.users, other.users)) {
            return false;
        }
        return true;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("conversation_id", id);
        map.put("lastMessagePreview", last_message);
        map.put("lastMessageDate", updated);
        map.put("lastMessageDay", "");
        User to_user = null;
        for (User user : users) {
            if (user.getId() != sender.getId()) {
                to_user = user;
            }
        }
        map.put("to_user_id", to_user.getId());
        map.put("to_user_name", to_user.getPerson().getFirst_name());
        map.put("message_count", message_count);
        map.put("unread", unread);
        return map;
    }

}
