package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_BID")
public class Bid {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the Bid")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "auction_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the auction for which the bid is posted")
    private Auction auction;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "expert_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the expert that posted the bid")
    private Expert expert;

    @Column(name = "amount", columnDefinition = "DECIMAL(7,2)")
    @ApiModelProperty(notes = "This represents for how much the expert is posting the bid")
    private Double amount;

    @Column
    @ApiModelProperty(notes = "This indicates if this Bid was created due to a user's invitation")
    private Boolean invited;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the bid was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the amount for this bid was updated")
    private Date updated;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the bid was deleted")
    private Date deleted;

    public Bid() {
    }

    public Bid(Auction auction, Expert expert, Double amount) {
        this.auction = auction;
        this.expert = expert;
        this.amount = amount;
        this.invited = amount == null;
        this.created = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        this.expert = expert;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public Boolean isInvited() {
        return invited;
    }

    public void setInvited(Boolean invited) {
        this.invited = invited;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("amount", amount);
        map.put("last_offer_date", updated != null ? updated : created);
        return map;
    }
}
