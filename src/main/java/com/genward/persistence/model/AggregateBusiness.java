package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


import io.swagger.annotations.ApiModelProperty;
import javax.persistence.EmbeddedId;

@Entity
@Table(name = "GWD_AGGREGATE_BUSINESS")
public class AggregateBusiness implements Serializable {

    public static final String TYPE_JOBS_BY_STATUS = "JOBS BY STATUS";
    public static final String TYPE_JOBS_BY_CATEGORY = "JOBS BY CATEGORY";
    public static final String TYPE_EXPERTS_BY_CATEGORY = "EXPERTS BY CATEGORY";
    
    @EmbeddedId
    private AggregateBusinessId id;

    @Column
    @ApiModelProperty(notes = "This is the count of business entities for this aggregation")
    private Long count;

    public AggregateBusiness(Date aggregationDate, String type, String section, Long count) {
        this.id = new AggregateBusinessId(aggregationDate, type, section);
        this.count = count;
    }

    public AggregateBusiness(AggregateBusinessId id, Long count) {
        this.id = id;
        this.count = count;
    }

    public AggregateBusiness() {

    }

    public AggregateBusinessId getId() {
        return id;
    }

    public void setId(AggregateBusinessId id) {
        this.id = id;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
    
    public Date getDate(){
        return this.id.getAggregationDate();
    }

}
