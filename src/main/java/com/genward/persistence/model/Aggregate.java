package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name = "GWD_AGGREGATE")
public class Aggregate implements Serializable
{
	@Id
	@Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "This attribute identify the Aggregate entity")
	private long id;
        
	//@Id
	@Column
	@ApiModelProperty(notes = "This the user that register the object")
	private long user_id;
	
	//@Id
	@Column
	@ApiModelProperty(notes = "This attribute identify the Object that gave points to the user")
	private String object;
	
	@Column
	@ApiModelProperty(notes = "This attribute is the total of points that the user has for operating over that object")
	private int points;
	
	@Column(columnDefinition ="DATETIME")
	@ApiModelProperty(notes = "This is the date when the user made an action to receive points")
	private Date creation_date;
	
	@Column(columnDefinition ="DATETIME")
	@ApiModelProperty(notes = "This is the date when this entry was deleted")
	private Date deletion_date;
	
	public Aggregate()
	{
		
	}
	
	public Aggregate(long user_id, String object, int points, Date creation_date)
	{
		this.user_id = user_id;
		this.object = object;
		this.points = points;
		this.creation_date = creation_date;
	}

	public long getId(){
            return this.id;
        }
	public long getUser_id()
	{
		return user_id;
	}

	public String getObject() 
	{
		return object;
	}

	public int getPoints() 
	{
		return points;
	}

	public void setPoints(int points) 
	{
		this.points = points;
	}
	
	public Date getCreation_date() 
	{
		return creation_date;
	}
	
	public Date getDeletion_date() 
	{
		return deletion_date;
	}
	
	public void setDeletion_date(Date deletion_date) 
	{
		this.deletion_date = deletion_date;;
	}
	public void setId(long id) 
	{
            this.id= id;
	}
}
