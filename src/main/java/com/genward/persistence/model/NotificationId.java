/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import com.genward.persistence.catalog.ActivityTypeCatalog;
import com.genward.persistence.catalog.NotificationChannelCatalog;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author fnunez
 */
@Embeddable
public class NotificationId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the user that is going to receive notifications")
    private User user;

    @ManyToOne
    @JoinColumn(name = "not_channel_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the channel for the notification to be sent")
    private NotificationChannelCatalog channel;

    @ManyToOne
    @JoinColumn(name = "act_type_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the activity associated with the notification")
    private ActivityTypeCatalog activity;

    public NotificationId() {
    }

    public NotificationId(User user, NotificationChannelCatalog channel, ActivityTypeCatalog activity) {
        this.user = user;
        this.channel = channel;
        this.activity = activity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ActivityTypeCatalog getActivity() {
        return activity;
    }

    public void setActivity(ActivityTypeCatalog activity) {
        this.activity = activity;
    }

    public NotificationChannelCatalog getChannel() {
        return channel;
    }

    public void setChannel(NotificationChannelCatalog channel) {
        this.channel = channel;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + Objects.hashCode(this.channel);
        hash = 97 * hash + Objects.hashCode(this.activity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotificationId other = (NotificationId) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.channel, other.channel)) {
            return false;
        }
        if (!Objects.equals(this.activity, other.activity)) {
            return false;
        }
        return true;
    }

}
