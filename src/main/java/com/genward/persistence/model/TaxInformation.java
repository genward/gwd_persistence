package com.genward.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.genward.persistence.catalog.FederalTaxClassificationCatalog;
import com.genward.persistence.catalog.TaxIdentificationNumberCatalog;
import com.genward.persistence.catalog.TaxRegionCatalog;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "GWD_TAX_INFORMATION")
public class TaxInformation {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This is the number that identifies this tax information")
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "The user that created this tax information")
    private User user;

    @Column(name = "legal_name")
    @ApiModelProperty(notes = "This is the legal name that appears in the tax information")
    private String legalName;

    @Column
    @ApiModelProperty(notes = "This is the address for this tax information")
    private String address;

    @Column
    @ApiModelProperty(notes = "This is the Federal Tax Classification")
    private String classification;

    @ManyToOne
    @JoinColumn(name = "classification_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the type of Federal Tax Classification")
    private FederalTaxClassificationCatalog classificationType;

    @Column(name = "tax_number")
    @ApiModelProperty(notes = "This is the tax number")
    private long taxNumber;

    @ManyToOne
    @JoinColumn(name = "tax_number_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the type of identification")
    private TaxIdentificationNumberCatalog identificationType;

    @OneToOne
    @JoinColumn(name = "region", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the region of this tax information")
    private TaxRegionCatalog region;

    @Column(name = "default_info")
    @ApiModelProperty(notes = "This attribute is to know if this is the default tax information for this user")
    private boolean defaultInfo;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when this tax information was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date to know when this tax information was updated")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This attribute identifies if this tax information is available")
    private boolean deleted;

    public TaxInformation() {
    }

    public TaxInformation(User user, String legalName, String address, String classification, FederalTaxClassificationCatalog classificationType, long taxNumber, TaxIdentificationNumberCatalog identificationType, TaxRegionCatalog region, boolean defaultInfo) {
        this.user = user;
        this.legalName = legalName;
        this.address = address;
        this.classification = classification;
        this.classificationType = classificationType;
        this.taxNumber = taxNumber;
        this.identificationType = identificationType;
        this.region = region;
        this.defaultInfo = defaultInfo;
        this.created = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public FederalTaxClassificationCatalog getClassificationType() {
        return classificationType;
    }

    public void setClassificationType(FederalTaxClassificationCatalog classificationType) {
        this.classificationType = classificationType;
    }

    public long getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(long taxNumber) {
        this.taxNumber = taxNumber;
    }

    public TaxIdentificationNumberCatalog getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(TaxIdentificationNumberCatalog identificationType) {
        this.identificationType = identificationType;
    }

    public TaxRegionCatalog getRegion() {
        return region;
    }

    public void setRegion(TaxRegionCatalog region) {
        this.region = region;
    }

    public boolean isDefaultInfo() {
        return defaultInfo;
    }

    public void setDefaultInfo(boolean defaultInfo) {
        this.defaultInfo = defaultInfo;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
