package com.genward.persistence.model;

import com.genward.elasticsearch.model.BatchEntity;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.genward.persistence.catalog.GenealogyCredentialCatalog;
import com.genward.persistence.catalog.LanguagesCatalog;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

@Entity
@Table(name = "GWD_EXPERT")
public class Expert implements BatchEntity {

    @Id
    @Column
    @ApiModelProperty(notes = "This is the id of the expert")
    private long id;

    @Column
    @ApiModelProperty(notes = "Facts about the expert's experience and profile")
    private String description;

    @Column
    @ApiModelProperty(notes = "These are the weekly hours that the expert may apply for jobs")
    private Integer available_hours;

    @Column
    @ApiModelProperty(notes = "This is the first available time on working days")
    private Integer availability_start;

    @Column
    @ApiModelProperty(notes = "This is the end of availability for the expert")
    private Integer availability_end;

    @Column
    @ApiModelProperty(notes = "This is the birthdate of the expert")
    private Date birthdate;

    @ManyToOne
    @JoinColumn(name = "credentials_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This are the credentials that the expert has")
    private GenealogyCredentialCatalog credentials;

    @Column
    @ApiModelProperty(notes = "This identify if the expert has a family history degree")
    private Integer fam_his_degree;

    @Column(name = "acceptable_rate", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the rate that the expert accept")
    private Double acceptable_rate;

    @Column(name = "minimum_rate", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the minimum rate that the expert can accept")
    private Double minimum_rate;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    //@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = UserSkill.class)
    //@JoinColumn(name = "user_id")
    private List<UserSkill> skills;

    @ManyToMany(fetch = FetchType.EAGER) //MERGE
    @JoinTable(name = "GWD_R_EXPERT_SPOKEN_LANGUAGES", joinColumns = {
        @JoinColumn(name = "expert_id")}, inverseJoinColumns = {
        @JoinColumn(name = "language_id")})
    // @OrderColumn(name = "id")
    @OrderBy(value = "name")
    private Set<LanguagesCatalog> spokenLanguages;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the expert registered")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the expert updated his profile")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This identify if the account is active")
    private Integer deleted;

    // Used for sync
    @Transient
    private User user;
    @Transient
    private String country;
    @Transient
    private List<Job> jobs;

    public Expert() {

    }

    public Expert(long id) {
        this.id = id;
    }

    public Expert(Long user_id, Integer available_hours, Date birthdate, GenealogyCredentialCatalog credentials, Integer fam_his_degree, Double acceptable_rate, Double minimum_rate, Integer availability_start, Integer availability_end, String description) {
        this.id = user_id;
        this.available_hours = available_hours != null ? available_hours : 0;
        this.birthdate = birthdate;
        this.credentials = credentials;
        this.fam_his_degree = fam_his_degree != null ? fam_his_degree : 0;
        this.acceptable_rate = acceptable_rate;
        this.minimum_rate = minimum_rate;
        this.availability_start = availability_start != null ? availability_start : 0;
        this.availability_end = availability_end != null ? availability_end : 0;
        this.description = description;
        this.created = new Date();
        this.updated = new Date();
        this.deleted = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long user_id) {
        this.id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAvailable_hours() {
        return available_hours;
    }

    public void setAvailable_hours(Integer available_hours) {
        this.available_hours = available_hours;
    }

    public Integer getAvailability_start() {
        return availability_start;
    }

    public void setAvailability_start(Integer availability_start) {
        this.availability_start = availability_start;
    }

    public Integer getAvailability_end() {
        return availability_end;
    }

    public void setAvailability_end(Integer availability_end) {
        this.availability_end = availability_end;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public GenealogyCredentialCatalog getCredentials() {
        return credentials;
    }

    public void setCredentials(GenealogyCredentialCatalog credentials) {
        this.credentials = credentials;
    }

    public Integer getFam_his_degree() {
        return fam_his_degree;
    }

    public void setFam_his_degree(Integer fam_his_degree) {
        this.fam_his_degree = fam_his_degree;
    }

    public Double getAcceptable_rate() {
        return acceptable_rate;
    }

    public void setAcceptable_rate(Double acceptable_rate) {
        this.acceptable_rate = acceptable_rate;
    }

    public Double getMinimum_rate() {
        return minimum_rate;
    }

    public void setMinimum_rate(Double minimum_rate) {
        this.minimum_rate = minimum_rate;
    }

    public List<UserSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<UserSkill> skills) {
        this.skills = skills;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public Set<LanguagesCatalog> getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(Set<LanguagesCatalog> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("description", description);
        map.put("available_hours", available_hours);
        map.put("availability_start", availability_start);
        map.put("availability_end", availability_end);
        map.put("birthdate", birthdate);
        map.put("credentials", credentials);
        map.put("fam_his_degree", (fam_his_degree == 0) ? "No" : "Yes");
        map.put("acceptable_rate", acceptable_rate);
        map.put("minimum_rate", minimum_rate);

        return map;
    }

}
