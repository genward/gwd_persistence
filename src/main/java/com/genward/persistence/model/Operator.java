package com.genward.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_OPERATOR")
public class Operator{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identify the Operator entity")
    private long id;

    @Column(nullable = false)
    @ApiModelProperty(notes = "Operator name")
    private String name;

    @Column(nullable = false)
    @ApiModelProperty(notes = "Points given by the operation")
    private int points;

    public Operator() {

    }

    public Operator(String name, int points) {
        this.name = name;
        this.points = points;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }
}
