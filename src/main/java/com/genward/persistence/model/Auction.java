package com.genward.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Table(name = "GWD_AUCTION")
public class Auction {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the auction")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the job for which the auction is published")
    private Job job;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the auction was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the auction shall expire")
    private Date expiration;

    @Column(name = "max_amount", columnDefinition = "DECIMAL(7,2)")
    @ApiModelProperty(notes = "This is the max allowed Bid amount for the auction")
    private Double maxAmount;

    @Column(name = "min_amount", columnDefinition = "DECIMAL(7,2)")
    @ApiModelProperty(notes = "This is the min allowed Bid amount for the auction")
    private Double minAmount;

    @Column
    @ApiModelProperty(notes = "This flag specifies if the auction is private")
    private Boolean secret;

    @Column(name = "restricted_range")
    @ApiModelProperty(notes = "This flag determines if the amount range constraint is closed, so no bid out of limits is permitted")
    private Boolean restrictedRange;

    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<Bid> bids;

    public Auction() {
    }

    public Auction(Job job, Date expiration, Double maxAmount, Double minAmount) {
        this.job = job;
        this.expiration = expiration;
        this.maxAmount = maxAmount;
        this.minAmount = minAmount;
        this.secret = false;
        this.restrictedRange = false;
        this.created = new Date();
    }

    public Auction(Job job, Date expiration, Double maxAmount, Double minAmount, Boolean secret, Boolean restrictedRange) {
        this.job = job;
        this.expiration = expiration;
        this.maxAmount = maxAmount;
        this.minAmount = minAmount;
        this.secret = secret;
        this.restrictedRange = restrictedRange;
        this.created = new Date();
    }
    
    

    public void addBid(Bid bid) {
        if (this.bids == null) {
            this.bids = new HashSet<>();
        }
        bid.setAuction(this);
        this.bids.add(bid);

    }

    public Auction(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    public Set<Bid> getBids() {
        return bids;
    }

    public void setBids(Set<Bid> bids) {
        this.bids = bids;
    }

    public Boolean isSecret() {
        return secret;
    }

    public void setSecret(Boolean secret) {
        this.secret = secret;
    }

    public Boolean isRestrictedRange() {
        return restrictedRange;
    }

    public void setRestrictedRange(Boolean restrictedRange) {
        this.restrictedRange = restrictedRange;
    }

}
