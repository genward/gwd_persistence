package com.genward.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.genward.persistence.catalog.BillingAccountTypeCatalog;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_PAYMENT_PARAMETERS")
public class PaymentParameters 
{
    public static final String PARAMETER_EXPERT_ABSORB_ORDER_FEE = "EXPERT_ABSORB_ORDER_FEE";
    public static final String PARAMETER_EXPERT_ABSORB_PAYOUT_FEE = "EXPERT_ABSORB_PAYOUT_FEE";
    public static final String PARAMETER_GENWARD_FIXED_FEE = "GENWARD_FIXED_FEE";
    public static final String PARAMETER_GENWARD_CALCULATED_FEE = "GENWARD_CALCULATED_FEE";
    public static final String PARAMETER_PAYPAL_PAYOUT_FIXED_FEE = "PAYPAL_PAYOUT_FIXED_FEE";
    public static final String PARAMETER_REFUND_PAYOUT_FIXED_FEE = "PAYPAL_REFUND_FIXED_FEE";
    
    public static final String ACTIVE = "1";
    
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the payment parameter")
	private long id;
	
	@OneToOne
    @JoinColumn(name = "account_type_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the type of the account")
	private BillingAccountTypeCatalog type;
	
	@Column
    @ApiModelProperty(notes = "This is the name of the parameter")
	private String parameter_name;
	
	@Column
    @ApiModelProperty(notes = "This is the value of the parameter")
	private String parameter_value;
	
	@Column
    @ApiModelProperty(notes = "This is the description of the parameter")
	private String parameter_description;
	
	public PaymentParameters()
	{
		
	}
	
	public PaymentParameters(BillingAccountTypeCatalog type, String parameter_name, String parameter_value, String parameter_description)
	{
		this.type = type;
		this.parameter_name = parameter_name;
		this.parameter_value = parameter_value;
		this.parameter_description = parameter_description;
	}

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public BillingAccountTypeCatalog getType() 
	{
		return type;
	}

	public void setType(BillingAccountTypeCatalog type) 
	{
		this.type = type;
	}

	public String getParameter_name() 
	{
		return parameter_name;
	}

	public void setParameter_name(String parameter_name) 
	{
		this.parameter_name = parameter_name;
	}

	public String getParameter_value() 
	{
		return parameter_value;
	}

	public void setParameter_value(String parameter_value) 
	{
		this.parameter_value = parameter_value;
	}

	public String getParameter_description() 
	{
		return parameter_description;
	}

	public void setParameter_description(String parameter_description) 
	{
		this.parameter_description = parameter_description;
	}
}
