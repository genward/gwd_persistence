/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity(name = "ItemFamilies")
@Table(name = "GWD_R_ITEM_FAMILIES")
public class ItemFamilies implements Serializable {

    @EmbeddedId
    private ItemFamiliesPK id;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("itemId")
    private Items item;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("familyId")
    private Families family;

    @Column(name = "pages")
    private String pages;

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    private ItemFamilies() {
    }

    public ItemFamilies(Items item, Families family) {
        this.item = item;
        this.family = family;
        this.id = new ItemFamiliesPK(item.getId(), family.getId());
    }
    
    public ItemFamilies(Items item, Families family, String pages) {
        this.item = item;
        this.family = family;
        this.id = new ItemFamiliesPK(item.getId(), family.getId());
        this.pages = pages;
    }

    public ItemFamiliesPK getId() {
        return id;
    }

    public void setId(ItemFamiliesPK id) {
        this.id = id;
    }

    public Items getItem() {
        return item;
    }

    public void setItem(Items item) {
        this.item = item;
    }

    public Families getFamily() {
        return family;
    }

    public void setFamily(Families family) {
        this.family = family;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.item);
        hash = 37 * hash + Objects.hashCode(this.family);
        hash = 37 * hash + Objects.hashCode(this.pages);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemFamilies other = (ItemFamilies) obj;
        if (!Objects.equals(this.pages, other.pages)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.item, other.item)) {
            return false;
        }
        if (!Objects.equals(this.family, other.family)) {
            return false;
        }
        return true;
    }

}
