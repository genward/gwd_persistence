package com.genward.persistence.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.genward.persistence.catalog.Catalog;
import com.genward.persistence.catalog.SkillTypeCatalog;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "GWD_SKILL")
public class Skill extends Catalog
{
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the type of the skill")
	private SkillTypeCatalog type;
	
	public Skill()
	{
		super();
	}
	
	public Skill(String name, SkillTypeCatalog type)
	{
		super(name);
		
		this.type = type;
	}
        
        public Skill(long id){
                super();
                this.setId(id);
        }

	public SkillTypeCatalog getType() {
		return type;
	}

	public void setType(SkillTypeCatalog type) {
		this.type = type;
	}
}
