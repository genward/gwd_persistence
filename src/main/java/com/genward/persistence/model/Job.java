package com.genward.persistence.model;

import com.genward.elasticsearch.model.BatchEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.genward.persistence.catalog.JobStatusCatalog;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "GWD_JOB")
public class Job implements BatchEntity {

    // Constants
    public static final long STATUS_DRAFT = 100;
    public static final long STATUS_IN_AUCTION = 120;
    public static final long STATUS_PENDING_APPROVAL = 150;
    public static final long STATUS_OPEN = 200;
    public static final long STATUS_WORKING = 300;
    public static final long STATUS_CLOSED = 400;
    public static final long STATUS_COMPLETED = 500;
    public static final long STATUS_FINISHED = 520;
    public static final long STATUS_CANCELLED_BY_USER = 600;
    public static final long STATUS_CANCELLED_BY_EXPERT = 601;
    public static final long STATUS_EXPIRED = 700;
    public static final long STATUS_EXPIRED_PAYMENT_UNAUTHORIZABLE = 702;
    public static final long MAX_STATUS_ID = 1000;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the job")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the name of the job")
    private String name;

    @Column
    @ApiModelProperty(notes = "This is the description of the job")
    private String description;

    @Column
    @Temporal(TemporalType.DATE)
    @ApiModelProperty(notes = "This is the due date that the job has")
    private Date due_date;

    @Column(name = "amount", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the amount that the expert will receive for doing the job")
    private Double amount;

    @Column
    @ApiModelProperty(notes = "This is the number of days that the job will be posted")
    private Integer days_post;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the user that created this job")
    private User user;

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the status process that the job has")
    private JobStatusCatalog status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "expert_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the expert hired to do this job")
    private Expert expert;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GWD_R_JOB_SKILL",
            joinColumns = @JoinColumn(name = "job_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GWD_R_JOB_PLACES", joinColumns = {
        @JoinColumn(name = "job_id")}, inverseJoinColumns = {
        @JoinColumn(name = "place_id")})
    private Set<Place> places = new HashSet<>();

    @OneToMany(mappedBy = "job", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<JobDocument> documents;

    @OneToOne(mappedBy = "job", cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "This is the feedback of this job")
    private Feedback feedback;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the job was saved for the first time")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the last updated date")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This identify if the job is deleted")
    private int deleted;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the job was post")
    private Date posted;

    @Transient
    private String s_id;

    @Transient
    private String redirect_url;

    @Transient
    private Auction auction;

    public Job() {

    }

    public Job(Long id) {
        this.id = id;
    }

    public Job(String name, String description, Date due_date, Double amount, Integer days_post, User user, JobStatusCatalog status, Expert expert) {
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.created = new Date();
        this.updated = new Date();
    }

    public Job(long id, String name, String description, Date due_date, Double amount, Integer days_post, User user, JobStatusCatalog status, Expert expert, Set<JobDocument> documents, Feedback feedback, Date created, Date updated, int deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.documents = documents;
        this.feedback = feedback;
        if (this.feedback != null) {
            this.feedback.setJob(this);
        }
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public Job(long id, String name, String description, Date due_date, Double amount, Integer days_post, User user, JobStatusCatalog status, Expert expert, Set<Skill> skills, Set<JobDocument> documents, Feedback feedback, Date created, Date updated, int deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.skills = skills;
        this.documents = documents;
        this.feedback = feedback;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public Job(long id, String name, String description, Date due_date, Double amount, Integer days_post, User user, JobStatusCatalog status, Expert expert, Set<Skill> skills, Set<JobDocument> documents, Feedback feedback, Date created, Date updated, int deleted, Date posted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.skills = skills;
        this.documents = documents;
        this.feedback = feedback;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
        this.posted = posted;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDays_post() {
        return days_post;
    }

    public void setDays_post(Integer days_post) {
        this.days_post = days_post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public JobStatusCatalog getStatus() {
        return status;
    }

    public void setStatus(JobStatusCatalog status) {
        this.status = status;
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        this.expert = expert;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Set<JobDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<JobDocument> documents) {
        this.documents = documents;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public Date getPosted() {
        return posted;
    }

    public void setPosted(Date posted) {
        this.posted = posted;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public Set<Place> getPlaces() {
        return places;
    }

    public void setPlaces(Set<Place> places) {
        this.places = places;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("name", name);
        map.put("description", description);
        map.put("due_date", due_date);
        map.put("amount", amount);
        map.put("days_post", days_post);
        map.put("status_id", status.getId());
        map.put("status_desc", status.getName());
        map.put("posted", posted);
        if (expert != null) {
            map.put("expert_id", expert.getId());
            map.put("expert_name", expert.getUser().getPerson().getFirst_name() + " " + expert.getUser().getPerson().getLast_name());
        }
        if (feedback != null) {
            map.put("feedback_comment", feedback.getComments());
            map.put("feedback_rate", feedback.getTotal());
        } else {
            map.put("feedback_comment", null);
            map.put("feedback_rate", null);
        }
        if (skills != null) {
            ArrayList<HashMap<String, Object>> skill_list = new ArrayList<HashMap<String, Object>>();
            for (Skill skill : skills) {
                HashMap<String, Object> skill_map = new HashMap<String, Object>();
                skill_map.put("id", skill.getId());
                skill_map.put("name", skill.getName());
                skill_map.put("category", skill.getType() != null ? skill.getType().getName() : null);
                skill_list.add(skill_map);
            }
            map.put("skills", skill_list);
        } else {
            map.put("skills", null);
        }

        if (documents != null) {
            ArrayList<HashMap<String, Object>> document_list = new ArrayList<HashMap<String, Object>>();
            for (JobDocument document : documents) {
                HashMap<String, Object> document_map = new HashMap<String, Object>();
                document_map.put("id", document.getId());
                document_map.put("name", document.getOriginal_name());
                document_map.put("content_type", document.getContent_type());
                document_list.add(document_map);
            }
            map.put("documents", document_list);
        } else {
            map.put("documents", null);
        }

        map.put("redirect_url", redirect_url);
        map.put("user_id", this.user.getId());
        map.put("user_name", user.getPerson().getFirst_name() + " " + user.getPerson().getLast_name());
        return map;
    }

}
