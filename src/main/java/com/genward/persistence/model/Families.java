/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import com.genward.elasticsearch.model.BatchEntity;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_FAMILIES")
public class Families implements BatchEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the family")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the full parsed name of the family (included its cardinality)")
    private String code;

    @ManyToOne
    @JoinColumn(name = "wife_id")
    @ApiModelProperty(notes = "This is the marriage place")
    private People wife;

    @ManyToOne
    @JoinColumn(name = "husband_id")
    @ApiModelProperty(notes = "This is the marriage place")
    private People husband;

    @Column(columnDefinition = "DATETIME", name = "marriage_date")
    @ApiModelProperty(notes = "This is the date when the person born")
    private Date marriageDate;

    @ManyToOne
    @JoinColumn(name = "marriage_place_id")
    @ApiModelProperty(notes = "This is the marriage place")
    private Place marriagePlace;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the item was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the item was updated")
    private Date updated;

    @Column(name = "deleted", columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a item is active or inactive")
    private int deleted;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "child_of_id")
    @ApiModelProperty(notes = "The children of this family")
    private List<People> children;
    
    @OneToMany(mappedBy = "family", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemFamilies> citations;


    public Families() {
    }

    public Families(String code, People wife, People husband, Date marriageDate, Place marriagePlace) {
        this.code = code;
        this.wife = wife;
        this.husband = husband;
        this.marriageDate = marriageDate;
        this.marriagePlace = marriagePlace;
        this.created = new Date();
    }

    public Families(long id, String code, People wife, People husband, Date marriageDate, Place marriagePlace, Date created, Date updated, int deleted, List<People> children, List<ItemFamilies> citations) {
        this.id = id;
        this.code = code;
        this.wife = wife;
        this.husband = husband;
        this.marriageDate = marriageDate;
        this.marriagePlace = marriagePlace;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
        this.children = children;
        this.citations = citations;
    }

    public Families(String code) {
        this.code = code;
        this.created = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public People getWife() {
        return wife;
    }

    public void setWife(People wife) {
        this.wife = wife;
    }

    public People getHusband() {
        return husband;
    }

    public void setHusband(People husband) {
        this.husband = husband;
    }

    public Date getMarriageDate() {
        return marriageDate;
    }

    public void setMarriageDate(Date marriageDate) {
        this.marriageDate = marriageDate;
    }

    public Place getMarriagePlace() {
        return marriagePlace;
    }

    public void setMarriagePlace(Place marriagePlace) {
        this.marriagePlace = marriagePlace;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public List<People> getChildren() {
        return children;
    }

    public void setChildren(List<People> children) {
        this.children = children;
    }

    public List<ItemFamilies> getCitations() {
        return citations;
    }

    public void setCitations(List<ItemFamilies> citations) {
        this.citations = citations;
    }

}
