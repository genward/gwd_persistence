package com.genward.persistence.model;

public enum RoleName 
{
	ROLE_USER,
	ROLE_OPERATOR,
	ROLE_EXPERT,
        ROLE_SYSTEM
}
