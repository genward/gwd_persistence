package com.genward.persistence.model;

import com.genward.elasticsearch.model.BatchEntity;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;

@Entity
@Table(name = "GWD_PEOPLE")
public class People implements BatchEntity{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the person")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the name of the person")
    private String name;

    @Column
    @ApiModelProperty(notes = "This is the title prefix of the person")
    private String prefix;

    @Column
    @ApiModelProperty(notes = "This is the full parsed name of the person (included its cardinality)")
    private String code;

    @Column
    @ApiModelProperty(notes = "This is the gender of the person")
    private String gender;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the person born")
    private Date birth;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the person died")
    private Date death;

    @ManyToOne//(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "surname_id")
    @ApiModelProperty(notes = "This is the surname of a person")
    private Surname surname;

    @ManyToOne
    @JoinColumn(name = "child_of_id")
    @ApiModelProperty(notes = "This is the family of origin for this person")
    private Families parents;

    @Column
    @ApiModelProperty(notes = "This is the flag that tells whether name or surname was inferred from the title")
    private boolean inferred;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the person was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the person was updated")
    private Date updated;

    @Column(name = "deleted", columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a person is active or inactive")
    private int deleted;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemPeople> citations;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PeoplePlaces> factPlaces;

    @OneToMany(fetch = FetchType.EAGER)
    @OrderColumn(name="id")
    @JoinColumn(name = "person_id")
    @ApiModelProperty(notes = "The alt names")
    private List<PeopleAltNames> altNames;
    
    @Transient
    private List<Families> marriages;

    public People() {
        this.created = new Date();
    }

    public People(String name, String prefix, String code, String gender, boolean inferred) {
        this.name = name;
        this.prefix = prefix;
        this.code = code;
        this.gender = gender;
        this.inferred = inferred;
        this.created = new Date();
    }

    public List<PeopleAltNames> getAltNames() {
        return altNames;
    }

    public void setAltNames(List<PeopleAltNames> altNames) {
        this.altNames = altNames;
    }

    public Families getParents() {
        return parents;
    }

    public void setParents(Families parents) {
        this.parents = parents;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Date getDeath() {
        return death;
    }

    public void setDeath(Date death) {
        this.death = death;
    }

    public List<ItemPeople> getCitations() {
        return citations;
    }

    public void setCitations(List<ItemPeople> citations) {
        this.citations = citations;
    }

    public List<PeoplePlaces> getFactPlaces() {
        return factPlaces;
    }

    public void setFactPlaces(List<PeoplePlaces> factPlaces) {
        this.factPlaces = factPlaces;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Surname getSurname() {
        return surname;
    }

    public void setSurname(Surname surname) {
        this.surname = surname;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public boolean isInferred() {
        return inferred;
    }

    public void setInferred(boolean inferred) {
        this.inferred = inferred;
    }

    public List<Families> getMarriages() {
        return marriages;
    }

    public void setMarriages(List<Families> marriages) {
        this.marriages = marriages;
    }

}
