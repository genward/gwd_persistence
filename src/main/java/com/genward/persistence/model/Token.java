package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name = "GWD_TOKEN")
public class Token
{
	public static final int TYPE_REGISTER = 1;
	public static final int TYPE_PASSWORD = 2;
	public static final int INSTANCE_GENCAT = 1;
	public static final int INSTANCE_GENPRO = 2;
	
	@Id
	@Column
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
        @ApiModelProperty(notes = "The email with relation to token to validate")
	private String username;
	
	@Column (nullable = false)
        @ApiModelProperty(notes = "The token to validate")
	private String token;
	
	@Column
        @ApiModelProperty(notes = "This is the date when the token was registered")
	private LocalDateTime creation_date;
	
	@Column
        @ApiModelProperty(notes = "Type of token, can be to recover password (2) or confirm account (0)")
	private int type;
	
	@Column
    @ApiModelProperty(notes = "Instance that created this token (Gencat or Genpro)")
	private int instance;
	
	public Token()
	{
		
	}
	
	public Token(String username, String token, LocalDateTime creation_date)
	{
		this.username = username;
		this.token = token;
		this.creation_date = creation_date;
	}
	
	public Token(String username, String token, LocalDateTime creation_date,int type)
	{
		this.username = username;
		this.token = token;
		this.creation_date = creation_date;
		this.type = type;
	}
	
	public Token(String username, String token, LocalDateTime creation_date, int type, int instance)
	{
		this.username = username;
		this.token = token;
		this.creation_date = creation_date;
		this.type = type;
		this.instance = instance;
	}
	
	public String getUsername() 
	{
		return username;
	}

	public String getToken() 
	{
		return token;
	}

	public LocalDateTime getCreation_date() 
	{
		return creation_date;
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}

	public int getInstance() {
		return instance;
	}

	public void setInstance(int instance) {
		this.instance = instance;
	}
}
