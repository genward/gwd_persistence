/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_PEOPLE_ALT_NAMES")
public class PeopleAltNames {
    
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the alt name")
    private long id;
    
    @ManyToOne
    @JoinColumn(name = "person_id")
    @ApiModelProperty(notes = "This is the person")
    private People person;
    
    @Column
    @ApiModelProperty(notes = "This is the alt name")
    private String name;
    
    @Column
    @ApiModelProperty(notes = "This is the prefix of the alt name")
    private String prefix;
    
    @Column
    @ApiModelProperty(notes = "This is the surname of the alt name")
    private String surname;
    
    @Column
    @ApiModelProperty(notes = "This is the type of alt name")
    private String type;
    
     @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the item was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the item was updated")
    private Date updated;

    @Column(name = "deleted", columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a item is active or inactive")
    private int deleted;

    public PeopleAltNames() {
    }

    public PeopleAltNames(People person, String name, String prefix, String surname, String type) {
        this.person = person;
        this.name = name;
        this.prefix = prefix;
        this.surname = surname;
        this.type = type;
        this.created = new Date();
    }
    
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
    
    
    
}
