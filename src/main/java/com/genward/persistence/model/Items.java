package com.genward.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.genward.elasticsearch.model.BatchEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.Table;

import com.genward.persistence.catalog.ItemSubjectCatalog;
import com.genward.persistence.catalog.ItemTypeCatalog;
import com.genward.persistence.request.ModelAnswerStatistics;

import io.swagger.annotations.ApiModelProperty;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Transient;

@SqlResultSetMapping(name = "dailyInsertResult", classes
        = {
            @ConstructorResult(targetClass = ModelAnswerStatistics.class, columns
                    = {
                @ColumnResult(name = "insertion_date", type = String.class)
                ,
				@ColumnResult(name = "count", type = Long.class)
            })
        })
@NamedNativeQueries(
        {
            @NamedNativeQuery(name = "dailyInsert",
                    query = "SELECT DATE_FORMAT(DATE(i.created), '%Y-%m-%d') insertion_date, COUNT(i.id) count FROM GWD_ITEMS i LEFT JOIN GWD_R_ITEM_REPOSITORIES r ON i.id = r.item_id WHERE i.created BETWEEN ?2 AND ?3 AND i.user_id = ?1 AND i.deleted=0 GROUP BY DATE_FORMAT(DATE(i.created), '%Y-%m-%d')",
                    resultSetMapping = "dailyInsertResult")
            ,
			
			@NamedNativeQuery(name = "dailyInsertTotal",
                    query = "SELECT DATE_FORMAT(DATE(i.created), '%Y-%m-%d') insertion_date, COUNT(i.id) count FROM GWD_ITEMS i LEFT JOIN GWD_R_ITEM_REPOSITORIES r ON i.id = r.item_id WHERE i.created BETWEEN ?1 AND ?2 AND i.deleted=0 GROUP BY DATE_FORMAT(DATE(i.created), '%Y-%m-%d')",
                    resultSetMapping = "dailyInsertResult")
            ,
            
            @NamedNativeQuery(name = "stateCount",
                    query = "SELECT P.name insertion_date, sum(Q1.items) count FROM (SELECT place_id state, count(*) items FROM GWD_R_ITEM_PLACES WHERE place_id IN (SELECT ID FROM GWD_PLACES WHERE TYPE_ID = ?2 AND CODE!='' AND ID IN (SELECT place_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id = ?1)) group by place_id UNION ALL SELECT X.locatedat_id state, count(*) items FROM GWD_R_ITEM_PLACES IP JOIN (SELECT place_id, locatedat_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id IN (SELECT ID FROM GWD_PLACES WHERE TYPE_ID = ?2 AND CODE!='' AND ID IN (SELECT place_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id = ?1))) X on IP.place_id=X.place_id group by X.locatedat_id UNION ALL SELECT Y.state_id as state, count(*) items from GWD_R_ITEM_PLACES gip JOIN (SELECT PLA.place_id as city,X.place_id as county,X.locatedat_id as state_id FROM GWD_R_PLACES_LOCATEDAT PLA JOIN  (SELECT place_id, locatedat_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id IN (SELECT ID FROM GWD_PLACES WHERE TYPE_ID = ?2 AND CODE!='' AND ID IN (SELECT place_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id = ?1)))X on PLA.locatedat_id = X.place_id )Y on gip.place_id=Y.city group by Y.state_id UNION ALL SELECT Z.state_id as state, count(*) as items FROM GWD_R_ITEM_PLACES GR JOIN (SELECT LA.place_id as delegation_id, Co.city_id as city_id, Co.county_id as county_id, Co.state_id as state_id FROM GWD_R_PLACES_LOCATEDAT LA JOIN (SELECT PLA.place_id as city_id,X.place_id as county_id,X.locatedat_id as state_id FROM GWD_R_PLACES_LOCATEDAT PLA JOIN (SELECT place_id, locatedat_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id IN (SELECT ID FROM GWD_PLACES WHERE TYPE_ID = ?2 AND CODE!='' AND ID IN (SELECT place_id FROM GWD_R_PLACES_LOCATEDAT where locatedat_id = ?1)))X on PLA.locatedat_id = X.place_id)Co on LA.locatedat_id = Co.city_id)Z on GR.place_id = Z.delegation_id group by Z.state_id)Q1 inner join GWD_PLACES P on Q1.state = P.id group by Q1.state",
                    resultSetMapping = "dailyInsertResult")
            ,
            
            @NamedNativeQuery(name = "subjectCount",
                    query = "SELECT s.name insertion_date, COUNT(*) count FROM GWD_ITEMS i inner join GWD_ITEM_TYPE_CATALOG s on i.type_id = s.id WHERE i.deleted = 0 GROUP BY s.name",
                    resultSetMapping = "dailyInsertResult")
            ,
            
            @NamedNativeQuery(name = "subjectCountUser",
                    query = "SELECT s.name insertion_date, COUNT(*) count FROM GWD_ITEMS i inner join GWD_ITEM_TYPE_CATALOG s on i.type_id = s.id WHERE i.user_id = ?1 and i.deleted = 0 GROUP BY s.name",
                    resultSetMapping = "dailyInsertResult")
            ,
            
            @NamedNativeQuery(name = "countryCount",
                    query = "SELECT Q.country insertion_date, sum(Q.items) count FROM (SELECT countries.name country, count(*) items FROM GWD_R_ITEM_PLACES IP JOIN (SELECT id, name FROM GWD_PLACES WHERE TYPE_ID = ?1 AND CODE!='')countries on IP.place_id = countries.id GROUP BY countries.id UNION ALL SELECT states.country country, count(*) items FROM GWD_R_ITEM_PLACES IP JOIN (SELECT place_id state_id, countries.name country FROM GWD_R_PLACES_LOCATEDAT PLA JOIN  (SELECT id, name FROM GWD_PLACES WHERE TYPE_ID = ?1 AND CODE!='')countries on PLA.locatedat_id = countries.id)states on IP.place_id = states.state_id group by states.country UNION ALL SELECT counties.country country, count(*) items FROM GWD_R_ITEM_PLACES IP JOIN (SELECT place_id county_id, states.state_id state_id, states.country country FROM GWD_R_PLACES_LOCATEDAT LA JOIN (SELECT place_id state_id, countries.name country FROM GWD_R_PLACES_LOCATEDAT PLA JOIN (SELECT id, name FROM GWD_PLACES WHERE TYPE_ID = ?1 AND CODE!='')countries on PLA.locatedat_id = countries.id )states on LA.locatedat_id = states.state_id)counties on IP.place_id = counties.county_id group by counties.country UNION ALL SELECT cities.country country, count(*) items FROM GWD_R_ITEM_PLACES IP JOIN (SELECT place_id city_id, counties.county_id, counties.state_id, counties.country country FROM GWD_R_PLACES_LOCATEDAT LOC JOIN (SELECT place_id county_id, states.state_id state_id, states.country country FROM GWD_R_PLACES_LOCATEDAT LA JOIN (SELECT place_id state_id, countries.name country FROM GWD_R_PLACES_LOCATEDAT PLA JOIN (SELECT id, name FROM GWD_PLACES WHERE TYPE_ID = ?1 AND CODE!='')countries on PLA.locatedat_id = countries.id)states on LA.locatedat_id = states.state_id)counties on LOC.locatedat_id = counties.county_id )cities on IP.place_id = cities.city_id group by cities.country UNION ALL SELECT delegations.country country, count(*) items FROM GWD_R_ITEM_PLACES IP JOIN (SELECT place_id del_id, cities.country country FROM GWD_R_PLACES_LOCATEDAT PL JOIN (SELECT place_id city_id, counties.county_id, counties.state_id, counties.country country FROM GWD_R_PLACES_LOCATEDAT LOC JOIN (SELECT place_id county_id, states.state_id state_id, states.country country FROM GWD_R_PLACES_LOCATEDAT LA JOIN (SELECT place_id state_id, countries.name country FROM GWD_R_PLACES_LOCATEDAT PLA JOIN (SELECT id, name FROM GWD_PLACES WHERE TYPE_ID = ?1 AND CODE!='')countries on PLA.locatedat_id = countries.id)states on LA.locatedat_id = states.state_id)counties on LOC.locatedat_id = counties.county_id)cities on PL.locatedat_id = cities.city_id)delegations on IP.place_id = delegations.del_id group by delegations.country)Q group by Q.country",
                    resultSetMapping = "dailyInsertResult")
        })

@Entity
@Table(name = "GWD_ITEMS")
public class Items implements BatchEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This is the id of the item")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the name of the item")
    private String title;

    @Column(columnDefinition = "DATE")
    @ApiModelProperty(notes = "This is the date when the item started")
    private Date start_date;

    @Column(columnDefinition = "DATE")
    @ApiModelProperty(notes = "This is the date when the item ended")
    private Date end_date;

    @Column
    @ApiModelProperty(notes = "This is the language of the item")
    private String language;

    @Column(columnDefinition = "DATE")
    @ApiModelProperty(notes = "This is the issue date of the item")
    private Date issued;

    @Column
    @ApiModelProperty(notes = "This is the citation of the item")
    private String citation;

    @ManyToOne
    @JoinColumn(name = "type_id")
    @ApiModelProperty(notes = "This is the type that the item has")
    private ItemTypeCatalog type;

    @ManyToOne
    @JoinColumn(name = "subject_id")
    @ApiModelProperty(notes = "This is the subject that the item has")
    private ItemSubjectCatalog subject;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the item was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the item was updated")
    private Date updated;

    @Column(name = "deleted", columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a item is active or inactive")
    private int deleted;

    @ManyToMany(fetch = FetchType.EAGER) //MERGE
    @JoinTable(name = "GWD_R_ITEM_PLACES", joinColumns = {
        @JoinColumn(name = "item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "place_id")})
    // @OrderColumn(name = "id")
    @OrderBy(value = "id")
    private Set<Place> places = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)//cascade = {CascadeType.ALL})//(cascade = {CascadeType.ALL})//(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})//(cascade = CascadeType.MERGE)//(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "GWD_R_ITEM_PUBLISHERS", joinColumns = {
        @JoinColumn(name = "item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "publisher_id")})
    // @OrderColumn(name = "id", nullable = false)
    @OrderBy(value = "id")
    private Set<Publisher> publishers = new HashSet<>();

    //@ManyToMany(fetch = FetchType.EAGER, mappedBy = "items")//(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.ALL}, mappedBy = "items")
    @ManyToMany(fetch = FetchType.EAGER)//cascade = {CascadeType.ALL})//(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "GWD_R_ITEM_REPOSITORIES", joinColumns = {
        @JoinColumn(name = "item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "repository_id")})
    // @OrderColumn(name = "id", nullable = false)
    @OrderBy(value = "id")
    private Set<Repositories> repositories = new HashSet<>();

    //@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.ALL}, mappedBy = "items")
    /*@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})//cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinTable(name = "GWD_R_ITEM_PEOPLE", joinColumns = {
        @JoinColumn(name = "item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "person_id")})
    // @OrderColumn(name = "id", nullable = false)
    @OrderBy(value = "id")
    private Set<People> people = new HashSet<>();*/
    //@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE}, mappedBy = "items")
    @ManyToMany(fetch = FetchType.EAGER)//cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinTable(name = "GWD_R_ITEM_SURNAMES", joinColumns = {
        @JoinColumn(name = "item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "surname_id")})
    //@OrderColumn(name = "id", nullable = false)
    @OrderBy(value = "id")
    private Set<Surname> surnames = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    @ApiModelProperty(notes = "This is the user who registered the repository")
    private User user;

    @Column
    @ApiModelProperty(notes = "This is the subtitle of the item")
    private String subtitle;

    @Column
    @ApiModelProperty(notes = "This is the serie of the item")
    private String series_name;

    @Column
    @ApiModelProperty(notes = "This is the volume of the item")
    private int volumes;

    @Column
    @ApiModelProperty(notes = "This is the name of the item")
    private String pages;

    @Column
    @ApiModelProperty(notes = "This is the author of the item")
    private String author;

    @Transient
    private int signature;

    public Items() {

    }

    public Items(String title, ItemTypeCatalog type, ItemSubjectCatalog subject) {
        this.title = title;
        this.type = type;
        this.subject = subject;
        this.created = new Date();
    }

    public Items(String title, Date start_date, Date end_date, String language, Date issued, String citation, ItemTypeCatalog type, ItemSubjectCatalog subject, User user, String subtitle, String series_name, int volumes, String pages) {
        this.title = title;
        this.start_date = start_date;
        this.end_date = end_date;
        this.language = language;
        this.issued = issued;
        this.citation = citation;
        this.type = type;
        this.subject = subject;
        this.subtitle = subtitle;
        this.series_name = series_name;
        this.volumes = volumes;
        this.pages = pages;
        this.user = user;
        created = new Date();
    }

    public Items(String title, Date start_date, Date end_date, String language, Date issued, String citation, ItemTypeCatalog type, ItemSubjectCatalog subject, String subtitle, String series_name, int volumes, String pages, String author) {
        this.title = title;
        this.start_date = start_date;
        this.end_date = end_date;
        this.language = language;
        this.issued = issued;
        this.citation = citation;
        this.type = type;
        this.subject = subject;
        this.subtitle = subtitle;
        this.series_name = series_name;
        this.volumes = volumes;
        this.pages = pages;
        this.author = author;
        this.created = new Date();
    }

    public Items(String title, Date start_date, Date end_date, String language, Date issued, String citation, ItemTypeCatalog type, ItemSubjectCatalog subject, User user, String subtitle, String series_name, int volumes, String pages, String signature) {
        this.title = title;
        this.start_date = start_date;
        this.end_date = end_date;
        this.language = language;
        this.issued = issued;
        this.citation = citation;
        this.type = type;
        this.subject = subject;
        this.subtitle = subtitle;
        this.series_name = series_name;
        this.volumes = volumes;
        this.pages = pages;
        this.user = user;
        created = new Date();
        updated = new Date();
        int hash = 7;
        for (int i = 0; i < signature.length(); i++) {
            hash = hash * 31 + signature.charAt(i);
        }
        this.signature = hash;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getIssued() {
        return issued;
    }

    public void setIssued(Date issued) {
        this.issued = issued;
    }

    public String getCitation() {
        return citation;
    }

    public void setCitation(String citation) {
        this.citation = citation;
    }

    public ItemTypeCatalog getType() {
        return type;
    }

    public void setType(ItemTypeCatalog type) {
        this.type = type;
    }

    public ItemSubjectCatalog getSubject() {
        return subject;
    }

    public void setSubject(ItemSubjectCatalog subject) {
        this.subject = subject;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSeries_name() {
        return series_name;
    }

    public void setSeries_name(String series_name) {
        this.series_name = series_name;
    }

    public int getVolumes() {
        return volumes;
    }

    public void setVolumes(int volumes) {
        this.volumes = volumes;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPlaces(Set<Place> places) {
        this.places = places;
    }

    public Set<Place> getPlaces() {
        return this.places;
    }

    public Set<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(Set<Publisher> publishers) {
        this.publishers = publishers;
    }

    public Set<Repositories> getRepositories() {
        return repositories;
    }

    public void setRepositories(Set<Repositories> repositories) {
        this.repositories = repositories;
    }

    /*public Set<People> getPeople() {
        return people;
    }

    public void setPeople(Set<People> people) {
        this.people = people;
    }*/
    public Set<Surname> getSurnames() {
        return surnames;
    }

    public void setSurnames(Set<Surname> surnames) {
        this.surnames = surnames;
    }

    @JsonIgnore
    public List<Place> getPlacesAsList() {
        return new ArrayList(places);
    }

    @JsonIgnore
    public void setPlaces(List<Place> places) {
        if (places != null) {
            this.places = new HashSet<>(places);
        }
    }

    @JsonIgnore
    public List<Publisher> getPublishersAsList() {
        return new ArrayList(publishers);
    }

    @JsonIgnore
    public void setPublishers(List<Publisher> publishers) {
        if (publishers != null) {
            this.publishers = new HashSet<>(publishers);
        }
    }

    /*@JsonIgnore
    public List<People> getPeopleAsList() {
        return new ArrayList(this.people);
    }

    @JsonIgnore
    public void setPeople(List<People> people) {
        this.people = new HashSet<>(people);
    }*/
    @JsonIgnore
    public List<Surname> getSurnamesAsList() {
        return new ArrayList(this.surnames);
    }

    @JsonIgnore
    public void setSurnames(List<Surname> surnames) {
        if (surnames != null) {
            this.surnames = new HashSet<>(surnames);
        }
    }

    @JsonIgnore
    public List<Repositories> getRepositoriesAsList() {
        return new ArrayList(this.repositories);
    }

    @JsonIgnore
    public void setRepositories(List<Repositories> repositories) {
        if (repositories != null) {
            this.repositories = new HashSet<>(repositories);
        }
    }

    @JsonIgnore
    public void setHash(String signature) {
        int hash = 7;
        for (int i = 0; i < signature.length(); i++) {
            hash = hash * 31 + signature.charAt(i);
        }
        this.signature = hash;
    }

    @JsonIgnore
    public int getHash() {
        return this.signature;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}
