package com.genward.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_PAYPAL_ORDER")
public class PaypalOrder {

    @Id
    @Column
    @ApiModelProperty(notes = "This attribute identifies an order from paypal")
    private String id;

    @OneToOne
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the job that needs to be pay")
    private Job job;

    @Column
    @ApiModelProperty(notes = "This is the url to redirect the user to approve the order")
    private String approval_url;

    @Column
    @ApiModelProperty(notes = "This is the authorization id for this order")
    private String authorization_id;

    @Column
    @ApiModelProperty(notes = "This is the capture id for this order")
    private String capture_id;

    @Column
    @ApiModelProperty(notes = "This is the refund id for this order")
    private String refund_id;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the order was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the order was authorized")
    private Date authorized;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the order was reauthorized")
    private Date reauthorized;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the order was captured")
    private Date captured;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the order was refunded")
    private Date refunded;

    @Column
    @ApiModelProperty(notes = "This identifies if the order is deleted")
    private boolean deleted;

    public PaypalOrder() {

    }

    public PaypalOrder(String id, Job job, String approval_url, Date created) {
        this.id = id;
        this.job = job;
        this.approval_url = approval_url;
        this.created = created;
    }

    public PaypalOrder(String id, Job job, String approval_url, String authorization_id, String capture_id, String refund_id, Date authorized, Date captured, Date refunded, Date reauthorized) {
        this.id = id;
        this.job = job;
        this.approval_url = approval_url;
        this.authorization_id = authorization_id;
        this.capture_id = capture_id;
        this.refund_id = refund_id;
        created = new Date();
        this.authorized = authorized;
        this.captured = captured;
        this.refunded = refunded;
        this.reauthorized = reauthorized;
        this.deleted = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getApproval_url() {
        return approval_url;
    }

    public void setApproval_url(String approval_url) {
        this.approval_url = approval_url;
    }

    public String getAuthorization_id() {
        return authorization_id;
    }

    public void setAuthorization_id(String authorization_id) {
        this.authorization_id = authorization_id;
    }

    public String getCapture_id() {
        return capture_id;
    }

    public void setCapture_id(String capture_id) {
        this.capture_id = capture_id;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public void setRefund_id(String refund_id) {
        this.refund_id = refund_id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getAuthorized() {
        return authorized;
    }

    public void setAuthorized(Date authorized) {
        this.authorized = authorized;
    }

    public Date getCaptured() {
        return captured;
    }

    public void setCaptured(Date captured) {
        this.captured = captured;
    }

    public Date getRefunded() {
        return refunded;
    }

    public void setRefunded(Date refunded) {
        this.refunded = refunded;
    }

    public Date getReauthorized() {
        return reauthorized;
    }

    public void setReauthorized(Date reauthorized) {
        this.reauthorized = reauthorized;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
