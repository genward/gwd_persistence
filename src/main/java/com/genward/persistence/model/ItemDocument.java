
package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_ITEM_DOCUMENT")
public class ItemDocument{ 
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the document")
    private long id;
    
    @Column (nullable = false)
    @ApiModelProperty(notes = "Document Format")
    private String format;
    
    @Column (nullable = false)
    @ApiModelProperty(notes = "the url where the document is found")
    private String uri;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "item_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the item associated with this document")
    private Items item;
    
    @Column (nullable = false, columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "Date when the document was registered")
    private Date created;
    
    @Column
    @ApiModelProperty(notes = "Document size")
    private long size;    
    
    @Column (nullable = false, columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "Date when the document was updated")
    private Date updated;
    
    @Column(name = "deleted",columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a item is active or inactive")
	private int deleted;
    
    public ItemDocument(){}
    
    public ItemDocument(Items item, String format, long size, String uri){
        this.format = format;
        this.uri = uri;
        this.item = item;
        this.size = size;
        created = new Date();
        updated = new Date();
    }
    
    public ItemDocument(String format, String uri, Items item, Date created, int size){
        this.format = format;
        this.uri = uri;
        this.item = item;
        this.created = created;
        this.size = size;
    }
    
    /**setter**/
    
    public void setId(long id){
        this.id = id;
    }
    
    public void setFormat(String format){
        this.format = format;
    }
    
    public void setUri(String uri){
        this.uri = uri;
    }
    
    public void setContainer(Items item){
        this.item = item;
    }
    
    public void setCreated(Date created){
        this.created = created;
    }
    
    public void setSize(long size){
        this.size = size;
    }
    
    /**getter**/
    public long getId(){
        return id;
    }
    
    public String getFormat(){
        return format;
    }
    
    public String getUri(){
        return uri;
    }
    
    public Items getItem(){
        return item;
    }
    
    public Date getCreated(){
        return created;
    }
    
    public long getSize(){
        return size;
    }

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public void setItem(Items item) {
		this.item = item;
	}
}
