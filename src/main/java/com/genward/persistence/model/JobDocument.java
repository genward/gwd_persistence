package com.genward.persistence.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "GWD_R_JOB_DOCUMENT")
public class JobDocument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This is the unique key of the document")
    private Long id;

    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "job_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the job associated with this document")
    private Job job;

    @Column
    @ApiModelProperty(notes = "Name of the document hashed")
    private String hash_name;

    @Column
    @ApiModelProperty(notes = "Content type of the document")
    private String content_type;

    @Column
    @ApiModelProperty(notes = "Name of the document")
    private String original_name;

    @Column
    @ApiModelProperty(notes = "Size of the file")
    private Long size;

    public JobDocument() {

    }

    public JobDocument(Long id, Job job, String hash_name, String content_type, String original_name, long size) {
        this.id = id;
        this.hash_name = hash_name;
        this.content_type = content_type;
        this.original_name = original_name;
        this.size = size;
        this.job = job;
    }

    public JobDocument(Job job, String hash_name, String content_type, String original_name, long size) {
        this.hash_name = hash_name;
        this.content_type = content_type;
        this.original_name = original_name;
        this.size = size;
        this.job = job;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getHash_name() {
        return hash_name;
    }

    public void setHash_name(String hash_name) {
        this.hash_name = hash_name;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
