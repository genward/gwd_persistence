package com.genward.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_SURNAMES")
public class Surname 
{
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the surname")
	private long id;
	
	@Column
    @ApiModelProperty(notes = "This is the surname of a person")
	private String surname;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the surname was created")
	private Date created;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the surname was updated")
	private Date updated;
	
	@Column(name = "deleted",columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a surname is active or inactive")
	private int deleted;
	
	//@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    //@JoinTable(name = "GWD_R_ITEM_SURNAMES", joinColumns = { @JoinColumn(name = "surname_id") }, inverseJoinColumns = { @JoinColumn(name = "item_id") })
	@ManyToMany(mappedBy = "surnames")
	private List<Items> items;
	
	public Surname()
	{
		
	}
	
	public Surname(String surname)
	{
		this.surname = surname;
		created = new Date();
		updated = new Date();
	}

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public String getSurname() 
	{
		return surname;
	}

	public void setSurname(String surname) 
	{
		this.surname = surname;
	}

	public Date getCreated() 
	{
		return created;
	}

	public void setCreated(Date created) 
	{
		this.created = created;
	}

	public Date getUpdated() 
	{
		return updated;
	}

	public void setUpdated(Date updated) 
	{
		this.updated = updated;
	}

	public int getDeleted() 
	{
		return deleted;
	}

	public void setDeleted(int deleted) 
	{
		this.deleted = deleted;
	}
}
