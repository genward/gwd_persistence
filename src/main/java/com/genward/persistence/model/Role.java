package com.genward.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_ROL")
public class Role
{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "This attribute identify the Rol entity")
    private long id;

    @Enumerated(EnumType.STRING)
    //@NaturalId
    @Column(length = 60)
    @ApiModelProperty(notes = "This is the name of the rol")
    private RoleName name;

    public Role() 
    {
    }

    public Role(RoleName name) 
    {
        this.name = name;
    }

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public RoleName getName() 
	{
		return name;
	}

	public void setName(RoleName name) 
	{
		this.name = name;
	}
    
    
}
