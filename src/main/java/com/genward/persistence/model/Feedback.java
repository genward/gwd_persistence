package com.genward.persistence.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.MapsId;

@Entity
@Table(name = "GWD_FEEDBACK")
public class Feedback implements Serializable {

    @Id   
    private Long job_id;
    
    @OneToOne
    @JoinColumn
    @MapsId
    @ApiModelProperty(notes = "This is the job associated with this feedback")
    private Job job;

    @Column(name = "quality", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the quality that the user consider this expert had doing this job")
    private Double quality;

    @Column(name = "expertise", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the expertise that the user consider this expert had doing this job")
    private Double expertise;

    @Column(name = "responsiveness", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the responsiveness that the user consider this expert had doing this job")
    private Double responsiveness;

    @Column
    @ApiModelProperty(notes = "This are the comments that the user wrote about the expert doing this job")
    private String comments;

    @Column(name = "total", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the average of all the criteria provided by the user")
    private Double total;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the feedback was created")
    private Date created;

    public Feedback() {

    }

    public Feedback(Job job, Double quality, Double expertise, Double responsiveness, String comments, Double total, Date created) {
        this.job = job;        
        this.quality = quality;
        this.expertise = expertise;
        this.responsiveness = responsiveness;
        this.comments = comments;
        this.total = total;
        this.created = created;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Double getQuality() {
        return quality;
    }

    public void setQuality(Double quality) {
        this.quality = quality;
    }

    public Double getExpertise() {
        return expertise;
    }

    public void setExpertise(Double expertise) {
        this.expertise = expertise;
    }

    public Double getResponsiveness() {
        return responsiveness;
    }

    public void setResponsiveness(Double responsiveness) {
        this.responsiveness = responsiveness;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getJob_id() {
        return job_id;
    }

    public void setJob_id(Long job_id) {
        this.job_id = job_id;
    }


}
