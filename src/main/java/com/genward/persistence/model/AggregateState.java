package com.genward.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.genward.persistence.catalog.Catalog;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_AGGREGATE_STATE")
public class AggregateState extends Catalog
{
	@Column
	@ApiModelProperty(notes = "This number of items that this country has")
	private long items;
	
	public AggregateState()
	{
		super();
	}
	
	public AggregateState(String name, long items)
	{
		super(name);
		
		this.items = items;
	}
	
	public long getItems()
	{
		return items;
	}
	
	public void setItems(long items)
	{
		this.items = items;
	}
}
