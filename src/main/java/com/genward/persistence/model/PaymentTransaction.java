package com.genward.persistence.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;

@Entity
@Table(name = "GWD_PAYMENT_TRANSACTION")
public class PaymentTransaction {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the transaction of a payment")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the external id that the payment service produces")
    private String external_id;

    @OneToOne
    @JoinColumn(name = "sender_account_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the account that sends the payment")
    private BillingAccount sender;

    @OneToOne
    @JoinColumn(name = "receiver_account_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the account that receives the payment")
    private BillingAccount receiver;

    @OneToOne
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the job that needs to be pay")
    private Job job;

    @Column(name = "amount", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the amount that the receiver account will get for doing the job")
    private double amount;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the transaction was created")
    private Date created;

    @OneToMany(mappedBy = "transaction", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TransactionFee> fees;

    public PaymentTransaction() {

    }

    public PaymentTransaction(String external_id, BillingAccount sender, BillingAccount receiver, Job job, double amount) {
        this.external_id = external_id;
        this.sender = sender;
        this.receiver = receiver;
        this.job = job;
        this.amount = amount;
        created = new Date();
        if (this.fees == null) {
            this.fees = new ArrayList<>();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public BillingAccount getSender() {
        return sender;
    }

    public void setSender(BillingAccount sender) {
        this.sender = sender;
    }

    public BillingAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(BillingAccount receiver) {
        this.receiver = receiver;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<TransactionFee> getFees() {
        return fees;
    }

    public void setFees(List<TransactionFee> fees) {
        this.fees = fees;
    }
}
