/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author fnunez
 */
@Embeddable
public class AggregateBusinessId implements Serializable {

    @ApiModelProperty(notes = "This is the date this aggregation was created")
    @Column(name = "aggregation_date")
    private Date aggregationDate;
    
    @ApiModelProperty(notes = "This is the description of the type of aggregation")
    @Column(name = "aggregation_type")
    private String type;
    
    @ApiModelProperty(notes = "This is the section description for the aggregation")
    @Column
    private String section;
    
    public AggregateBusinessId() {
    }

    public AggregateBusinessId(Date aggregationDate, String type, String section) {
        this.aggregationDate = aggregationDate;
        this.type = type;
        this.section = section;
    }

    public Date getAggregationDate() {
        return aggregationDate;
    }

    public void setAggregationDate(Date aggregationDate) {
        this.aggregationDate = aggregationDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.aggregationDate);
        hash = 89 * hash + Objects.hashCode(this.type);
        hash = 89 * hash + Objects.hashCode(this.section);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AggregateBusinessId other = (AggregateBusinessId) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.section, other.section)) {
            return false;
        }
        if (!Objects.equals(this.aggregationDate, other.aggregationDate)) {
            return false;
        }
        return true;
    }

}
