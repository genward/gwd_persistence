package com.genward.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.genward.elasticsearch.model.BatchEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.genward.persistence.catalog.RepositoryAvailabilityCatalog;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_REPOSITORIES")
public class Repositories implements BatchEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This is the id of the repository")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the name of the repository")
    private String name;

    @Column
    @ApiModelProperty(notes = "This is the address of the repository")
    private String address;

    @Column
    @ApiModelProperty(notes = "This is the phone of the repository")
    private String phone;

    @Column
    @ApiModelProperty(notes = "This is the zip code of the repository")
    private String zip;

    @Column
    @ApiModelProperty(notes = "This is the website of the repository")
    private String website;

    @ManyToOne
    @JoinColumn(name = "availability_id")
    @ApiModelProperty(notes = "This is the availability that the repository has")
    private RepositoryAvailabilityCatalog availability;

    @OneToOne
    @JoinColumn(name = "place_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "The place where the repository is located")
    private Place place;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the repository was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the repository was updated")
    private Date updated;

    @Column(name = "deleted", columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a repository is active or inactive")
    private int deleted;

    //@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.ALL})//(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    //@JoinTable(name = "GWD_R_ITEM_REPOSITORIES", joinColumns = { @JoinColumn(name = "repository_id") }, inverseJoinColumns = { @JoinColumn(name = "item_id") })
    @ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "repositories")
    private List<Items> items;// = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    @ApiModelProperty(notes = "This is the user who registered the repository")
    private User user;

    @Column(name = "latitude", columnDefinition = "DECIMAL(7,4)")
    @ApiModelProperty(notes = "This is the latitude of the place")
    private Double latitude;

    @Column(name = "lONGITUDE", columnDefinition = "DECIMAL(7,4)")
    @ApiModelProperty(notes = "This is the longitude of the place")
    private Double longitude;

    @Transient
    private int signature;

    public Repositories() {

    }

    public Repositories(String name, String address, String phone, String website, RepositoryAvailabilityCatalog availability, Place place, Double latitude, Double longitude) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.availability = availability;
        this.place = place;
        this.latitude = latitude;
        this.longitude = longitude;
        this.created = new Date();
    }

    public Repositories(String name, String address, String zip, String website, RepositoryAvailabilityCatalog availability, Place place, String phone) {
        this.name = name;
        this.address = address;
        this.zip = zip;
        this.website = website;
        this.availability = availability;
        this.place = place;
        this.phone = phone;
        created = new Date();
        updated = new Date();
    }

    public Repositories(String name, String address, String website, Place p, String phone, Double lat, Double lon) {
        this.name = name;
        this.address = address;
        this.website = website;
        this.place = p;
        this.phone = phone;
        this.latitude = lat;
        this.longitude = lon;
        this.created = new Date();
    }

    public Repositories(String name, String address, String website, RepositoryAvailabilityCatalog availability, Date created) {
        this.name = name;
        this.address = address;
        this.website = website;
        this.availability = availability;
        this.created = created;
    }

    public Repositories(String name, String website, RepositoryAvailabilityCatalog availability) {
        this.name = name;
        this.website = website;
        this.availability = availability;
        this.created = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public RepositoryAvailabilityCatalog getAvailability() {
        return availability;
    }

    public void setAvailability(RepositoryAvailabilityCatalog availability) {
        this.availability = availability;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonIgnore
    public String[] getFields() {
        String[] fields = new String[10];

        fields[0] = (name == null) ? "name" : "";
        fields[1] = (address == null) ? "address" : "";
        fields[2] = (zip == null) ? "zip" : "";
        fields[3] = (website == null) ? "website" : "";
        fields[4] = (latitude == null) ? "latitude" : "";
        fields[5] = (longitude == null) ? "longitude" : "";
        fields[6] = (availability == null) ? "availability" : "";
        fields[7] = (user == null) ? "user" : "";
        fields[8] = (place == null) ? "place" : "";
        fields[9] = (phone == null) ? "phone" : "";
        return fields;
    }

    @JsonIgnore
    @Override
    public String toString() {
        return "{id:" + id + ", name:'" + name + "', address:'" + address + "', zip:'" + zip + "', website:'" + website + "', availability:" + availability.toString() + ", place:" + place.toString() + ", updated:" + updated + ", deleted:" + deleted + ", user:" + user.getId() + ", latitude:" + latitude + ", longitude:" + longitude + ", phone:'" + phone + "'}";
    }

    @JsonIgnore
    public void setHash(String signature) {
        int hash = 7;
        for (int i = 0; i < signature.length(); i++) {
            hash = hash * 31 + signature.charAt(i);
        }
        this.signature = hash;
    }

    @JsonIgnore
    public int getHash() {
        return this.signature;
    }
}
