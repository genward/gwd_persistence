package com.genward.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_PUBLISHERS")
public class Publisher
{
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the publisher")
	private long id;
	
	@Column
    @ApiModelProperty(notes = "This is the name of the publisher")
	private String name;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the publisher was created")
	private Date created;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the publisher was updated")
	private Date updated;
	
	@Column(name = "deleted",columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a publisher is active or inactive")
	private int deleted;
	
	@ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "publishers")//(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "publishers")
    private List<Items> items;
	
	public Publisher()
	{
		
	}
	
	public Publisher(String name)
	{
		this.name = name;
		created = new Date();
		updated = new Date();
	}

	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created) 
	{
		this.created = created;
	}

	public Date getUpdated() 
	{
		return updated;
	}

	public void setUpdated(Date updated) 
	{
		this.updated = updated;
	}

	public int getDeleted() 
	{
		return deleted;
	}

	public void setDeleted(int deleted) 
	{
		this.deleted = deleted;
	}
        @JsonIgnore
	public String[] getFields()
	{
		String[] fields = new String[10];
	    	
	    fields[0] = (name == null) ? "name" : "";
	    	
	    return fields;
        }
}
