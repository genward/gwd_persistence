/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity(name = "ItemPeople")
@Table(name = "GWD_R_ITEM_PEOPLE")
public class ItemPeople implements Serializable {

    @EmbeddedId
    private ItemPeoplePK id;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("itemId")
    private Items item;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("personId")
    private People person;
    
    @Column(name = "pages")
    private String pages;

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    private ItemPeople() {
    }

    public ItemPeople(Items item, People person) {
        this.item = item;
        this.person = person;
        this.id = new ItemPeoplePK(item.getId(), person.getId());
    }
    
    public ItemPeople(Items item, People person, String pages) {
        this.item = item;
        this.person = person;
        this.id = new ItemPeoplePK(item.getId(), person.getId());
        this.pages = pages;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.item);
        hash = 79 * hash + Objects.hashCode(this.person);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemPeople other = (ItemPeople) obj;
        if (!Objects.equals(this.item, other.item)) {
            return false;
        }
        if (!Objects.equals(this.person, other.person)) {
            return false;
        }
        return true;
    }

    public ItemPeoplePK getId() {
        return id;
    }

    public void setId(ItemPeoplePK id) {
        this.id = id;
    }

    public Items getItem() {
        return item;
    }

    public void setItem(Items item) {
        this.item = item;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }
    
    

}
