/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import java.io.Serializable;

/**
 *
 * @author sergio
 */
public class PlacesLocatedAtPK implements Serializable {

    private long place_id;
    private long locatedat_id;

    public PlacesLocatedAtPK(long place_id, long locatedat_id) {
        this.place_id = place_id;
        this.locatedat_id = locatedat_id;
    }

    public PlacesLocatedAtPK() {
    }

    @Override
    public int hashCode() {
        final long prime = 31;
        long result = 1;
        result = prime * result + this.place_id;
        result = prime * result + this.locatedat_id;
        return (new Long(result)).intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PlacesLocatedAtPK other = (PlacesLocatedAtPK) obj;
        if (this.place_id != other.place_id) {
            return false;
        }
        if (this.locatedat_id != other.locatedat_id) {
            return false;
        }
        return true;
    }

}
