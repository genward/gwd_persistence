/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import com.genward.persistence.catalog.PeoplePlaceRelTypeCatalog;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity(name = "ItemPeople")
@Table(name = "GWD_R_PEOPLE_PLACES")
public class PeoplePlaces implements Serializable {

    @EmbeddedId
    private PeoplePlacesPK id;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("placeId")
    private Place place;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("personId")
    private People person;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("typeId")
    private PeoplePlaceRelTypeCatalog type;

    @Column(name = "date")
    private Date date;

    private PeoplePlaces() {
    }

    public PeoplePlaces(Place place, People person, PeoplePlaceRelTypeCatalog type, Date date) {
        this.place = place;
        this.person = person;
        this.type = type;
        this.id = new PeoplePlacesPK(place.getId(), person.getId(), type.getId());
        this.date = date;
    }

    public PeoplePlaces(Place place, People person, PeoplePlaceRelTypeCatalog type) {
        this.place = place;
        this.person = person;
        this.type = type;
        this.id = new PeoplePlacesPK(place.getId(), person.getId(), type.getId());
    }

    public PeoplePlacesPK getId() {
        return id;
    }

    public void setId(PeoplePlacesPK id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public PeoplePlaceRelTypeCatalog getType() {
        return type;
    }

    public void setType(PeoplePlaceRelTypeCatalog type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.place);
        hash = 29 * hash + Objects.hashCode(this.person);
        hash = 29 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PeoplePlaces other = (PeoplePlaces) obj;
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        if (!Objects.equals(this.person, other.person)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

}
