package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.genward.persistence.catalog.ActivityTypeCatalog;
import com.genward.persistence.catalog.NotificationChannelCatalog;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.EmbeddedId;

@Entity
@Table(name = "GWD_NOTIFICATIONS")
public class Notification implements Serializable {

    @EmbeddedId
    private NotificationId id;

    @Column
    @ApiModelProperty(notes = "This is the counter of the notification")
    private Boolean counter;

    @Column
    @ApiModelProperty(notes = "This is sound that the notification does")
    private Boolean sound;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is date when this configuration was created")
    private Date created;

    public Notification(User user, NotificationChannelCatalog channel, ActivityTypeCatalog activity, Boolean counter, Boolean sound) {
        this.id = new NotificationId(user, channel, activity);
        this.counter = counter;
        this.sound = sound;
        created = new Date();
    }

    public Notification(NotificationId id, Boolean counter, Boolean sound, Date created) {
        this.id = id;
        this.counter = counter;
        this.sound = sound;
        this.created = created;
    }

    public Notification() {

    }

    public NotificationId getId() {
        return id;
    }

    public void setId(NotificationId id) {
        this.id = id;
    }

    public Boolean getCounter() {
        return counter;
    }

    public void setCounter(Boolean counter) {
        this.counter = counter;
    }

    public Boolean getSound() {
        return sound;
    }

    public void setSound(Boolean sound) {
        this.sound = sound;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public HashMap<String, Object> toHashMap()
    {
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("notId", id.getActivity().getId());
    	map.put("notName", id.getActivity().getName());
    	map.put("channelId", id.getChannel().getId());
    	map.put("channelName", id.getChannel().getName());
    	map.put("counter", counter);
    	map.put("sound", sound);
    	map.put("created", created);
    	
    	return map;
    }
}
