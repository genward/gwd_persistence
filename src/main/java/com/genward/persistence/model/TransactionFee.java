package com.genward.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.genward.persistence.catalog.FeeTypeCatalog;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_TRANSACTION_FEE")
public class TransactionFee {

    public final static String PAYPAL_CAPTURE_FEE = "PAYPAL CAPTURE FEE";
    public final static String PAYPAL_REFUND_FEE = "PAYPAL REFUND FEE";
    public final static String PAYPAL_CAPTURE_ABSORPTION_FEE = "PAYPAL CAPTURE ABSORPTION FEE";
    public final static String PAYPAL_PAYOUT_FEE = "PAYPAL PAYOUT FEE";
    public final static String GENWARD_PAYOUT_FIXED_FEE = "GENWARD PAYOUT FIXED FEE";
    public final static String GENWARD_PAYOUT_CALCULATED_FEE = "GENWARD PAYOUT CALCULATED FEE";
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the fee that is going to have the transaction")
    private long id;

    @ManyToOne
    @JoinColumn(name = "transaction_id")
    @ApiModelProperty(notes = "This is the transaction that generated this fee")
    private PaymentTransaction transaction;

    @Column(name = "amount", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This are the current points that the user has")
    private double amount;

    @ManyToOne
    @JoinColumn(name = "type_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the type of the fee")
    private FeeTypeCatalog type;

    public TransactionFee() {
        super();
    }

    public TransactionFee(double amount, FeeTypeCatalog type) {
        this.amount = amount;
        this.type = type;
    }

    public TransactionFee(PaymentTransaction transaction, double amount, FeeTypeCatalog type) {
        this.transaction = transaction;
        this.amount = amount;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PaymentTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(PaymentTransaction transaction) {
        this.transaction = transaction;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public FeeTypeCatalog getType() {
        return type;
    }

    public void setType(FeeTypeCatalog type) {
        this.type = type;
    }
}
