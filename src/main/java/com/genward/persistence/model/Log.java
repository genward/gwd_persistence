package com.genward.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "GWD_LOG")
public class Log implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identify the Log entity")
    private long id;
    
    @Column
    @ApiModelProperty(notes = "Synced Entity name")
    private String entity;

    @Column
    @ApiModelProperty(notes = "Total new elements inserted")
    private long totalInserted = 0;

    @Column
    @ApiModelProperty(notes = "Total elements readed in a single sync session")
    private long totalReaded = 0;

    @Column
    @ApiModelProperty(notes = "Total elements deleted in a single sync session")
    private long totalDeleted = 0;

    @Column
    @ApiModelProperty(notes = "Total elements updated in a single sync session")
    private long totalUpdated = 0;

    @Column
    @ApiModelProperty(notes = "Total errors obtained")
    private long totalErrors = 0;

    @Column
    @ApiModelProperty(notes = "Error messages obtained in a sync session")
    private String errorMessages;

    @Column
    @ApiModelProperty(notes = "List of unsynced ids")
    private String unsyncedIds;

    @Column
    @ApiModelProperty(notes = "Date when the sync session started")
    private Date created;
    
    @Column
    @ApiModelProperty(notes = "Date when the sync session ended")
    private Date ended;

    public Log() {

    }
    
    public Log(String entity) {
        this.entity = entity;
        this.created = new Date();
    }

    public Log(long id, String entity, String errorMessages, String unsyncedIds, Date created, Date ended) {
        this.id = id;
        this.entity = entity;
        this.errorMessages = errorMessages;
        this.unsyncedIds = unsyncedIds;
        this.created = created;
        this.ended = ended;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTotalInserted() {
        return totalInserted;
    }

    public void setTotalInserted(long totalInserted) {
        this.totalInserted = totalInserted;
    }

    public long getTotalReaded() {
        return totalReaded;
    }

    public void setTotalReaded(long totalReaded) {
        this.totalReaded = totalReaded;
    }

    public long getTotalDeleted() {
        return totalDeleted;
    }

    public void setTotalDeleted(long totalDeleted) {
        this.totalDeleted = totalDeleted;
    }

    public long getTotalUpdated() {
        return totalUpdated;
    }

    public void setTotalUpdated(long totalUpdated) {
        this.totalUpdated = totalUpdated;
    }

    public long getTotalErrors() {
        return totalErrors;
    }

    public void setTotalErrors(long totalErrors) {
        this.totalErrors = totalErrors;
    }

    public String getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(String errorMessages) {
        this.errorMessages = errorMessages;
    }

    public String getUnsyncedIds() {
        return unsyncedIds;
    }

    public void setUnsyncedIds(String unsyncedIds) {
        this.unsyncedIds = unsyncedIds;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getEnded() {
        return ended;
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }
    
    @JsonIgnore
    public void addDeleted() {
        this.totalDeleted++;
    }
    @JsonIgnore
    public void addTotal() {
        this.totalReaded++;
    }
    @JsonIgnore
    public void addInserted() {
        this.totalInserted++;
    }
    @JsonIgnore
    public void addUpdated() {
        this.totalUpdated++;
    }
    
    @JsonIgnore
    public void addError() {
        this.totalErrors++;
    }
    
    
    @JsonIgnore
    public void addError(String message) {
        this.errorMessages += message;
        this.addError();
    }
    
    @JsonIgnore
    public void addError(Throwable ex) {
        this.errorMessages += ex.toString();
        this.addError();
    }
    
    @JsonIgnore
    public void addError(String message, List unsynced) {
        this.unsyncedIds += StringUtils.join(unsynced, ",");
        this.addError(message);
    }
    @JsonIgnore
    public void addError(Throwable th, List unsynced) {
        this.unsyncedIds += StringUtils.join(unsynced, ",");
        this.addError(th.toString(), unsynced);
    }

}
