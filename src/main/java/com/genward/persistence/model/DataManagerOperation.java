/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_DATAMANAGER_OPERATION")
public class DataManagerOperation implements Serializable {

    public final static String PARAMETER_LAST_STARTED = "LAST_STARTED";
    public final static String PARAMETER_LAST_DONE = "LAST_DONE";

    @Id
    @Basic(optional = false)
    @Column(name = "PARAMETER")
    private String parameter;

    @Basic(optional = false)
    @Column(name = "VALUE")
    private String value;

    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;

    // Control fields
    @Basic(optional = false)
    @Column(columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    public DataManagerOperation() {
    }

    public DataManagerOperation(String parameter, String value, Date updated) {
        this.parameter = parameter;
        this.value = value;
        this.updated = updated;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
