package com.genward.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import com.genward.persistence.catalog.EntityTypeCatalog;
import com.genward.persistence.catalog.EventTypeCatalog;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_EVENTS")
public class Event
{
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the event")
	private long id;
	
	@ManyToOne
    @JoinColumn(name = "entity_type_id")
    @ApiModelProperty(notes = "This is the entity type of the event")
	private EntityTypeCatalog entity_type;
	
	@ManyToOne
    @JoinColumn(name = "user_id")
    @ApiModelProperty(notes = "This is the user who created the event")
	private User user;
	
	@ManyToOne
    @JoinColumn(name = "event_type_id")
    @ApiModelProperty(notes = "This is the entity type of the event")
	private EventTypeCatalog event_type;
	
	@Column(columnDefinition ="DATETIME")
    @ApiModelProperty(notes = "This is the date when the event was created")
	private Date created;
	
	public Event()
	{
		
	}
	
	public Event(EntityTypeCatalog entity_type, User user, EventTypeCatalog event_type)
	{
		this.entity_type = entity_type;
		this.user = user;
		this.event_type = event_type;
		created = new Date();
	}
	
	public long getId() 
	{
		return id;
	}
	
	public void setId(long id) 
	{
		this.id = id;
	}
	
	public EntityTypeCatalog getEntity_type() 
	{
		return entity_type;
	}
	
	public void setEntity_type(EntityTypeCatalog entity_type)
	{
		this.entity_type = entity_type;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user) 
	{
		this.user = user;
	}
	
	public EventTypeCatalog getEvent_type() 
	{
		return event_type;
	}
	
	public void setEvent_type(EventTypeCatalog event_type) 
	{
		this.event_type = event_type;
	}
	
	public Date getCreated() 
	{
		return created;
	}
	
	public void setCreated(Date created) 
	{
		this.created = created;
	}
	
	
}
