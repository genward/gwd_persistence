package com.genward.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.genward.persistence.catalog.PlaceTypeCatalog;

import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import javax.persistence.ElementCollection;
import javax.persistence.Transient;

@Entity
@Table(name = "GWD_PLACES")
public class Place { 

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the place")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the name of the place")
    private String name;

    @Column(name = "latitude", columnDefinition = "DECIMAL(10,7)")
    @ApiModelProperty(notes = "This is the latitude of the place")
    private Double latitude;

    @Column(name = "lONGITUDE", columnDefinition = "DECIMAL(10,7)")
    @ApiModelProperty(notes = "This is the longitude of the place")
    private Double longitude;

    @ManyToOne
    @JoinColumn(name = "type_id")
    @ApiModelProperty(notes = "This is the type of the place")
    private PlaceTypeCatalog type;
    
    @Column
    @ApiModelProperty(notes = "The place code (parsed full name)")
    private String code;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the place was created")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the place was updated")
    private Date updated;

    @Column(name = "deleted", columnDefinition = "TINYINT(1) DEFAULT 0")
    @ApiModelProperty(notes = "This is the status to detect if a surname is active or inactive")
    private int deleted;

    @ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "places")
    private List<Items> items;

    //@OneToMany(cascade = CascadeType.MERGE, targetEntity = AltNames.class)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = AltNames.class)
    @JoinColumn(name = "place_id")
    @ApiModelProperty(notes = "The alter names that has this place")
    private List<AltNames> alt_names;

    /*
     @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
     @JoinTable(name = "GWD_R_PLACES_LOCATEDAT", joinColumns = { @JoinColumn(name = "place_id") }, inverseJoinColumns = { @JoinColumn(name = "locatedat_id") })
     private List<Place> locateDat = new ArrayList<Place>();
     */
    @Transient
    private int signature;
    
    @Transient
    private String s_id;

    public Place() {

    }

    public Place(String name, Double latitude, Double longitude, PlaceTypeCatalog type) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        created = new Date();
        updated = new Date();
    }
    
    public Place(String name, String code, Double latitude, Double longitude, PlaceTypeCatalog type) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.code = code;
        this.created = new Date();
    }

    public long getId() {
        return id;
    }

    /*public List<Place> getLocateDat(){
     return this.locateDat;
     }*/
    public void setId(long id) {
        this.id = id;
    }

    /*public void setLocateDat(List<Place> locateDat){
     this.locateDat = locateDat;
     }*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public PlaceTypeCatalog getType() {
        return type;
    }

    public void setType(PlaceTypeCatalog type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @JsonIgnore
    public String toString() {
        return "{name:'" + name + "', latitude:'" + latitude + "', longitude:'" + longitude + "', type:" + type.toString() + ", }";
    }

    @JsonIgnore
    public void setHash(String signature) {
        int hash = 7;
        for (int i = 0; i < signature.length(); i++) {
            hash = hash * 31 + signature.charAt(i);
        }
        this.signature = hash;
    }

    @JsonIgnore
    public int getHash() {
        return this.signature;
    }

    @JsonIgnore
    public String[] getFields() {
        String[] fields = new String[5];

        fields[0] = (name == null) ? "name" : "";
        fields[1] = (latitude == null) ? "latitude" : "";
        fields[2] = (longitude == null) ? "longitude" : "";
        return fields;
    }

    public List<AltNames> getAlt_names() {
        return alt_names;
    }

    public void setAlt_names(List<AltNames> alt_names) {
        this.alt_names = alt_names;
    }

    public String getS_id()
	{
		return s_id;
	}
    
    public void setS_id()
	{
		s_id = id + "";
	}
    
    public boolean equals(Place place)
    {
    	if(id == place.getId())
    		return true;
    	
    	return false;
    }
}
