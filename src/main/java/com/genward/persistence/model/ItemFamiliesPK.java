/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author fnunez
 */
@Embeddable
public class ItemFamiliesPK implements Serializable {

    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "family_id")
    private Long familyId;

    private ItemFamiliesPK() {
    }

    public ItemFamiliesPK(Long itemId, Long familyId) {
        this.itemId = itemId;
        this.familyId = familyId;
    }

    public Long getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Long familyId) {
        this.familyId = familyId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.itemId);
        hash = 97 * hash + Objects.hashCode(this.familyId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemFamiliesPK other = (ItemFamiliesPK) obj;
        if (!Objects.equals(this.itemId, other.itemId)) {
            return false;
        }
        if (!Objects.equals(this.familyId, other.familyId)) {
            return false;
        }
        return true;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

}
