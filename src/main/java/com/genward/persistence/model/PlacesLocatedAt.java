package com.genward.persistence.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_R_PLACES_LOCATEDAT")
@IdClass(PlacesLocatedAtPK.class)
public class PlacesLocatedAt implements Serializable {

    @Id
    @Column
    @ApiModelProperty(notes = "This is the place id that make relation")
    private long place_id;

    @Id
    @Column
    @ApiModelProperty(notes = "This is the place id father")
    private long locatedat_id;
    
    public PlacesLocatedAt(){}

    public PlacesLocatedAt(long place_id, long locatedat_id) {
        this.place_id = place_id;
        this.locatedat_id = locatedat_id;
    }

    public long getPlace_id() {
        return place_id;
    }

    public void setPlace_id(long place_id) {
        this.place_id = place_id;
    }

    public long getLocatedat_id() {
        return locatedat_id;
    }

    public void setLocatedat_id(long locatedat_id) {
        this.locatedat_id = locatedat_id;
    }
    
    
    
    
}
