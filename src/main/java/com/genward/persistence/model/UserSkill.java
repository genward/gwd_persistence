package com.genward.persistence.model;

import com.genward.ormlite.model.UserSkills;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.EmbeddedId;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
@Table(name = "GWD_USER_SKILL")
public class UserSkill implements Serializable {

    @EmbeddedId
    private UserSkillPK id = new UserSkillPK();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    private Expert user;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("skillId")
    private Skill skill;

    @Column
    @ApiModelProperty(notes = "This is the experience that the user has with this skill")
    private String experience;

    @Column
    @ApiModelProperty(notes = "This is the rating that the user has with this skill")
    private String rating;

    @Column
    @ApiModelProperty(notes = "This is the points that the user obtains with this skill")
    private int points;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when this skill was assigned to the user")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the info of this association was updated")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This identify if the user still have this skill")
    private int deleted;
    
    @Column(name = "rate_avg", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This identify if the user still have this skill")
    private double rate_avg;
    
    @Column
    @ApiModelProperty(notes = "This is the number of jobs that the user has done using this skill")
    private int jobs;

    public UserSkill() {

    }

    public UserSkill(Expert user, Skill skill, String experience, String rating, int points) {
        this.user = user;
        this.skill = skill;
        this.experience = experience;
        this.rating = rating;
        this.points = points;
        created = new Date();
        updated = new Date();
    }

    public UserSkill(Expert user, Skill skill, String experience, String rating, int points, Date created, Date updated, int deleted) {
        this.user = user;
        this.skill = skill;
        this.experience = experience;
        this.id = new UserSkillPK(user.getId(), skill.getId());
        this.rating = rating;
        this.points = points;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public UserSkill(UserSkills usk) {
        this.created = usk.getCreated();
        this.updated = usk.getUpdated();
        this.deleted = usk.getDeleted();
        this.experience = usk.getExperience();
        this.points = usk.getPoints();
        this.rating = usk.getRating();
        this.jobs = usk.getJobs();
        this.rate_avg = usk.getRate_avg();
    }

    public UserSkillPK getId() {
        return id;
    }

    public void setId(UserSkillPK id) {
        this.id = id;
    }

    public Expert getUser() {
        return user;
    }

    public void setUser(Expert user) {
        this.user = user;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public double getRate_avg() {
		return rate_avg;
	}

	public void setRate_avg(double rate_avg) {
		this.rate_avg = rate_avg;
	}

	public int getJobs() {
		return jobs;
	}

	public void setJobs(int jobs) {
		this.jobs = jobs;
	}

	@Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.user);
        hash = 29 * hash + Objects.hashCode(this.skill);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserSkill other = (UserSkill) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.skill, other.skill)) {
            return false;
        }
        return true;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("skill_id", skill.getId());
        map.put("skill_name", skill.getName());
        map.put("skill_type_id", skill.getType().getId());
        map.put("skill_type_name", skill.getType().getName());
        map.put("experience", experience);
        map.put("rating", rating);
        map.put("points", points);
        map.put("rate_avg", rate_avg);
        map.put("jobs", jobs);

        return map;
    }
}
