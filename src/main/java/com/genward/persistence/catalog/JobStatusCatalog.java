package com.genward.persistence.catalog;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_JOB_STATUS_CATALOG")
public class JobStatusCatalog extends Catalog {

    public JobStatusCatalog() {
        super();
    }

    public JobStatusCatalog(String name) {
        super(name);
    }

    public JobStatusCatalog(long id) {
        super();
        this.setId(id);
    }

}
