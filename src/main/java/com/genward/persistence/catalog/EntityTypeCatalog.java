package com.genward.persistence.catalog;

import com.genward.persistence.model.Event;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_ENTITY_TYPE_CATALOG")
public class EntityTypeCatalog extends Catalog implements Serializable{ 
       
        public EntityTypeCatalog()
        {
        	super();
        }
        
        public EntityTypeCatalog(long id, String name){
        	super(name);
            setId(id);
        }
        
        public EntityTypeCatalog(String name){
            super(name);
        }
}
