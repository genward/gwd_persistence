package com.genward.persistence.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_TAX_REGION")
public class TaxRegionCatalog extends Catalog
{
	@Column
	@ApiModelProperty(notes = "This are the extra parameters that a tax info can have depending the region")
	private String parameters;
	
	public TaxRegionCatalog(String name, String parameters)
	{
		super(name);
		
		this.parameters = parameters;
	}

	public String getParameters() 
	{
		return parameters;
	}

	public void setParameters(String parameters) 
	{
		this.parameters = parameters;
	}
}
