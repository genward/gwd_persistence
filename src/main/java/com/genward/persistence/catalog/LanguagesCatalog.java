package com.genward.persistence.catalog;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_LANGUAGES_CATALOG")
public class LanguagesCatalog extends Catalog {

    public LanguagesCatalog() {
        super();
    }

    public LanguagesCatalog(String name) {
        super(name);
    }

    public LanguagesCatalog(Long id) {
        super();
        this.setId(id);
    }
}
