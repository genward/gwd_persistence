package com.genward.persistence.catalog;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_REPOSITORY_AVAILABILITY_CATALOG")
public class RepositoryAvailabilityCatalog extends Catalog implements Serializable{ 
    
    @Column(columnDefinition ="TINYINT")
    @ApiModelProperty(notes = "This is the flag that indicates if it is deleted")
    private int deleted;
    
     public RepositoryAvailabilityCatalog()
     {
    	 super();
     }
    
    public RepositoryAvailabilityCatalog(long id, String name, int deleted){
        super(name);
    	setId(id);
        this.deleted = deleted;
    }
    
    public RepositoryAvailabilityCatalog(String name, int deleted){
        super(name);
        this.deleted = deleted;
    }
    
    public RepositoryAvailabilityCatalog(String name)
    {
    	super(name);
    }
    
    /*get*/
    public int getDeleted(){
        return this.deleted;
    }
    
    /*set*/
    public void setDeleted(int deleted){
        this.deleted = deleted;
    }
    
    @JsonIgnore
    public String[] getFields() {
        String[] fields = new String[10];
        fields[0] = (getName() == null) ? "name" : "";
        return fields;
    }

    @JsonIgnore
    @Override
    public String toString() {
        return "{id:" + getId() + ", name:'" + getName() + "'}";
    }
}
