package com.genward.persistence.catalog;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_ACTIVITY_TYPE")
public class ActivityTypeCatalog extends Catalog
{
	public ActivityTypeCatalog()
	{
		super();
	}
	
	public ActivityTypeCatalog(String name)
	{
		super(name);
	}
}
