package com.genward.persistence.catalog;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Catalog
{
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This is the catalog identifier")
    private long id;
    
    @Column
    @ApiModelProperty(notes = "This is the name of the catalog")
    private String name;
    
    public Catalog()
    {
    	
    }
    
    public Catalog(String name)
    {
    	this.name = name;
    }
    
    public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
}
