package com.genward.persistence.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_FEE_TYPE")
public class FeeTypeCatalog extends Catalog
{
	@Column
    @ApiModelProperty(notes = "This attribute identifies if it's a genward income")
	private boolean genward_income;
	
	public FeeTypeCatalog()
	{
		super();
	}
	
	public FeeTypeCatalog(String name, boolean genward_income)
	{
		super(name);
		this.genward_income = genward_income;
	}

	public boolean isGenward_income() 
	{
		return genward_income;
	}

	public void setGenward_income(boolean genward_income) 
	{
		this.genward_income = genward_income;
	}
}
