package com.genward.persistence.catalog;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "GWD_ITEM_SUBJECT_CATALOG")
public class ItemSubjectCatalog extends Catalog implements Serializable{ 
    
    @Column(columnDefinition ="TINYINT")
    @ApiModelProperty(notes = "This is the flag that indicates if it is deleted")
    private int deleted;
    
     public ItemSubjectCatalog()
     {
    	 super();
     }
    
    public ItemSubjectCatalog(long id, String name, int deleted){
    	super(name);
        setId(id);
        this.deleted = deleted;
    }
    
    public ItemSubjectCatalog(String name, int deleted){
        super(name);
        this.deleted = deleted;
    }
    
    public ItemSubjectCatalog(String name)
    {
    	super(name);
    }
    
    public int getDeleted(){
        return this.deleted;
    }

    public void setDeleted(int deleted){
        this.deleted = deleted;
    }
    
    @JsonIgnore
    public String[] getFields() {
        String[] fields = new String[10];
        fields[0] = (getName() == null) ? "name" : "";
        return fields;
    }

    @JsonIgnore
    @Override
    public String toString() {
        return "{id:" + getId() + ", name:'" + getName() + "'}";
    }
}
