package com.genward.persistence.catalog;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_EVENT_TYPE_CATALOG")
public class EventTypeCatalog extends Catalog implements Serializable { 
    
    public EventTypeCatalog()
    {
    	super();
    }
    
    public EventTypeCatalog(long id, String name){
        super(name);
    	setId(id);
    }
    
    public EventTypeCatalog(String name){
        super(name);
    }
}
