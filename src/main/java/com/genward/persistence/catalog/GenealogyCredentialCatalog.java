package com.genward.persistence.catalog;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_GENEALOGY_CREDENTIALS")
public class GenealogyCredentialCatalog extends Catalog
{
	public GenealogyCredentialCatalog()
	{
		super();
	}
        
        public GenealogyCredentialCatalog(long id){
                super();
                this.setId(id);
        }
	
	public GenealogyCredentialCatalog(String name)
	{
		super(name);
	}
}
