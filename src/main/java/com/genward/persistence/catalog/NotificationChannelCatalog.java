package com.genward.persistence.catalog;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_NOTIFICATION_CHANNEL")
public class NotificationChannelCatalog extends Catalog
{
	public NotificationChannelCatalog()
	{
		super();
	}
	
	public NotificationChannelCatalog(String name)
	{
		super(name);
	}
}
