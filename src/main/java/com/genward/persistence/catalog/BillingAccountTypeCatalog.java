package com.genward.persistence.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "GWD_BILLING_ACCOUNT_TYPE")
public class BillingAccountTypeCatalog extends Catalog {

    public static final Long PAYPAL = 1l;

    @Column
    @ApiModelProperty(notes = "This attribute identifies the class that will implement this payment method")
    private String implementor;

    @Column
    @ApiModelProperty(notes = "This is the status to detect if this payment method is active or inactive")
    private boolean deleted;

    public BillingAccountTypeCatalog() {
        super();
    }

    public BillingAccountTypeCatalog(String name, String implementor) {
        super(name);

        this.implementor = implementor;
    }
    
    public BillingAccountTypeCatalog(Long id){
        super();
        this.setId(id);
    }

    public String getImplementor() {
        return implementor;
    }

    public void setImplementor(String implementor) {
        this.implementor = implementor;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
