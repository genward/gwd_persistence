package com.genward.persistence.catalog;

import com.genward.persistence.model.Skill;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_SKILL_TYPE")
public class SkillTypeCatalog extends Catalog {

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER)
    @OrderBy(value = "name")
    private List<Skill> skills;

    public SkillTypeCatalog() {
        super();
    }

    public SkillTypeCatalog(String name) {
        super(name);
    }

    public SkillTypeCatalog(long id) {
        super();
        this.setId(id);
    }

    public SkillTypeCatalog(long id, String name) {
        super(name);
        this.setId(id);
    }
    
    public SkillTypeCatalog(long id, String name, List<Skill> skills) {
        super(name);
        this.setId(id);
        this.skills = skills;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

}
