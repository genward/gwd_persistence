/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.persistence.catalog;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_PEOPLE_PLACE_RELATION_CATALOG")
public class PeoplePlaceRelTypeCatalog extends Catalog implements Serializable {
    
    @Column(columnDefinition = "TINYINT")
    @ApiModelProperty(notes = "This is the flag that indicates if it is deleted")
    private int deleted;

    public PeoplePlaceRelTypeCatalog() 
    {
    	super();
    }

    public PeoplePlaceRelTypeCatalog(String name) 
    {
        super(name);
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

}
