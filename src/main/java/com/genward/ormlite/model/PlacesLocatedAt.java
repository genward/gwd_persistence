package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_R_PLACES_LOCATEDAT")
public class PlacesLocatedAt implements Serializable {

    @Column(name="place_id", updatable = false, insertable=false)
    @ApiModelProperty(notes = "This is the place id that make relation")
    private long placeid;

    @Column(name="locatedat_id", updatable = false, insertable=false)
    @ApiModelProperty(notes = "This is the place id father")
    private long locatedatid;
    
    public PlacesLocatedAt(){}

    public PlacesLocatedAt(long place_id, long locatedat_id) {
        this.placeid = place_id;
        this.locatedatid = locatedat_id;
    }

    public long getPlace_id() {
        return placeid;
    }

    public void setPlace_id(long place_id) {
        this.placeid = place_id;
    }

    public long getLocatedat_id() {
        return locatedatid;
    }

    public void setLocatedat_id(long locatedat_id) {
        this.locatedatid = locatedat_id;
    }
    
    
    
    
}
