package com.genward.ormlite.model;

import com.genward.persistence.model.*;
import com.genward.elasticsearch.model.BatchEntity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.genward.persistence.catalog.JobStatusCatalog;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@Table(name = "GWD_JOB")
public class JobLite implements BatchEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "This attribute identifies the job")
    private long id;

    @Column
    @ApiModelProperty(notes = "This is the name of the job")
    private String name;

    @Column
    @ApiModelProperty(notes = "This is the description of the job")
    private String description;

    @Column
    @ApiModelProperty(notes = "This is the due date that the job has")
    private Date due_date;

    @Column(name = "amount", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the amount that the expert will receive for doing the job")
    private double amount;

    @Column
    @ApiModelProperty(notes = "This is the number of days that the job will be posted")
    private int days_post;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the user that created this job")
    private User user;

    @ManyToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(notes = "This is the status process that the job has")
    private JobStatusCatalog status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "expert_id", referencedColumnName = "id")
    @ApiModelProperty(notes = "This is the expert hired to do this job")
    private Expert expert;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GWD_R_JOB_SKILL",
            joinColumns = @JoinColumn(name = "job_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills;

    @OneToMany(mappedBy = "job", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<JobDocument> documents;

    @Transient
    private Feedback feedback;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the expert registered")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the expert updated his profile")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This identify if the account is active")
    private int deleted;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the job was post")
    private Date posted;
    
    public JobLite() {

    }

    public JobLite(Long id) {
        this.id = id;
    }

    public JobLite(String name, String description, Date due_date, double amount, int days_post, User user,JobStatusCatalog status, Expert expert) {
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.created = new Date();
        this.updated = new Date();
    }

    public JobLite(long id, String name, String description, Date due_date, double amount, int days_post, User user, JobStatusCatalog status, Expert expert, Set<JobDocument> documents, Feedback feedback, Date created, Date updated, int deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.documents = documents;
        this.feedback = feedback;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public JobLite(long id, String name, String description, Date due_date, double amount, int days_post, User user, JobStatusCatalog status, Expert expert, Set<Skill> skills, Set<JobDocument> documents, Feedback feedback, Date created, Date updated, int deleted, Date posted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.due_date = due_date;
        this.amount = amount;
        this.days_post = days_post;
        this.user = user;
        this.status = status;
        this.expert = expert;
        this.skills = skills;
        this.documents = documents;
        this.feedback = feedback;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
        this.posted = posted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getDays_post() {
        return days_post;
    }

    public void setDays_post(int days_post) {
        this.days_post = days_post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public JobStatusCatalog getStatus() {
        return status;
    }

    public void setStatus(JobStatusCatalog status) {
        this.status = status;
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        this.expert = expert;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Set<JobDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<JobDocument> documents) {
        this.documents = documents;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public Job toJob() {
        return new Job(id, name, description, due_date, amount, days_post, user, status, expert, skills, documents, feedback, created, updated, deleted, posted);
    }

    public Date getPosted() {
        return posted;
    }

    public void setPosted(Date posted) {
        this.posted = posted;
    }

}
