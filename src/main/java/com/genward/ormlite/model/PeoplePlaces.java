/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.ormlite.model;

import com.genward.persistence.catalog.PeoplePlaceRelTypeCatalog;
import com.genward.persistence.model.People;
import com.genward.persistence.model.Place;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_R_PEOPLE_PLACES")
public class PeoplePlaces implements Serializable{
     
    @ManyToOne
    @JoinColumn(name = "person_id")
    @ApiModelProperty(notes = "This is the person")
    private People person;
    
    @ManyToOne
    @JoinColumn(name = "place_id")
    @ApiModelProperty(notes = "This is the place")
    private Place place;
    
    @ManyToOne
    @JoinColumn(name = "type_id")
    @ApiModelProperty(notes = "type of relation")
    private PeoplePlaceRelTypeCatalog type;
    
    @Column(columnDefinition = "DATETIME", name = "date")
    @ApiModelProperty(notes = "date for the relation")
    private Date date;

    public PeoplePlaces() {
    }

    public PeoplePlaces(People person, Place place, PeoplePlaceRelTypeCatalog type, Date date) {
        this.person = person;
        this.place = place;
        this.type = type;
        this.date = date;
    }

    
    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public PeoplePlaceRelTypeCatalog getType() {
        return type;
    }

    public void setType(PeoplePlaceRelTypeCatalog type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    
    
}
