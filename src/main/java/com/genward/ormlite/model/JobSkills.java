/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name="GWD_R_JOB_SKILL")
public class JobSkills implements Serializable {

    @Column
    @ApiModelProperty(notes = "This is the job id")
    private long job_id;

    @Column
    @ApiModelProperty(notes = "This is the skill id")
    private long skill_id;

    public JobSkills() {
    }

    public JobSkills(long job_id, long skill_id) {
        this.job_id = job_id;
        this.skill_id = skill_id;
    }

    public long getJob_id() {
        return job_id;
    }

    public void setJob_id(long job_id) {
        this.job_id = job_id;
    }

    public long getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(long skill_id) {
        this.skill_id = skill_id;
    }

}
