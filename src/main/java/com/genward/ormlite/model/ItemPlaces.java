package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_R_ITEM_PLACES")
public class ItemPlaces implements Serializable {

    @Column
    @ApiModelProperty(notes = "This is the item_id")
    private long item_id;

    @Column
    @ApiModelProperty(notes = "This is the place ID")
    private long place_id;

    public ItemPlaces() {
    }

    public ItemPlaces(long item_id, long place_id) {
        this.item_id = item_id;
        this.place_id = place_id;
    }

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public long getPlace_id() {
        return place_id;
    }

    public void setPlace_id(long place_id) {
        this.place_id = place_id;
    }

}
