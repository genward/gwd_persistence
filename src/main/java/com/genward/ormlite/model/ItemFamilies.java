/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_R_ITEM_FAMILIES")
public class ItemFamilies {

    @Column
    @ApiModelProperty(notes = "This is the item_id")
    private long item_id;

    @Column
    @ApiModelProperty(notes = "This is the FAMILY ID")
    private long family_id;

    @Column
    @ApiModelProperty(notes = "This is the pages where the item refers to the family")
    private String pages;

    public ItemFamilies() {
    }

    public ItemFamilies(long item_id, long family_id, String pages) {
        this.item_id = item_id;
        this.family_id = family_id;
        this.pages = pages;
    }

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public long getFamily_id() {
        return family_id;
    }

    public void setFamily_id(long family_id) {
        this.family_id = family_id;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

}
