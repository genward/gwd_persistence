/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_R_ITEM_PEOPLE")
public class ItemPeople {

    @Column
    @ApiModelProperty(notes = "This is the item_id")
    private long item_id;

    @Column
    @ApiModelProperty(notes = "This is the person ID")
    private long person_id;

    @Column
    @ApiModelProperty(notes = "This is the pages where the item refers to the person")
    private String pages;

    public ItemPeople() {
    }

    public ItemPeople(long item_id, long person_id, String pages) {
        this.item_id = item_id;
        this.person_id = person_id;
        this.pages = pages;
    }

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public long getPerson_id() {
        return person_id;
    }

    public void setPerson_id(long person_id) {
        this.person_id = person_id;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

}
