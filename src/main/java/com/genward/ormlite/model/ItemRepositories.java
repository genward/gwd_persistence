package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_R_ITEM_REPOSITORIES")
public class ItemRepositories implements Serializable {

    @Column
    @ApiModelProperty(notes = "This is the item_id")
    private long item_id;

    @Column
    @ApiModelProperty(notes = "This is the REPOSITORY ID")
    private long repository_id;

    public ItemRepositories() {
    }

    public ItemRepositories(long item_id, long repository_id) {
        this.item_id = item_id;
        this.repository_id = repository_id;
    }

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public long getRepository_id() {
        return repository_id;
    }

    public void setRepository_id(long repository_id) {
        this.repository_id = repository_id;
    }

}
