/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author fnunez
 */
@Entity
@Table(name = "GWD_USER_SKILL")
public class UserSkills implements Serializable {

    @Column
    private long user_id;
    @Column
    private long skill_id;

    @Column
    @ApiModelProperty(notes = "This is the experience that the user has with this skill")
    private String experience;

    @Column
    @ApiModelProperty(notes = "This is the rating that the user has with this skill")
    private String rating;

    @Column
    @ApiModelProperty(notes = "This is the points that the user obtains with this skill")
    private int points;

    @Column(name = "rate_avg", columnDefinition = "DECIMAL(10,2)")
    @ApiModelProperty(notes = "This is the average of rating for all jobs related with this skill-user")
    private double rate_avg;

    @Column
    @ApiModelProperty(notes = "This is the number of jobs that the user has done using this skill")
    private int jobs;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when this skill was assigned to the user")
    private Date created;

    @Column(columnDefinition = "DATETIME")
    @ApiModelProperty(notes = "This is the date when the info of this association was updated")
    private Date updated;

    @Column
    @ApiModelProperty(notes = "This identify if the user still have this skill")
    private int deleted;

    public UserSkills() {
    }

    public UserSkills(long user_id, long skill_id, String experience, String rating, int points, Date created, Date updated, int deleted) {
        this.user_id = user_id;
        this.skill_id = skill_id;
        this.experience = experience;
        this.rating = rating;
        this.points = points;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public UserSkills(long user_id, long skill_id, String experience, String rating, int points, double rate_avg, int jobs, Date created, Date updated, int deleted) {
        this.user_id = user_id;
        this.skill_id = skill_id;
        this.experience = experience;
        this.rating = rating;
        this.points = points;
        this.rate_avg = rate_avg;
        this.jobs = jobs;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(long skill_id) {
        this.skill_id = skill_id;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public double getRate_avg() {
        return rate_avg;
    }

    public void setRate_avg(double rate_avg) {
        this.rate_avg = rate_avg;
    }

    public int getJobs() {
        return jobs;
    }

    public void setJobs(int jobs) {
        this.jobs = jobs;
    }
    
    

}
