package com.genward.ormlite.model;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GWD_R_ITEM_SURNAMES")
public class ItemSurnames implements Serializable {

    @Column
    @ApiModelProperty(notes = "This is the item_id")
    private long item_id;

    @Column
    @ApiModelProperty(notes = "This is the surname id")
    private long surname_id;

    public ItemSurnames() {
    }

    public ItemSurnames(long item_id, long surname_id) {
        this.item_id = item_id;
        this.surname_id = surname_id;
    }

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public long getSurname_id() {
        return surname_id;
    }

    public void setSurname_id(long surname_id) {
        this.surname_id = surname_id;
    }

}
